\contentsline {section}{\numberline {1}ROSS}{4}
\contentsline {subsection}{\numberline {1.1}Quick Build}{4}
\contentsline {subsection}{\numberline {1.2}Data Structures}{5}
\contentsline {subsubsection}{\numberline {1.2.1}Events}{5}
\contentsline {subsubsection}{\numberline {1.2.2}Logical Process Data Structure}{7}
\contentsline {subsubsection}{\numberline {1.2.3}Kernel Processes Data Structure}{9}
\contentsline {subsubsection}{\numberline {1.2.4}Processing Element Data Structure}{10}
\contentsline {subsection}{\numberline {1.3}ROSS Application Programming Interface (API)}{10}
\contentsline {subsubsection}{\numberline {1.3.1}Building ROSS}{10}
\contentsline {subsubsection}{\numberline {1.3.2}main -- Setting up the ROSS Model}{11}
\contentsline {subsubsection}{\numberline {1.3.3}Event Handler Data Structure}{14}
\contentsline {subsubsection}{\numberline {1.3.4} Global Variable and Function Declarations}{15}
\contentsline {subsubsection}{\numberline {1.3.5}Initialization}{19}
\contentsline {subsubsection}{\numberline {1.3.6}Event Handlers}{22}
\contentsline {subsubsection}{\numberline {1.3.7}Reverse Computation}{25}
\contentsline {subsubsection}{\numberline {1.3.8}Collecting Application Statistics/Results}{26}
\contentsline {subsection}{\numberline {1.4}Glossary of Functions \& Variables}{27}
\contentsline {subsection}{\numberline {1.5}Random Number Generation Functions}{29}
\contentsline {subsection}{\numberline {1.6}Priority Queue}{30}
\contentsline {subsection}{\numberline {1.7}ROSS Errors}{30}
\contentsline {subsection}{\numberline {1.8}Model Development Hints}{31}
\contentsline {section}{\numberline {2}ROSS.Net -- Large-scale Meta-Modeling Framework for Large-Scale Network Modeling}{32}
\contentsline {subsection}{\numberline {2.1}ROSS.Net API}{33}
\contentsline {subsubsection}{\numberline {2.1.1}Network Topology Definition}{34}
\contentsline {subsubsection}{\numberline {2.1.2}Network Traffic Topology Definition}{35}
\contentsline {subsubsection}{\numberline {2.1.3}Dynamic Network Link Topology Definition}{35}
\contentsline {subsection}{\numberline {2.2}ROSS Memory Library}{35}
\contentsline {subsection}{\numberline {2.3}Executing ROSS.Net}{37}
\contentsline {subsection}{\numberline {2.4}The Tools Directory}{37}
\contentsline {subsection}{\numberline {2.5}Constructing New Models}{38}
\contentsline {subsection}{\numberline {2.6}Model Development}{38}
\contentsline {subsubsection}{\numberline {2.6.1}LP Initialize}{39}
\contentsline {subsubsection}{\numberline {2.6.2}LP Event Handler}{39}
\contentsline {subsubsection}{\numberline {2.6.3}LP Reverse Computation Event Handler}{40}
\contentsline {subsubsection}{\numberline {2.6.4}LP Finalize}{40}
\contentsline {subsubsection}{\numberline {2.6.5}LP XML Handler}{40}
\contentsline {subsubsection}{\numberline {2.6.6}Model Main and Finalization}{40}
\contentsline {subsection}{\numberline {2.7}Incorporating Models into the Framework}{41}
\contentsline {subsubsection}{\numberline {2.7.1}Adding the model source code}{41}
\contentsline {subsubsection}{\numberline {2.7.2}Adding the model to the build system}{42}
\contentsline {subsection}{\numberline {2.8}Edit the ROSS.Net Source Code}{43}
\contentsline {subsection}{\numberline {2.9}ROSS.Net Modeling Paradigm}{45}
\contentsline {subsection}{\numberline {2.10}Model Function Hooks}{45}
\contentsline {subsection}{\numberline {2.11}LP Function Hooks}{47}
\contentsline {subsubsection}{\numberline {2.11.1}LP Data Types}{47}
\contentsline {subsubsection}{\numberline {2.11.2}LP XML Handler}{48}
\contentsline {subsubsection}{\numberline {2.11.3}LP Initialization}{49}
\contentsline {subsubsection}{\numberline {2.11.4}LP Event Handler}{49}
\contentsline {subsubsection}{\numberline {2.11.5}LP Reverse Computation Event Handler}{50}
\contentsline {subsubsection}{\numberline {2.11.6}LP Finalization}{51}
\contentsline {subsection}{\numberline {2.12}Sending Events in ROSS.Net}{52}
