#ifndef INC_gvt_mpi_allreduce_h
#define INC_gvt_mpi_allreduce_h

static inline bool
tw_gvt_inprogress(tw_pe * me)
{
	if (!(me -> interval_counter)) return true;
	return false;
}

static int old_percent_complete = 0;
static int percent_complete = 0;

static unsigned gvt_counter = 0;

//static unsigned long long early_finish_thres = EARLY_FINISH;

static inline void 
gvt_print(tw_pe * me)
{
	percent_complete = (int) floor(100 * (me -> GVT / g_tw_ts_end));

	if (percent_complete < old_percent_complete + 10) return;	
	old_percent_complete = percent_complete; 

	//fprintf(stderr, "GVT #%d: simulation %d%% complete (GVT = %.0f) (%d)\n", 
	//		 gvt_counter, percent_complete, me -> GVT, active_threads);
	
	fprintf(stderr, "GVT #%d: simulation %d%% complete (GVT = %.0f)\n", gvt_counter, percent_complete, me -> GVT);
}

#endif
