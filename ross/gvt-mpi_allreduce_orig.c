#include <ross.h>

static __thread unsigned int gvt_force = 0;
static __thread tw_stat all_reduce_cnt = 0;

static const tw_optdef gvt_opts [] =
{
	TWOPT_GROUP("ROSS MPI GVT"),
	TWOPT_UINT("gvt-interval", g_tw_gvt_interval, "GVT Interval"),
	TWOPT_END()
};

void
tw_gvt_step1(tw_pe * me)
{
	tw_stat local_white = 0;
	tw_stat total_white = 0;

	tw_stime lvt = DBL_MAX;
	tw_stime gvt = DBL_MAX;
	tw_clock s, start = tw_clock_read();

	//fprintf(stderr, "%u`s gvt %.0f\n", g_tw_tid, me -> GVT);

	while (1)
	{
	  	s = tw_clock_read();
	    tw_net_read(me);
	  	me -> stats.s_net_read += tw_clock_read() - s;
      
		// send message counts to create consistent cut
		local_white = me -> s_nwhite_sent - me -> s_nwhite_recv;
		all_reduce_cnt++;
		
		// reduce local_white with sum, map it to total_white
		mt_allreduce_long_sum(&local_white, &total_white, &sum_barrier[sum_index]);
		sum_index = (sum_index + 1) % 2;
	
		// Ensures no intransit message accross any thread and machines
		if (total_white == 0) break;
	}


	lvt = min_pq_outq(me);
	all_reduce_cnt++;
	
	// reduce lvt with min and map it to gvt
	mt_allreduce_double_min(&lvt, &gvt, &min_barrier[min_index]);	
	min_index = (min_index + 1) % 2;

	g_tw_gvt_done++;
	if (me -> GVT > gvt) tw_error(TW_LOC, "PE %u GVT decreased %g -> %g", me->id, me->GVT, gvt);

	me -> s_nwhite_sent = 0;
	me -> s_nwhite_recv = 0;
	me -> trans_msg_ts = DBL_MAX;
	me -> GVT = gvt;
	me -> interval_counter = g_tw_gvt_interval;
	me -> stats.s_gvt += tw_clock_read() - start;
	
	if (tw_node_eq(&g_tw_mynode, &g_tw_masternode)) 
	{
		gvt_counter++; 
		gvt_print(me);
	}

	start = tw_clock_read();
	tw_pe_fossil_collect(me);
	me->stats.s_fossil_collect += tw_clock_read() - start;
}

inline tw_stime
min_pq_outq(tw_pe * me)
{
	// return the minimum of pq of PE	
	tw_stime pq_min = tw_pq_minimum(me -> pq);
	tw_stime lvt = me -> trans_msg_ts;
	
	if (lvt > pq_min) lvt = pq_min;
	
	return lvt;
}

void
tw_gvt_step2(tw_pe *me) {}

void gvt_init (tw_pe * me) 
{
	me -> s_nwhite_sent = 0;
	me -> s_nwhite_recv = 0;
	me -> interval_counter = g_tw_gvt_interval;
}

int 
percent_comp_ret () {return percent_complete;}

const tw_optdef *
tw_gvt_setup(void) {return gvt_opts;}

unsigned int 
get_gvt_count () {return gvt_counter;}

void
tw_gvt_start(void) {}

void
tw_gvt_force_update(tw_pe *me)
{
	gvt_force++;
	me -> interval_counter = 0;
}

void
tw_gvt_stats(FILE * f)
{
	fprintf(f, "\nTW GVT Statistics: Barrier\n");
	fprintf(f, "\t%-50s %11d\n", "GVT Interval", g_tw_gvt_interval);
	fprintf(f, "\t%-50s %11d\n", "Batch Size", g_tw_mblock);
	fprintf(f, "\t%-50s %11d\n", "Forced GVT", gvt_force);
	fprintf(f, "\t%-50s %11d\n", "Total GVT Computations", g_tw_gvt_done);
	fprintf(f, "\t%-50s %11lld\n", "Total All Reduce Calls", all_reduce_cnt);
	fprintf(f, "\t%-50s %11.2lf\n", "All Reduce / GVT", (double) ((double) all_reduce_cnt / (double) g_tw_gvt_done));
}
