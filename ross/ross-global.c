#include <ross.h>
#include <pthread.h>
	/*
	 * LP data structures are allocated dynamically when the
	 * process starts up based on the number it requires.
	 *
	 * g_tw_nlp         -- Number of LPs in simulation.
	 * g_tw_lp_offset   -- global id of g_tw_lp[0]
	 * g_tw_nkp         -- Number of KPs in simulation.
	 * g_tw_lp          -- Public LP objects.
	 * g_tw_kp          -- Public KP objects.
	 * g_tw_sv_growcnt  -- Add this many SV's to an LP if it is empty.
	 * g_tw_fossil_attempts  -- Number of times fossil_collect is called
         * g_tw_nRNG_per_lp -- Number of RNG per LP
	 */

#ifdef DDS  
//__thread sigset_t set;
//bool * barrier_blocked;

//pthread_t * tid_table;

unsigned * sys_tid_table;
int * affinity_table;
int * affinity_table_inv;

unsigned int zero_counter_threshold;

//volatile unsigned int master_done = 0;
unsigned int master_done = 0;

struct bool_padded * blocked_threads;
//bool * blocked_threads;

struct sem_padded * locks;
//sem_t * locks;
#endif

#ifdef PAPI
pthread_mutex_t papi_lock;
long long total_inst = 0;
long long total_cycles = 0;
#endif

pthread_mutex_t stat_lock;

tw_synch     g_tw_synchronization_protocol = NO_SYNCH;
__thread map_local_f  g_tw_custom_lp_global_to_local_map = NULL;
__thread map_custom_f g_tw_custom_initial_mapping = NULL;
__thread tw_lp_map    g_tw_mapping = LINEAR;   

__thread tw_lpid        g_tw_nlp = 0;
__thread tw_lpid	g_tw_lp_offset = 0;
__thread tw_lp		**g_tw_lp = NULL;
__thread tw_kp		**g_tw_kp = NULL;

__thread int             g_tw_fossil_attempts = 0;
__thread tw_seed        *g_tw_rng_seed = NULL;
__thread unsigned int	g_tw_sim_started = 0;

__thread tw_kpid        g_tw_nkp = 1;
__thread int             g_tw_sv_growcnt = 10;
__thread unsigned int    g_tw_nRNG_per_lp = 1;
__thread tw_lpid         g_tw_rng_default = TW_TRUE;
__thread size_t          g_tw_rng_max = 1;
__thread size_t g_tw_msg_sz;

#if ROSS_MEMORY
__thread unsigned int	g_tw_memory_nqueues = 64;
#else 
__thread unsigned int	g_tw_memory_nqueues = 0;
#endif

__thread size_t		g_tw_memory_sz = 0;
__thread size_t		g_tw_event_msg_sz = 0;

        /*
         * Number of iterations in scheduler (invoked from tw_run)
         * before calling application callout.  Default is to call
         * callout for each iteration.
         */
__thread unsigned int    g_tw_periodicity = 1;

        /*
         * Minimum lookahead for a model -- model defined when
         * using the Simple Synchronization Protocol (conservative)
         */
__thread tw_stime g_tw_lookahead = 0.005;

	/*
	 * Number of messages to process at once out of the PQ before
	 * returning back to handling things like GVT, message recption,
	 * etc.
	 */

#ifdef SIM_SLOW
unsigned int g_tw_mblock = 1;
#else
unsigned int g_tw_mblock = 8;
#endif

unsigned int g_tw_gvt_interval = 1;

// controls out of buffer
__thread unsigned int	g_tw_gvt_threshold = 1000;

//tw_stime     g_tw_ts_end = 100000.0;
//tw_stime     g_tw_ts_end = 50000.0;
//tw_stime     g_tw_ts_end = 20000.0; 

tw_stime     g_tw_ts_end = 5000.0; // Test with this

//tw_stime     g_tw_ts_end = 2000.0; 
//tw_stime     g_tw_ts_end = 1000.0;
//tw_stime     g_tw_ts_end = 500.0;  

	/*
	 * g_tw_npe             -- Number of PEs.
	 * g_tw_pe              -- Public PE objects.
	 * g_tw_events_per_pe   -- Number of events to place in for each PE.
	 *                         MUST be > 1 because of abort buffer.
	 */
__thread tw_pe		**g_tw_pe;
__thread unsigned int g_tw_master = 0;
__thread unsigned int  g_tw_gvt_done = 0;

__thread tw_peid		g_tw_npe = 1;
__thread int            g_tw_events_per_pe = 2048; // model changes this to opt_mem + mul x nlp_per_pe

//DJ
__thread int sum_index = 0;
__thread int min_index = 0;
__thread int local_numa_id = -1;

	/*
	 * Network variables:
	 *
	 * g_tw_net_barrier_flag -- when set, PEs should stop in next barrier encountered
	 * g_tw_masternode -- pointer to GVT net node, for GVT comp
	 */
__thread unsigned int	g_tw_net_device_size = 0;
__thread tw_node		g_tw_mynode = -1;
__thread tw_node		g_tw_masternode = -1;

//DJ
__thread int		g_tw_n_threads = -1;
__thread int		g_tw_pid = -1;
__thread int		g_tw_tid = -1;

__thread FILE		*g_tw_csv = NULL;
int barrier_dbg = 0;

tw_stime g_tw_clock_rate = 1000000000.0; // Default to 1 GHz
