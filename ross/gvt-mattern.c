#include <ross.h>
#include <stddef.h>

// Statistics
static __thread unsigned int gvt_force = 0;
static __thread tw_stime measure_fc = 0;
static __thread tw_stime measure_gvt = 0;

static int c1_counter = 0; 		// Counts how many threads reached the C 1
static int c2_counter = 0;		// Counts how many threads reached the C 2
static int gvt_done_counter = 0;
static bool reset = true;
static bool t_update = true;

static tw_stime t_min = DBL_MAX;
static unsigned int * msg_counters; 

void 
gvt_init(tw_pe * me) // From tw_run() in tw_setup.c
{
	if (!g_tw_tid) msg_counters = (unsigned int *) calloc(sizeof(unsigned int), g_tw_n_threads);	

	me -> interval_counter = g_tw_gvt_interval;
	me -> color = white;
    me -> t_red = DBL_MAX;
		
	me -> msg_counters = (unsigned int *) calloc(sizeof(unsigned int), g_tw_n_threads);
	me -> c2_checked = false;
}


void 
tw_gvt_step1(tw_pe * me)
{
	// C 1 is being constructed
	if (me -> color == white && !gvt_done_counter)
	{
		// Each thread checks the C 1 and accumulate its message counters
		#ifdef DEBUG
		fprintf(stderr, "%d w\n", g_tw_tid);
		#endif

		measure_gvt = tw_clock_read();
		me -> color = red;
		me -> t_red = DBL_MAX;			
			
		accumulate_msg_counters(me); // control block is updated 
			
		__sync_add_and_fetch(&c1_counter, 1);	
	}
	else if (c1_counter == g_tw_n_threads && !(me -> c2_checked) && me -> color == red &&
			me -> msg_counters[g_tw_tid] + msg_counters[g_tw_tid] == 0)
	{
		if (!reset) reset = true; // Might traverse all the way to the main memory, do not know how to fix it ..

		#ifdef DEBUG
		fprintf(stderr, "%d m\n", g_tw_tid);
		#endif

		me -> c2_checked = true; 
		update_control(me);			
		
		__sync_add_and_fetch(&c2_counter, 1);
	}
	else if (c2_counter == g_tw_n_threads && me -> color == red)
	{
		finish_gvt(me);
		
		#ifdef DEBUG
		fprintf(stderr, "%d takes %.0f\n", g_tw_tid, t_min);	
		#endif
        me -> GVT = t_min; 

		measure_fc = tw_clock_read();
		tw_pe_fossil_collect(me);
		me -> stats.s_fossil_collect += tw_clock_read() - measure_fc;

		__sync_add_and_fetch(&gvt_done_counter, 1);
		me -> stats.s_gvt += tw_clock_read() - measure_gvt;
	}
	else if (gvt_done_counter == g_tw_n_threads && me -> color == white) 
	{
		//if (__sync_lock_test_and_set(&reset, false))
		if (__sync_bool_compare_and_swap(&reset, true, false))
		{	
			#ifdef DEBUG
			fprintf(stderr, "%d r\n", g_tw_tid);
			#else
			gvt_print(me);
			#endif

			t_min = DBL_MAX;
			c1_counter = 0;
			c2_counter = 0;
			gvt_done_counter = 0;
		}
	}
}

int 
percent_comp_ret () {return percent_complete;}

inline void
mattern_receive_track (tw_pe * me, tw_event * e)
{
	if (e -> color == white) me -> msg_counters[g_tw_tid]--;	
}

inline void
mattern_send_track (tw_pe * me, tw_event * cp_e, tw_event * e, int dest_node_id)
{
	if (me -> color == white) 
	{
		me -> msg_counters[dest_node_id]++;
		cp_e -> color = white;
	}
	else if (me -> color == red)
	{
		cp_e -> color = red;
		me -> t_red = min(me -> t_red, e -> recv_ts);
	}
	else tw_error(TW_LOC, "Error in mattern orig send"); 	
}

inline void 
update_control(tw_pe * me)
{
	tw_stime t = min_pq_outq(me);
	if (me -> t_red < t) t = me -> t_red;

	//pthread_mutex_lock(&cm_lock);
	//t_min = min(t_min, t);
	//pthread_mutex_unlock(&cm_lock);

	while (1)
	{
		//if (__sync_lock_test_and_set(&t_update, false))
		if (__sync_bool_compare_and_swap(&t_update, true, false))
		{
			t_min = min(t_min, t);
			t_update = true;
			break;
		}	
	}

	msg_counters[g_tw_tid] = 0;	
	me -> msg_counters[g_tw_tid] = 0;
}

inline void 
finish_gvt(tw_pe * me)
{	
	g_tw_gvt_done++;
	me -> interval_counter = g_tw_gvt_interval;
	me -> trans_msg_ts = DBL_MAX;

	me -> c2_checked = false;
	me -> color = white;
}

inline void 
accumulate_msg_counters(tw_pe * me)
{
	for (int i = 0; i < g_tw_n_threads; i++)
	{
		if (i != g_tw_tid)
		{
			__sync_add_and_fetch(&msg_counters[i], me -> msg_counters[i]); 
			me -> msg_counters[i] = 0;
		}
	}
}

inline tw_stime
min_pq_outq(tw_pe * me)
{
	//tw_net_read(me); // might be neccessary

	tw_stime pq_min = tw_pq_minimum(me -> pq);
	tw_stime t_msg = me -> trans_msg_ts;
	
	if (t_msg > pq_min) t_msg = pq_min;

	if (t_msg == DBL_MAX) 
	{
		tw_net_read(me); 
		tw_stime pq_min = tw_pq_minimum(me -> pq);
		return pq_min;
	}

	return t_msg;
}

void
tw_gvt_force_update(tw_pe * me) 
{
	gvt_force++;
	me -> interval_counter = 0;
	//fprintf(stderr, "GVT forced\n");
}


static const tw_optdef gvt_opts [] =
{
	TWOPT_GROUP("ROSS MPI GVT"),
	TWOPT_UINT("gvt-interval", g_tw_gvt_interval, "GVT Interval"),
	TWOPT_END()
};

const tw_optdef *
tw_gvt_setup(void) 
{
	return gvt_opts;
}

void
tw_gvt_stats(FILE * f)
{
	fprintf(f, "\nTW GVT Statistics: Mattern\n");
	fprintf(f, "\t%-50s %11d\n", "GVT Interval", g_tw_gvt_interval);
	fprintf(f, "\t%-50s %11d\n", "Batch Size", g_tw_mblock);
	fprintf(f, "\t%-50s %11d\n", "Forced GVT", gvt_force);
	fprintf(f, "\t%-50s %11d\n", "Total GVT Computations", g_tw_gvt_done);
}

void 
tw_gvt_start() {} // From tw_init() in tw-setup.c	
