#ifndef INC_gvt_mattern_h
#define INC_gvt_mattern_h

static inline bool
tw_gvt_inprogress(tw_pe * me) 
{
	// when interval counter is 0, gvt is being computed
	if (!(me -> interval_counter)) return true;
	else return false;
}

static int old_percent_complete = 0;
static int percent_complete = 0;

static inline void 
gvt_print(tw_pe * me)
{
	percent_complete = (int) floor(100 * (me -> GVT / g_tw_ts_end));
	if (percent_complete < old_percent_complete + 10) return;	

	old_percent_complete = percent_complete; 

	#ifndef DEBUG
	fprintf(stderr, "GVT #%d: simulation %d%% complete (GVT = %.0f)\n", g_tw_gvt_done, percent_complete, me -> GVT);
	#else
	fprintf(stderr, "GVT #%d: simulation %d%% complete (GVT = %.0f) - %d\n", g_tw_gvt_done, percent_complete, me -> GVT, g_tw_tid);
	#endif
}
#endif
