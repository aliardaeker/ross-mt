#ifndef INC_tw_eventq_h
#define INC_tw_eventq_h

#define ROSS_DEBUG_FOSSIL 0
#define ROSS_DEBUG 0

#include <ross.h>
static inline void tw_eventq_delete_any(tw_eventq *q, tw_event *e);
static inline void tw_eventq_push(tw_eventq *q, tw_event *e);
static inline void tw_eventq_unshift(tw_eventq *q, tw_event *e);

static inline void 
tw_eventq_debug(tw_eventq * q)
{
	tw_event	* next;
	tw_event	* last;

  	int		 cnt;

  	cnt = 0;
  	next = q -> head;
  	last = NULL;

	//printf("\ntw_eventq_debug: node=%d q=%p size=%ld contents send_pe:event_id\n", g_tw_mynode, q, q->size);
 
	while (next)
    	{
      		cnt++;

      		if (next -> prev != last) tw_error(TW_LOC, "\nPrev pointer not correct!\n");
		else
		{
			//if (!g_tw_pid) fprintf(stderr, "Prev pointer correct\n");
			//else fprintf(stderr, "	Prev pointer correct\n");
		}

		//printf("%d:%d ", (int) next -> send_pe, next -> event_id);

      		last = next;
      		next = next -> next;
    	}

  	if (q -> tail != last) if (!g_tw_pid) tw_error(TW_LOC, "\nTail pointer not correct!\n");
	
  	if (cnt != q -> size) tw_error(TW_LOC, "\nSize not correct! cnt = %d, q size = %d\n", cnt, q -> size);	
}

static inline void 
// q is free event q, h and t are processed events to be freeq
tw_eventq_push_list(tw_eventq * q, tw_event * h, tw_event * t, int cnt)
{
	tw_event	* e, * n;
	tw_event	* cev;
	tw_event	* next;
	tw_pe 		* pe;

	// prev pointer not correct !!!
	//if (!g_tw_pid) fprintf(stderr, "q size: %ld\n", q -> size);
	#if ROSS_DEBUG_FOSSIL
	tw_eventq_debug(q);
	#endif
	
	t -> next = q -> head;

	// seg fault here
	if (q -> head) q -> head -> prev = t;

	
 	q -> head = h;
 	q -> head -> prev = NULL;

  	if (!q -> tail) q -> tail = t;

  	q -> size += cnt;

  	// iterate through list to collect sent events
  	t = t -> next;
  	
	for (e = h; e != t; )
    {
		// Start of the event list caused by this event
      	if (e -> caused_by_me)
		{
	  		cev = next = e -> caused_by_me;

	  		while (cev)
	    	{
				// Next in the caused by me chain
	      		next = cev -> cause_next;
	      		cev -> cause_next = NULL;
				
				// Insert cev into cev -> src_lp -> pe`s free queue
	      		if (cev -> state.owner == TW_pe_sevent_q) tw_event_free(cev -> src_lp -> pe, cev);
	      		
				cev = next;
	    	}

	  		e -> caused_by_me = NULL;
	  		e -> state.owner = TW_pe_free_q;
	
			//DJ:
			if (e -> numa_id != local_numa_id)
			{
				n = e -> next;
				tw_eventq_delete_any(q, e);
				
				pe = e -> dest_lp -> pe;
				tw_eventq_unshift(&(pe -> numa_free_q[e -> numa_id]), e);
				e = n;
			}
			else e = e -> next;
		}
		else e = e -> next;
	}
	
	#if ROSS_DEBUG_FOSSIL
  	tw_eventq_debug(q);
	#endif
}


static inline void 
tw_eventq_fossil_collect(tw_eventq * q, tw_pe * pe) 
{
	// q is &kp -> pevent_q	

	tw_stime gvt = pe -> GVT;

	//if (pe -> in_flush) gvt = pe -> fake_GVT;

	tw_event * h = q -> head;
	tw_event * t = q -> tail;
	
	int cnt;

	/* Nothing to collect from this event list? */
	if (!t || t -> recv_ts >= gvt) 
	{
		// Always returns from here !!! 		
		// GVT is too small so that we cannot collect anything so that pq is not cleared and free q is not populated
		// and threads have out of free event q			

		//if (t && !g_tw_pid && g_tw_tid == 1) fprintf(stderr, "tail`s recv_ts %.0f >= GVT (%.0f). h: %.0f, lvt: %.0f, size: %zu\n", 
		//				     t -> recv_ts, pe -> GVT, h -> recv_ts, pe -> LVT, q -> size);
		//else fprintf(stderr, "%llu has empty p_eventq, GVT: %f\n", pe -> id, pe -> GVT);
		
		//if (t) fprintf(stderr, "%d cannot fossil collects - %.0f, %.0f, %.0f - %ld\n", g_tw_tid, t -> recv_ts, h -> recv_ts, gvt, q -> size);
		//else fprintf(stderr, "%d cannot fossil collects - %.0f\n", g_tw_tid, gvt);
		
		return;
	}

	//fprintf(stderr, "%d fossil\n", g_tw_tid);
	
	if (h -> recv_ts < gvt)
	{
		//fprintf(stderr, "%llu fossil collects all, GVT: %.0f, tail recv_ts: %.0f\n", pe -> id, pe -> GVT, t -> recv_ts);
 		
		/* Everything in the queue can be collected */
      		tw_eventq_push_list(&pe -> free_q, h, t, q -> size);
      		
		q -> head = q -> tail = NULL;
      		q -> size = 0;
  	} 
  	else 
	{
		//fprintf(stderr, "%llu fossil collects some, GVT: %.0f, tail recv_ts: %.0f\n", pe -> id, pe -> GVT, t -> recv_ts);
    		
		/* Only some of the list can be collected.  We'll wind up
     		* with at least one event being collected and at least
     		* another event staying behind in the eventq structure so
    	 	* we can really optimize this list splicing operation for
     		* these conditions.
     		*/
    		tw_event *n;

    		/* Search the leading part of the list... */
    		for (h = t->prev, cnt = 1; h && h->recv_ts < gvt; cnt++) h = h->prev;

    		/* t isn't eligible for collection; its the new head */
    		n = h;

    		/* Back up one cell, we overshot where to cut the list */
    		h = h->next;

    		/* Cut h..t out of the event queue */
    		q->tail = n;
    		n->next = NULL;
    		q->size -= cnt;

    		/* Free h..t (inclusive) */
		tw_eventq_push_list(&pe -> free_q, h, t, cnt);
  	}

	//if (!g_tw_pid && g_tw_tid == 1) fprintf(stderr, "FC Collected\n");
}

//DJ:
static inline void
mt_tw_eventq_push_list(tw_eventq * q, tw_event * h, tw_event * t, int cnt)
{
	#if ROSS_DEBUG
  	tw_eventq_debug(q);
	#endif

  	t->next = q->head;

  	if (q->head) q->head->prev = t;

  	q->head = h;
  	q->head->prev = NULL;

  	if (!q->tail) q->tail = t;

  	q->size += cnt;
	
	#if ROSS_DEBUG
  	tw_eventq_debug(q);
	#endif
}

static inline void 
tw_eventq_alloc(tw_eventq * q, unsigned int cnt)
{
  tw_event *event;
  size_t event_len;
  size_t align;

  /* Construct a linked list of free events.  We allocate
   * the events such that they look like this in memory:
   *
   *  ------------------
   *  | tw_event       |
   *  | user_data      |
   *  ------------------
   *  | tw_event       |
   *  | user_data      |
   *  ------------------
   *  ......
   *  ------------------
   */

  align = max(sizeof(double), sizeof(void*));
  event_len = sizeof(tw_event) + g_tw_msg_sz;
  
  if (event_len & (align - 1))
  {
      event_len += align - (event_len & (align - 1));
      //tw_error(TW_LOC, "REALIGNING EVENT MEMORY!\n");
  }
  
  g_tw_event_msg_sz = event_len;

  // compute number of events needed for the network.
  // g_tw_gvt_threshold = (int) ceil(g_tw_net_device_size / g_tw_event_msg_sz);
  
  g_tw_gvt_threshold = g_tw_net_device_size; 
  //g_tw_gvt_threshold = 2;
  
  g_tw_events_per_pe += g_tw_gvt_threshold;
  //g_tw_events_per_pe = 1;
  
  cnt += g_tw_gvt_threshold;

  q->head = event = tw_calloc(TW_LOC, "events", event_len, cnt);
  q->size = cnt;

  while (--cnt) {
//DJ:
    //event->numa_id = g_tw_tid/NUMA_NODE_SIZE;
    event->numa_id = g_tw_tid/g_tw_n_threads;
    event->state.owner = TW_pe_free_q;
    event->prev = (tw_event *) (((char *)event) - event_len);
    event->next = (tw_event *) (((char *)event) + event_len);
    event = event->next;
  }
//DJ:
  //event->numa_id = g_tw_tid/NUMA_NODE_SIZE;
  event->numa_id = g_tw_tid/g_tw_n_threads;
  event->state.owner = TW_pe_free_q;
  event->prev = (tw_event *) (((char *)event) - event_len);
  q->head->prev = event->next = NULL;
  q->tail = event;
}

static inline void 
tw_eventq_push(tw_eventq *q, tw_event *e)
{
  	tw_event *t = q->tail;

	#if ROSS_DEBUG
  	tw_eventq_debug(q);
	#endif

  	e->next = NULL;
  	e->prev = t;
 	if (t) t->next = e;
  	else q->head = e;

  	q->tail = e;
  	q->size++;

	#if ROSS_DEBUG
  	tw_eventq_debug(q);
	#endif
}

static inline tw_event * 
tw_eventq_peek(tw_eventq *q)
{
  return q->tail;
}

static inline tw_event * 
tw_eventq_pop(tw_eventq * q)
{
  	tw_event *t = q->tail;
  	tw_event *p;

	#if ROSS_DEBUG
  	tw_eventq_debug(q);
	#endif

  	if (!t) return NULL;

  	p = t->prev;

  	if (p) p->next = NULL;
  	else q->head = NULL;

  	q->tail = p;
  	q->size--;

 	t->next = t->prev = NULL;

	#if ROSS_DEBUG
  	tw_eventq_debug(q);
	#endif
  	return t;
}

static inline void 
tw_eventq_unshift(tw_eventq *q, tw_event *e)
{
  	tw_event * h = q -> head;

	#if ROSS_DEBUG
  	tw_eventq_debug(q);
	#endif

  	#pragma GCC diagnostic push
  	#pragma GCC diagnostic ignored "-Wmaybe-uninitialized"
  	e -> prev = NULL;
  	#pragma GCC diagnostic pop

  	e -> next = h;

  	if (h) h -> prev = e;
  	else q -> tail = e;

  	q -> head = e;
  	q -> size++;

	#if ROSS_DEBUG
  	tw_eventq_debug(q);
	#endif
}

static inline tw_event * 
tw_eventq_peek_head(tw_eventq *q)
{
  return q->head;
}

static inline tw_event * 
tw_eventq_shift(tw_eventq *q)
{
	tw_event *h = q->head;
	tw_event *n;

	#if ROSS_DEBUG
  	tw_eventq_debug(q);
	#endif

  	if (!h) return NULL;

  	n = h->next;

  	if (n) n->prev = NULL;
  	else q->tail = NULL;

  	q->head = n;
  	q->size--;

  	h->next = h->prev = NULL;

	#if ROSS_DEBUG
  	tw_eventq_debug(q);
	#endif

  	return h;
}

static inline void 
tw_eventq_delete_any(tw_eventq *q, tw_event *e)
{
  	tw_event *p = e->prev;
  	tw_event *n = e->next;

	#if ROSS_DEBUG
  	tw_eventq_debug(q);
	#endif

 	if (p) p->next = n;
  	else q->head = n;

  	if (n) n->prev = p;
  	else q->tail = p;

  	e->next = e->prev = NULL;
  	q->size--;

	#if ROSS_DEBUG
  	tw_eventq_debug(q);
	#endif
}

static inline tw_event * 
tw_eventq_pop_list(tw_eventq * q)
{
  	tw_event	*h = q->head;

  	q->size = 0;
  	q->head = q->tail = NULL;

  	return h;
}

/*
 * The purpose of this function is to be able to remove some
 * part of a list.. could be all of list, from head to some inner
 * buffer, or from some inner buffer to tail.  I only care about the
 * last case.. 
 */
static inline void 
tw_eventq_splice(tw_eventq * q, tw_event * h, tw_event * t, int cnt)
{
	#if ROSS_DEBUG
  	tw_eventq_debug(q);
	#endif

  	if (h == q->head && t == q->tail)
    	{
      		q->size = 0;
      		q->head = q->tail = NULL;
      		return;
    	}

  	if(h == q->head) q->head = t->next;
  	else h->prev->next = t->next;

  	if(t == q->tail) q->tail = h->prev;
  	else t->next->prev = h->prev;

  	q->size -= cnt;

	#if ROSS_DEBUG
  	tw_eventq_debug(q);
	#endif
}

#endif
