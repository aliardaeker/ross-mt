#include <ross.h>

// Statistics
static __thread unsigned int gvt_force = 0;
static __thread tw_stime measure_fc = 0;
static __thread tw_stime measure_gvt = 0;

// Phase Counters
static int counter_a = 0;
static int counter_send = 0;
static int counter_b = 0;
static int counter_aware = 0;

static tw_stime * local_mins; 	// PE minimums
static tw_stime node_gvt = DBL_MAX;        // local gvt
 
static bool GVT_run = true;            // starts the first phase
static bool reset_flag = true;          // resets flags (tas)
static bool compute_GVT = true;         // compute local minima just once (tas)
static bool node_GVT_set = false;        // local gvt is read, local minima is computed

void 
gvt_init(tw_pe * me) // From tw_run() in tw_setup.c
{
	if (!g_tw_tid) local_mins = (tw_stime *) calloc(sizeof(tw_stime), g_tw_n_threads);

	me -> phase = phase_a;
	me -> interval_counter = g_tw_gvt_interval;
}

void
tw_gvt_step1(tw_pe *me)
{
	// Phase A: compute min B
	if (me -> phase == phase_a && GVT_run)
	{
		//printf("Phase a %llu\n", me -> id);
		measure_gvt = tw_clock_read();

		tw_net_read(me); 			   // Receive all messages remote or regional
        
		tw_stime pq_min = tw_pq_minimum(me -> pq); // Smallest time stamp of any unprocessed event in priority queue
        tw_stime lvt = me -> trans_msg_ts;

		#ifdef DEBUG 
		fprintf(stderr, "pq: %.0f, net: %.0f, lvt: %.0f\n", pq_min, net_min, lvt);
		#endif

        if (lvt > pq_min) lvt = pq_min;
        me -> min_a = lvt;
	
		me -> phase = send;
		__sync_fetch_and_add(&counter_a, 1);	
	}
	// Phase Send: process and send one more message
	else if (me -> phase == send && counter_a == g_tw_n_threads)
	{				
		//printf("Phase s %llu\n", me -> id);
		tw_net_read(me); 			   // Receive all messages remote or regional
		tw_sched_batch(me);			   // Execute next batch of events and send messages

		if (GVT_run) 
		{
			GVT_run = false;
			reset_flag = true;
		}
		
		me -> phase = phase_b;
		__sync_fetch_and_add(&counter_send, 1);	
	}
	// Phase B: compute min B
	else if (me -> phase == phase_b && counter_send == g_tw_n_threads)
	{
		//printf("Phase b %llu\n", me -> id);
		tw_net_read(me); 			   // Receive all messages remote or regional
        
		tw_stime pq_min = tw_pq_minimum(me -> pq); // Smallest time stamp of any unprocessed event in priority queue
        tw_stime lvt = me -> trans_msg_ts;
 
		#ifdef DEBUG 
		fprintf(stderr, "pq: %.0f, net: %.0f, lvt: %.0f\n", pq_min, net_min, lvt);
		#endif

        if (lvt > pq_min) lvt = pq_min;
        me -> min_b = lvt;

		local_mins[g_tw_tid] = min(me -> min_a, me -> min_b);
		me -> phase = aware;
		__sync_fetch_and_add(&counter_b, 1);	
	}
	// Phase aware: compute GVT
	else if (me -> phase == aware && counter_b == g_tw_n_threads)
	{
		//printf("Phase Aware %llu\n", me -> id);
		//if (__sync_lock_test_and_set(&compute_GVT, false))
		if (__sync_bool_compare_and_swap(&compute_GVT, true, false))
		{
			for (int i = 0; i < g_tw_n_threads; i++) if (local_mins[i] < node_gvt) node_gvt = local_mins[i];
			node_GVT_set = true;	
		
			me -> GVT = node_gvt;
			gvt_print(me);
		}
	
		if (node_GVT_set)
		{
			finish_gvt(me); 
			me -> GVT = node_gvt;

            //fprintf(stderr, "     %llu Fossil Collects\n", me -> id);
            measure_fc = tw_clock_read();
            tw_pe_fossil_collect(me);
            me -> stats.s_fossil_collect += tw_clock_read() - measure_fc;
		
            __sync_fetch_and_add(&counter_aware, 1);
			me -> stats.s_gvt += tw_clock_read() - measure_gvt;	
		}
	}
	// End Phase: Reset counters and start the next GVT round
	else if (counter_aware == g_tw_n_threads)		
	{
		//printf("Phase end %llu\n", me -> id);
		//if (__sync_lock_test_and_set(&reset_flag, false))
		if (__sync_bool_compare_and_swap(&reset_flag, true, false))
		{
			counter_a = 0;
			counter_send = 0;
			counter_b = 0;	
			counter_aware = 0;
			
			compute_GVT = true;
			node_GVT_set = false;
			node_gvt = DBL_MAX;
			GVT_run = true;
		}
	}
}

int 
percent_comp_ret () {return percent_complete;}

void
finish_gvt(tw_pe * me)
{
	g_tw_gvt_done++;
	me -> interval_counter = g_tw_gvt_interval;
	me -> trans_msg_ts = DBL_MAX;	

	me -> phase = phase_a;
}

void
tw_gvt_force_update(tw_pe *me) 
{
	gvt_force++;
	me -> interval_counter = 0;
    //fprintf(stderr, "GVT forced\n");
}

static const tw_optdef gvt_opts [] =
{
	TWOPT_GROUP("ROSS MPI GVT"),
	TWOPT_UINT("gvt-interval", g_tw_gvt_interval, "GVT Interval"),
	TWOPT_END()
};

const tw_optdef *
tw_gvt_setup(void)
{
    return gvt_opts;
}

void
tw_gvt_stats(FILE * f)
{
	fprintf(f, "\nTW GVT Statistics: Wait Free\n");
	fprintf(f, "\t%-50s %11d\n", "GVT Interval", g_tw_gvt_interval);
	fprintf(f, "\t%-50s %11d\n", "Batch Size", g_tw_mblock);
	fprintf(f, "\t%-50s %11d\n", "Forced GVT", gvt_force);
	fprintf(f, "\t%-50s %11d\n", "Total GVT Computations", g_tw_gvt_done);
}

void
tw_gvt_start() { }
