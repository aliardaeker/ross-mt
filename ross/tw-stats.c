#include <ross.h>

static void
show_lld(const char *name, tw_stat v)
{
	printf("\t%-50s %11lld\n", name, v);
	//fprintf(g_tw_csv, "%lld,", v);
}

static void
show_4f(const char *name, double v)
{
	printf("\t%-50s %11.4lf\n", name, v);
	//fprintf(g_tw_csv, "%.4lf,", v);
}

static void
show_2f(const char *name, double v)
{
	printf("\t%-50s %11.2f %%\n", name, v);
	//fprintf(g_tw_csv, "%.2f,", v);
}

static void
show_1f(const char *name, double v)
{
	printf("\t%-50s %11.1f\n", name, v);
	//fprintf(g_tw_csv, "%.2f,", v);
}

#ifdef DDS 
static void
show_0f(const char *name, double v)
{
	printf("\t%-50s %11.0f\n", name, v);
}

unsigned int accum_blocked_counter = 0;
#endif

tw_statistics s;

void
tw_stats(tw_pe * me, int n)
{
	if (me -> id == 0) bzero(&s, sizeof(s));
	tw_net_barrier(me);	
  
	pthread_mutex_lock(&stat_lock);
	tw_pe	* pe = me;
	tw_kp	* kp;
	tw_lp	* lp = NULL;
	//fprintf(stderr, "%lu summing stats\n", me -> id); 0, 1, 2, 3

	tw_wtime rt;
	tw_wall_sub(&rt, &me -> end_time, &me -> start_time); 		

	s.s_max_run_time = max(s.s_max_run_time, tw_wall_to_double(&rt)); // max wall clock run time
	s.s_total = max(s.s_total, pe -> stats.s_total); 				  // max cpu run time
	s.s_total_all += pe -> stats.s_total;		

	for(int i = 0; i < g_tw_nkp; i++)
	{
		kp = tw_getkp(i);
		s.s_nevent_processed += kp -> s_nevent_processed; // Number of events processed
		s.s_e_rbs += kp -> s_e_rbs;			  // Number of rollbacks
			
		s.s_rb_secondary += kp -> s_rb_secondary;
		s.s_rb_total += kp -> s_rb_total;
	}

	for(int i = 0; i < g_tw_nlp; i++)
	{
		lp = tw_getlp(i);
		if (lp->type.final) (*lp->type.final) (lp->cur_state, lp);
	}

	s.s_event_process += pe->stats.s_event_process; // CPU time for event processing
	s.s_pq += pe->stats.s_pq;
	s.s_rollback += pe->stats.s_rollback;
	s.s_cancel_q += pe->stats.s_cancel_q;
	s.s_event_abort += pe->stats.s_event_abort;

	s.s_net_read += pe->stats.s_net_read; 	// CPU time for core simulation tasks
	s.s_gvt_func += pe -> stats.s_gvt_func;
	s.s_sched_event += pe -> stats.s_sched_event;
	s.s_cancel_event += pe -> stats.s_cancel_event;
	s.s_batch_event += pe -> stats.s_batch_event;

	#ifdef DDS 
	s.read_msg += pe -> stats.read_msg;
	accum_blocked_counter += pe -> blocked_counter; 
	#endif

	s.s_gvt += pe->stats.s_gvt;		// CPU time for GVT related tasks
	s.s_fossil_collect += pe->stats.s_fossil_collect;
	s.force_gvt += pe -> gvt_force;	

	s.s_net_events = s.s_nevent_processed - s.s_e_rbs;  // number of committed events, accumulated
	pthread_mutex_unlock(&stat_lock);
	tw_net_barrier(me);	

	if (!tw_ismaster()) return;

	fprintf(stdout, "\n");
	show_4f("Wall Clock Run Time (sec) ", (double) s.s_max_run_time);  // wall clock run time (max)

	printf("\nTW Library Statistics:\n");
	show_lld("Total Events Processed", s.s_nevent_processed); // number of total events 
	show_lld("Events Rolled Back", s.s_e_rbs);		// number of rollbacks
	show_lld("Net Events Processed", s.s_net_events); // number of committed events
	
	printf("\n");
    double rate = ((double) s.s_net_events / s.s_max_run_time);
	show_1f("Event Rate (net events / sec)", rate);

	double eff = 100.0 * ((double) s.s_net_events / (double) s.s_nevent_processed);
	show_2f("Efficiency", eff);
        
	printf("\nAccumulated CPU Times Among Threads (in secs at %1.4lf GHz):\n", g_tw_clock_rate / 1000000000.0);
	show_4f("Run Time", (double) s.s_total_all / g_tw_clock_rate);   // cpu run time (total)
	
	show_4f("Event Read", (double) s.s_net_read / g_tw_clock_rate);
	show_4f("GVT Function", (double) s.s_gvt_func / g_tw_clock_rate);
	show_4f("Sched Event", (double) s.s_sched_event / g_tw_clock_rate);
	show_4f("Cancel Function", (double) s.s_cancel_event / g_tw_clock_rate);
	show_4f("Batch Event", (double) s.s_batch_event / g_tw_clock_rate);

	printf("\n");	
	show_4f("Event Processing", (double) s.s_event_process / g_tw_clock_rate);
	show_4f("Priority Queue (enq/deq)", (double) s.s_pq / g_tw_clock_rate);
	show_4f("Event Cancel", (double) s.s_cancel_q / g_tw_clock_rate);
	show_4f("Primary Rollbacks", (double) s.s_rollback / g_tw_clock_rate);
	show_4f("Event Abort", (double) s.s_event_abort / g_tw_clock_rate);
	
	#ifdef DDS 
	printf("\nDDS statistics:\n");
	show_4f("Time Spent on Read Message Counts", (double) s.read_msg / g_tw_clock_rate);
	show_0f("Accumulated number of sched outs", (double) accum_blocked_counter);
	show_0f("Threshold to be blocked", (double) thres_blocked());
	//show_0f("Threshold to be awaken", (double) thres_awaken());
	
	//show_0f("Total number of Unlock All", (double) get_unlock_all_counter());
	//show_0f("Total number of Unlock Rest", (double) get_unlock_rest_counter());
	#endif
	
	printf("\n");
	show_4f("GVT Round Total", (double) s.s_gvt / g_tw_clock_rate);
	
	#ifdef DDS
	show_4f("GVT Round Total / # GVTs", (double) (s.s_gvt / get_gvt_count()) / g_tw_clock_rate);
	#else
	show_4f("GVT Round Total / # GVTs", (double) (s.s_gvt / g_tw_gvt_done) / g_tw_clock_rate);
	#endif

	show_4f("Fossil Collect", (double) s.s_fossil_collect / g_tw_clock_rate);	

	FILE * fp = fopen("./output/rates.txt", "a");
    fprintf(fp, "%f\n", rate);
	fclose(fp);

	fp = fopen("./output/eff.txt", "a");
    fprintf(fp, "%f\n", eff);
	fclose(fp);

	tw_gvt_stats(stdout);
}
