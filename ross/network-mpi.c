#include <ross.h>

//#define MAX_NODES 256
//#define MAX_NODES 1024
#define MAX_NODES 4096

#define EVENT_TAG 1
#define EVENT_SIZE(e) (g_tw_event_msg_sz)

typedef struct 
{
		// This points to the thread specific inq location so that when this is written, inq is also updated.
        tw_eventq * outq_ptr; 
        pthread_mutex_t mutex;
} g_mt_outq;

// For mt_send
static g_mt_outq mt_outq_ptr[MAX_NODES];

// For mt_receive
static __thread tw_eventq inq;

//static pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;

void init_mt_ouq_ptr()
{
	inq.size = 0;
	inq.head = inq.tail = NULL;
	pthread_mutex_init(&(mt_outq_ptr[g_tw_tid].mutex), NULL);
		
	mt_outq_ptr[g_tw_tid].outq_ptr = &inq;
}

void destroy_mt_ouq_ptr()
{
	pthread_mutex_destroy(&(mt_outq_ptr[g_tw_tid].mutex));
}

void
tw_net_init(int *argc, char ***argv,int thread_id)
{
 	g_tw_masternode = 0;
	g_tw_pid = 0;

  	g_tw_mynode = thread_id;
	g_tw_tid = thread_id; 

	init_mt_ouq_ptr();
}

tw_stime
tw_inq_minimum(tw_pe * me)
{
	tw_stime m = DBL_MAX;
	tw_event * e;

	e = inq.head;

	while (e) 
	{
    		if (m > e -> recv_ts) m = e->recv_ts;
    		e = e -> next;
  	}

	return m;
}

tw_stime
tw_net_minimum(tw_pe *me)
{
	tw_stime m = DBL_MAX;
	tw_event * e;
	
	pthread_mutex_lock(&(mt_outq_ptr[g_tw_tid].mutex));
  	e = mt_outq_ptr[g_tw_tid].outq_ptr -> head;
	pthread_mutex_unlock(&(mt_outq_ptr[g_tw_tid].mutex));

	while (e) 
	{
    		if (m > e -> recv_ts) m = e->recv_ts;
    		e = e -> next;
  	}

	return m;
}

unsigned int 
tw_nnodes(void)
{
	return g_tw_n_threads;
}

void
tw_net_start(void)
{
  	tw_pe_create(1);

  	tw_pe_init(0, g_tw_mynode);

  	g_tw_pe[0] -> hash_t = tw_hash_create();
}

void
tw_net_abort(void)
{
	exit(1);
}

void
tw_net_stop(void)
{
	destroy_mt_ouq_ptr();
}

void
net_barrier()
{
	barrier_sync(&g_barrier, g_tw_tid);
}

void
tw_net_barrier(tw_pe * pe)
{
	barrier_sync(&g_barrier, g_tw_tid);
}

void net_send_finish(tw_pe *me, tw_event *e, char * buffer)
{
	tw_event_free(me,e);
}

#ifdef DDS 
//static unsigned int unlock_all_counter = 0;
//static unsigned int unlock_rest_counter = 0;

static unsigned int inq_size_threshold = 0;

//static unsigned long long poll_threshold = 100;
//static unsigned int * to_activate;

void
init_master_check (unsigned int n)
{
	//barrier_blocked = (bool *) calloc(n, sizeof(bool));
	//tid_table = (pthread_t *) calloc(n, sizeof(pthread_t));

	affinity_table = (int *) malloc(n * sizeof(int));
	affinity_table_inv = (int *) malloc(n * sizeof(int));
	sys_tid_table = (unsigned *) calloc(n, sizeof(unsigned));

	//to_activate = (unsigned int *) calloc(n, sizeof(unsigned int));
	
	//blocked_threads = (bool *) calloc(n, sizeof(bool));
	blocked_threads = (struct bool_padded *) calloc(n, sizeof(struct bool_padded));

	//locks = (sem_t *) malloc(n* sizeof(sem_t));
	locks = (struct sem_padded *) malloc(n * sizeof(struct sem_padded));

	for (int i = 0; i < n; i++) 
	{
		//if (sem_init(&locks[i], 0, 0)) tw_error(TW_LOC, "Mutex Init Failed\n\n"); 
		if (sem_init(&locks[i].sem, 0, 0)) tw_error(TW_LOC, "Mutex Init Failed\n\n"); 

		// 0 for threads within a process, 0 for initial sem value
	}
	master_done = 0;
}

void 
set_affinity ()
{
	int smt = 0, core = 0;
	bool next_tid = false;
	//fprintf(stderr, "*** SET ***\n");
	
	for (int tid = 0; tid < g_tw_n_threads; tid++)
	{
		if (!blocked_threads[tid].blocked)
		{
			if (affinity_table_inv[tid] >= 0) continue;
			next_tid = false;

			//fprintf(stderr, "	Try %d to set\n", tid);
			for (; smt < 4; smt++)
			{
				if (core == 64) core = 0;
				for (; core < 64; core++)
				{
					int target_cpu = core * 4 + smt;

					if (affinity_table[target_cpu] == -1) 
					{
						affinity_table[target_cpu] = tid;
						affinity_table_inv[tid] = target_cpu;
						target_cpu = target_cpu % 256;
					
						cpu_set_t cpuset;
		                CPU_ZERO(&cpuset);
        		        CPU_SET(target_cpu , &cpuset);
                		if (CPU_ISSET(target_cpu , &cpuset) == 0)
                		{
                    		fprintf(stderr, "CPU_ISSET fails\n");
                   			exit(1);
                		}

		                unsigned sys_tid = sys_tid_table[tid];

		                int s = sched_setaffinity(sys_tid, sizeof(cpuset), &cpuset);
        		        if (s != 0)
                		{
                    		fprintf(stderr, "sched_setaffinity. tid: %d cpu: %d, error: %d\n", sys_tid, target_cpu, s);
                   			exit(1);
                		}
						//fprintf(stderr, "%d set on %d\n", tid, target_cpu);
						next_tid = true;
						break;
					}
				}

				if (next_tid) break;
			}
		}
	}	
}


inline void
reactivate_all ()
{
	for (int i = 0; i < g_tw_n_threads; i++)
	{
		if (blocked_threads[i].blocked) 
		//if (blocked_threads[i]) 
		{
			//blocked_threads[i] = false;
			increment_active_thread_count();

	        if (sem_post(&locks[i].sem)) tw_error(TW_LOC, "Sem Post Failed\n\n");
	        //if (sem_post(&locks[i])) tw_error(TW_LOC, "Sem Post Failed\n\n");
		}
	}
}

inline void
reactivate ()
{
	for (int i = 0; i < g_tw_n_threads; i++)
	{
		if (blocked_threads[i].blocked && mt_outq_ptr[i].outq_ptr -> size > inq_size_threshold)
		//if (blocked_threads[i] && mt_outq_ptr[i].outq_ptr -> size > inq_size_threshold)
		{
			//blocked_threads[i] = false;
			increment_active_thread_count();

	        if (sem_post(&locks[i].sem)) tw_error(TW_LOC, "Sem Post Failed\n\n");
	        //if (sem_post(&locks[i])) tw_error(TW_LOC, "Sem Post Failed\n\n");
		}
	}
}

inline void
pre_reactivate ()
{
	int c = 0;
	for (int i = 0; i < g_tw_n_threads; i++)
	{
		if (blocked_threads[i].blocked && mt_outq_ptr[i].outq_ptr -> size > inq_size_threshold) c++;
		//if (blocked_threads[i] && mt_outq_ptr[i].outq_ptr -> size > inq_size_threshold) c++;
	}
	increase_active_thread_count(c);
}

inline void
real_reactivate ()
{
	for (int i = 0; i < g_tw_n_threads; i++)
	{
		if (blocked_threads[i].blocked && mt_outq_ptr[i].outq_ptr -> size > inq_size_threshold)
		//if (blocked_threads[i] && mt_outq_ptr[i].outq_ptr -> size > inq_size_threshold)
		{
			//blocked_threads[i] = false;

	        if (sem_post(&locks[i].sem)) tw_error(TW_LOC, "Sem Post Failed\n\n");
	        //if (sem_post(&locks[i])) tw_error(TW_LOC, "Sem Post Failed\n\n");
		}
	}
}

/*
void
unlock_rest ()
{
	//fprintf(stderr, "UNLOCK REST\n");
	//if (controller_busy())
    {
		for (int i = 0; i < g_tw_n_threads; i++)
		{
			if (blocked_threads[i])
        	{
				unlock_rest_counter++; 
				#ifdef DEBUG
				fprintf(stderr, "		%d i rest\n", i);
				#endif

				increment_active_thread_count(1);
				blocked_threads[i] = false;
				//set_affinity(i);

				if (sem_post(&locks[i])) tw_error(TW_LOC, "Sem Post Failed\n\n"); 

				#ifdef DEBUG
				fprintf(stderr, "		%d i rests\n", i);
				#endif
			}
		}
		//controller_done();
	}	
}

void
unlock_all ()
{
	//fprintf(stderr, "UNLOCK ALL\n");
	unlock_all_counter++; 

	maximize_active_thread_count();
	for (int i = 0; i < g_tw_n_threads; i++) blocked_threads[i] = false;
	for (int i = 0; i < g_tw_n_threads; i++) sem_post(&locks[i]); 
}
*/



/*
void 
master_check ()
{
	//unsigned long long poll_counter = 0;
	bool activate = false;
	int counter = 0;

	while (1)
	{
		if (master_done == g_tw_n_threads) break;
		if (master_done > 0) unlock_rest ();	
		if (!num_active_threads()) unlock_all ();	

		if (!activate && num_active_threads() < g_tw_n_threads) 
		//if (num_active_threads() < g_tw_n_threads) 
		{
			for (int i = 0; i < g_tw_n_threads; i++)
			{
				if (blocked_threads[i] && mt_outq_ptr[i].outq_ptr -> size > inq_size_threshold)
				{
					if (!activate) activate = true;
					to_activate[counter] = i;
					counter++;
				}
			}
		} 
					
		if (activate)
		{
			//if (poll_counter++ >= poll_threshold && controller_busy())
			if (controller_busy())
			{
				for (int i = 0; i < counter; i++) sem_post(&locks[to_activate[i]]);
				for (int i = 0; i < counter; i++) blocked_threads[to_activate[i]] = false;
				increase_active_thread_count(counter);
					
				controller_done();
				counter = 0;
				activate = false;
				//poll_counter = 0;
			}
		}

		//if (num_active_threads() < g_tw_n_threads && gvt_not_inprogress())
		//if (num_active_threads() < g_tw_n_threads && poll_counter++ >= poll_threshold)
		//if (num_active_threads() < g_tw_n_threads) 
		
		// old reactivation ..
	}

	for (int i = 0; i < g_tw_n_threads; i++) sem_destroy(&locks[i]);

	free(to_activate);
	free(blocked_threads);
	free(locks);
}
*/

/*
		{
			if (controller_busy())
			//while (!controller_busy()) {}
			{
				for (int i = 0; i < g_tw_n_threads; i++)
				{
					if (blocked_threads[i] && mt_outq_ptr[i].outq_ptr -> size > inq_size_threshold)
					{
						increment_active_thread_count();
						blocked_threads[i] = false;
						//set_affinity(i, 1, -1);

	            		if (sem_post(&locks[i])) tw_error(TW_LOC, "Sem Post Failed\n\n");
					}
				}

				controller_done();
				//poll_counter = 0;
			}
			poll_counter = 0;
		}
*/

inline int
outq_size ()
{
	return mt_outq_ptr[g_tw_tid].outq_ptr -> size;
}

inline int
inq_size ()
{
	return inq.size;
}

inline bool
queues_empty (tw_pe * me)
{
	if (!inq_size() && !outq_size() && !(me -> event_q.size) && !tw_pq_get_size(me -> pq) && !(me -> cancel_q)) return true;
	return false;	
}

inline void 
check_msg_counts (tw_pe * me)
{	
	if (queues_empty(me)) me -> zero_counter++; 
	else me -> zero_counter = 0;

	if (me -> zero_counter > zero_counter_threshold) me -> blocked = true;
}

unsigned int
thres_blocked()
{
	return zero_counter_threshold; 
}

unsigned int
thres_awaken()
{
	return inq_size_threshold; 
}
#endif

void
recv_finish(tw_pe * me, tw_event *e, char * buffer)
{
	e -> dest_lp = tw_getlocal_lp((tw_lpid) e -> dest_lp);
	tw_pe * dest_pe = e -> dest_lp -> pe;
  	
	//if (g_tw_tid == e -> send_pe) exit(1);
	//if (g_tw_tid != dest_pe -> id) exit(1);

	#if defined(ROSS_GVT_mpi_allreduce) 
	//fprintf(stderr, "%d recv from %lld\n", g_tw_tid, e -> send_pe);
	me -> s_nwhite_recv++;	
	#endif

	if (e -> send_pe > tw_nnodes() - 1) tw_error(TW_LOC, "bad sendpe_id: %d", e->send_pe);

  	e -> cancel_next = NULL;
  	e -> caused_by_me = NULL;
  	e -> cause_next = NULL;

  	if (e -> recv_ts < me -> GVT) 
	{
		#ifdef DDS
			pthread_mutex_lock(&lock);
			//FILE * fp = fopen("./output/rates.txt", "a");
    		//fprintf(fp, "STRAGGLER: %d %d %u\n", g_tw_n_threads, g_tw_gvt_interval, zero_counter_threshold);
			//fclose(fp);
	
			fprintf(stderr, "%lld RECEIVES STRAGGLER FROM %lld. (ts: %.0lf, gvt: %.0f) - (%d, %d - %d %d %d)\n", 
			me -> id, e -> send_pe, e -> recv_ts, me -> GVT, me -> blocked_gvt, me -> awaken_gvt, 
			e -> state.cancel_q, e -> state.owner, e -> state.remote);
			exit(1);
			pthread_mutex_unlock(&lock);
		#else
			fprintf(stderr, "%lld RECEIVES STRAGGLER FROM %lld. (ts: %.0lf, gvt: %.0f, E id: %d)\n", 
			me -> id, e -> send_pe, e -> recv_ts, me -> GVT, e -> event_id);
			exit(1);
		#endif
	}

  	if (tw_gvt_inprogress(me)) me -> trans_msg_ts = min(me -> trans_msg_ts, e -> recv_ts);

	// if cancel event, retrieve and flush
  	// else, store in hash table
  	if (e -> state.cancel_q)
    {
		//DJ: find an event in the hash table for which this cancel event is made 
		tw_event * cancel = tw_hash_remove(me -> hash_t, e, e -> send_pe);

      	// NOTE: it is possible to cancel the event we
      	// are currently processing at this PE since this
      	// MPI module lets me read cancel events during 
      	// event sends over the network.

      	cancel -> state.cancel_q = 1;
      	cancel -> state.remote = 0;
			
		//Put orig event in the cancelq
      	cancel -> cancel_next = dest_pe -> cancel_q;
      	dest_pe -> cancel_q = cancel;

		//DJ:cancel event done it's job now we can free it 
		tw_event_free(me, e);
      
		//fprintf(stderr,"cancel %llu, %llu\n", me -> id, dest_pe -> id); //0-0, 1-1, 2-2, 3-3
     	
		return;
    }

	tw_hash_insert(me -> hash_t, e, e -> send_pe);
  	e -> state.remote = 1;

  	if (me == dest_pe && e -> dest_lp -> kp -> last_time <= e->recv_ts) 
	{
    		/* Fast case, we are sending to our own PE and
     		* there is no rollback caused by this send.
     		*/
    		tw_pq_enqueue(dest_pe -> pq, e);
			
			//fprintf(stderr,"%llu, %llu\n", me -> id, dest_pe -> id); //0-0, 1-1, 2-2, 3-3
    		
			return;
  	}
  
  	if(tw_node_eq(&me->node, &dest_pe->node)) 
	{
    		/* Slower, but still local send, so put into top
     		* of dest_pe->event_q. 
     		*/
			//fprintf(stderr,"%llu, %llu\n", me -> id, dest_pe -> id); //0-0, 1-1, 2-2, 3-3
    		
			e -> state.owner = TW_pe_event_q;
    		tw_eventq_push(&dest_pe->event_q, e);
    		return;
  	}

  	/* Never should happen; MPI should have gotten the
  	 * message to the correct node without needing us
   	 * to redirect the message there for it.  This is
   	 * probably a serious bug with the event headers
   	 * not being formatted right.
   	 */
  	
	tw_error(
	   TW_LOC,
	   "Event recived by PE %u but meant for PE %u",
	 	me->id,
	   dest_pe->id);
}

//Moves events from outq to appropriate PE queue
//This will involve invoking receive_finish for each event in the outq
static void mt_receive(tw_pe * me)
{
	int status;
	
	while (inq.size > 0)
	{
		status = pthread_mutex_lock(&(mt_outq_ptr[g_tw_tid].mutex));
		if (status != 0) printf("In mt_receive: Lock failed error-code=%d\n",status);	
		tw_event * e = tw_eventq_pop(&inq); // size--;

		if (e == NULL)
		{ 
			status = pthread_mutex_unlock(&(mt_outq_ptr[g_tw_tid].mutex));	
			fprintf(stderr, "Null msg\n");
			continue;
		}

		recv_finish(me, e, NULL);
			
		status = pthread_mutex_unlock(&(mt_outq_ptr[g_tw_tid].mutex));	
		if(status != 0) printf("In mt_receive: UnLock failed error-code=%d\n",status);

		me -> stats.s_nread_network++;

		#ifdef ROSS_GVT_mattern
		mattern_receive_track(me, e);
		#endif
	}
}

void net_recv_finish(tw_pe *me, tw_event *e, char * buffer)
{
	int status;
	
	status = pthread_mutex_lock(&(mt_outq_ptr[e -> recv_pe].mutex));
	if (status) printf("Error\n");
     	
	tw_eventq_unshift(mt_outq_ptr[e -> recv_pe].outq_ptr, e);  
	
	status = pthread_mutex_unlock(&(mt_outq_ptr[e -> recv_pe].mutex));
	if (status) printf("Error\n");
}

static void mt_send(tw_pe * me, tw_event *e, int dest_node_id)
{
	int status;
	tw_event * cp_e = NULL;
	
	if (!e -> state.cancel_q) e -> event_id = (tw_eventid) ++me->seq_num;

	e -> send_pe = (tw_peid) g_tw_mynode; // Sender: g_tw_tid
	e -> state.owner = e -> state.cancel_q ? TW_net_acancel : TW_net_asend; // If cancel event, owner: net_acancel

	// Dummy event parameters doesn't have any significance
	// Take it from sender pe's free_q or malloc
	cp_e = mt_tw_event_new(dest_node_id, e->recv_ts, e->src_lp); 

	memcpy(cp_e, e, g_tw_event_msg_sz); //make cp_e identical to e

	#ifdef ROSS_GVT_mattern
	mattern_send_track(me, cp_e, e, dest_node_id);
	#endif

	status = pthread_mutex_lock(&(mt_outq_ptr[dest_node_id].mutex));	
	if (status!=0) printf("mt_send lock failed! error-code=%d\n",status);

	// dest_node_id: thread id in a machine, numa_node_id: machine rank
	tw_eventq_unshift(mt_outq_ptr[dest_node_id].outq_ptr, cp_e); // size++
	
	status = pthread_mutex_unlock(&(mt_outq_ptr[dest_node_id].mutex));
	if (status!=0) printf("mt_send unlock failed! error-code=%d\n",status);

	#if defined(ROSS_GVT_mpi_allreduce)
	//fprintf(stderr, "	%d sent to %d\n", g_tw_tid, dest_node_id);
	//fprintf(stderr, "	sent to %d - %d, %d, %d\n", dest_node_id, e -> state.cancel_q, e -> state.owner, e -> state.remote);
	me -> s_nwhite_sent++;
	#endif
	
	if (e -> state.owner == TW_net_asend) 
	{
		/* Event finished transmission and was not cancelled.
		 * Add to our sent event queue so we can retain the
		 * event in case we need to cancel it later.  Note it
		 * is currently in remote format and must be converted
		 * back to local format for fossil collection.
		 */
		e->state.owner = TW_pe_sevent_q;
		
		if (g_tw_synchronization_protocol == CONSERVATIVE) tw_event_free(me, e);
		return;
	}

	if (e->state.owner == TW_net_acancel) 
	{
		/* We just finished sending the cancellation message
		 * for this event.  We need to free the buffer and
		 * make it available for reuse.
		 */
		
		//DJ: It means this is a cancelation message we can free this event immidiately  
		tw_event_free(me, e);
		return;
	}

  	/* Never should happen, not unless we somehow broke this
   	* module's other functions related to sending an event.
   	*/

  	tw_error(
           	TW_LOC,
           	"Don't know how to finish send of owner=%u, cancel_q=%d",
           	e->state.owner,
           	e->state.cancel_q);
}

/*
 * NOTE: Chris believes that this network layer is too aggressive at
 * reading events out of the network.. so we are modifying the algorithm
 * to only send events when tw_net_send it called, and only read events
 * when tw_net_read is called.
 */

void
tw_net_read(tw_pe *me)
{
	mt_receive(me);
}

void
tw_net_send(tw_event * e, int cancel_msg)
{
	tw_node * dest_node = NULL;
	int dest_node_id = 0;
	tw_pe * me = e -> src_lp -> pe;

	e -> state.remote = 0;
    if (e == me -> abort_event) tw_error(TW_LOC, "sending abort event!");
        
	dest_node = tw_net_onnode((*e -> src_lp -> type.map) ((tw_lpid) e -> dest_lp));
    dest_node_id = *dest_node;

	mt_send(me, e, dest_node_id);
}

void
tw_net_cancel(tw_event *e)
{
	tw_pe *src_pe = e->src_lp->pe;
  
	switch (e->state.owner) 
	{
  		case TW_pe_sevent_q:
    		// Way late; the event was already sent and is in
     		// our sent event queue.  Mark it as a cancel and
     		// place it at the front of the outq.
     			
			//DJ: This is the only case for muitithreaded implementation
			//Because above two are not present in this case
			//Whatever events are generated during processing are sent immidiately
			//So they must be cancelled by sending explicit cancel message

    		e->state.cancel_q = 1;

			//Change this
    		//tw_eventq_unshift(&outq, e);
			//DJ:
			
			src_pe -> stats.s_cancel++;
			tw_net_send(e, 1);	
    		break;

  		default:
    			// Huh?  Where did you come from?  Why are we being
     			// told about you?  We did not send you so we cannot
     			// cancel you!
    
			tw_error(
	     			TW_LOC,
	     			"Don't know how to cancel event owned by %u",
	     			e->state.owner);
 	}

	tw_net_read(src_pe);
}

