#include <ross.h>

/* 
 * Get all events out of my event queue and spin them out into
 * the priority queue so they can be processed in time stamp
 * order.  
 */
static void
tw_sched_event_q(tw_pe * me)
{
  	tw_clock	 start;
  	tw_kp		*dest_kp;
  	tw_event	*cev;
  	tw_event	*nev;
  
  	while (me->event_q.size) 
    	{
      		cev = tw_eventq_pop_list(&me->event_q);
      
      		for (; cev; cev = nev) 
		{
	  		nev = cev->next;
	  
	  		if (!cev->state.owner || cev->state.owner == TW_pe_free_q) tw_error(TW_LOC, "no owner!");
	  
	  		if (cev->state.cancel_q)
	    		{
	      			cev->state.owner = TW_pe_anti_msg;
	      			cev->next = cev->prev = NULL;
	      			continue;
	    		}
	  
	  		switch (cev->state.owner) 
	    		{
	    			case TW_pe_event_q:
	      				dest_kp = cev->dest_lp->kp;
	      
	      				if (dest_kp->last_time > cev->recv_ts) 
					{
		  				/* cev is a straggler message which has arrived
		   				* after we processed events occuring after it.
		   				* We need to jump back to before cev's timestamp.
		   				*/
						
						#ifdef rollback_stats
						//02/20/2012
		  				me->stats.rollback_distance+=(double)(me->LVT - cev->recv_ts);
						me->stats.number_distance++;
						#endif
			
						start = tw_clock_read(); // Rollback

						#ifdef rollback_stats 
						tw_kp_rollback_to(dest_kp, cev->recv_ts,cev);
						#else
						tw_kp_rollback_to(dest_kp, cev->recv_ts);
						#endif

						me -> stats.s_rollback += tw_clock_read() - start;
					}
	      
					start = tw_clock_read();
	      				tw_pq_enqueue(me->pq, cev);
	      				me->stats.s_pq += tw_clock_read() - start; // Priority Queue push
	      				break;
	      
	    			default:
	      				tw_error(TW_LOC,
		       			"Event in event_q, but owner %d not recognized",
		      	 		cev->state.owner);
	    		}
		}
	}
}

/*
 * OPT: need to link events into canq in reverse order so 
 *      that when we rollback the 1st event, we should not
 *	need to do any further rollbacks.
 */
static void
tw_sched_cancel_q(tw_pe * me)
{
        tw_clock	 start;
	tw_event 	*cev;
	tw_event	*nev;

	start = tw_clock_read();
	
	while (me->cancel_q) 
	{
		cev = me->cancel_q;
		me->cancel_q = NULL;

		for (; cev; cev = nev) 
		{
			nev = cev->cancel_next;

			if (!cev->state.cancel_q) tw_error(TW_LOC, "No cancel_q bit on event in cancel_q");

			if (!cev->state.owner || cev->state.owner == TW_pe_free_q) tw_error(TW_LOC, "Cancelled event, no owner!");

			switch (cev->state.owner) 
			{
			case TW_pe_event_q:
				/* This event hasn't been added to our pq yet and we
				 * have not officially received it yet either.  We'll
				 * do the actual free of this event when we receive it
				 * as we spin out the event_q chain.
				 */
				tw_eventq_delete_any(&me->event_q, cev);

				tw_event_free(me, cev);
				break;

			case TW_pe_anti_msg:
				tw_event_free(me, cev);
				break;

			case TW_pe_pq:
				/* Event was not cancelled directly from the event_q
				 * because the cancel message came after we popped it
				 * out of that queue but before we could process it.
				 */
				tw_pq_delete_any(me->pq, cev);
				tw_event_free(me, cev);
				break;

			case TW_kp_pevent_q:
				/* The event was already processed. 
				 * SECONDARY ROLLBACK
				 */

				tw_kp_rollback_event(cev);

				tw_event_free(me, cev);
				break;

			default:
				tw_error(TW_LOC,
					"Event in cancel_q, but owner %d not recognized",
					cev->state.owner);
			}
		}
	}

	me->stats.s_cancel_q += tw_clock_read() - start; // Cancel Queue
}

static void
tw_sched_batch(tw_pe * me)
{
        tw_clock	 start;
	unsigned int	 msg_i;

	/* Process g_tw_mblock events, or until the PQ is empty
	 * (whichever comes first). 
	 */
	for (msg_i = g_tw_mblock; msg_i; msg_i--) 
	{
		tw_event *cev;
		tw_lp *clp;
		tw_kp *ckp;

		/* OUT OF FREE EVENT BUFFERS.  BAD.
		 * Go do fossil collect immediately.
		 */

		//if (me -> id == 0) fprintf(stderr, "%f, %lu, %d\n", me -> GVT, me -> free_q.size, g_tw_gvt_threshold);

		if (me -> free_q.size <= g_tw_gvt_threshold) 
		{
			// Mattern prints this a lot. Still a problem. GVT proceeds but there might be a bug.
			// Barrier does not do that.

			//if (g_tw_pid == 0 && g_tw_tid == 1) 
			{
				//fprintf(stderr, "%u, %u -> OUT OF BUFFER, %.0f, free q size: %lu, thresold: %d\n", 
				//		g_tw_pid, g_tw_tid, me -> GVT, me -> free_q.size, g_tw_gvt_threshold);
			}		

			//jwang
			tw_gvt_force_update(me);
			break;
		}

		start = tw_clock_read();
		if (!(cev = tw_pq_dequeue(me -> pq))) 
		{
			//if (!g_tw_pid && g_tw_tid == 1) fprintf(stderr, "%llu pq empty, %f\n", me -> id, me -> GVT);	
			break;
		}
		me -> stats.s_pq += tw_clock_read() - start; // Priority Queue pop 

		clp = cev -> dest_lp;	
		ckp = clp -> kp;
		me -> cur_event = cev;
		ckp -> last_time = cev -> recv_ts;

		/* Save state if no reverse computation is available */
		if (!clp -> type.revent) tw_error(TW_LOC, "Reverse Computation must be implemented!");

                start = tw_clock_read();
		// Event is sent here
		//tw_stime s = tw_clock_read();
		(*clp -> type.event)(
			clp -> cur_state,
			&cev -> cv,
			tw_event_data(cev),
			clp);
		//me -> stats.s_event_send += tw_clock_read() - s; 
		
		ckp -> s_nevent_processed++;
                me -> stats.s_event_process += tw_clock_read() - start; // Event Processing

        #ifdef disparity_check
		    // For disparity check
		    lvts[g_tw_tid].lvt = cev -> recv_ts;
        #endif
    
		/* We ran out of events while processing this event.  We
		 * cannot continue without doing GVT and fossil collect.
		 */

		if (me -> cev_abort) 
		{
		        start = tw_clock_read();
			me -> stats.s_nevent_abort++;
			me -> cev_abort = 0;

			tw_event_rollback(cev);

			tw_pq_enqueue(me -> pq, cev);

			#ifdef SL_LOCKS
				pthread_mutex_lock(&kp_locks[g_tw_tid]);
				cev = tw_eventq_peek(&ckp -> pevent_q);
				pthread_mutex_unlock(&kp_locks[g_tw_tid]);
			#else		
				cev = tw_eventq_peek(&ckp -> pevent_q);
			#endif	

			ckp -> last_time = cev ? cev -> recv_ts : me -> GVT;
		        me->stats.s_event_abort += tw_clock_read() - start; // Event Abort

			break;
		}

		/* Thread current event into processed queue of kp */
		cev -> state.owner = TW_kp_pevent_q;
		
		#ifdef SL_LOCKS
			pthread_mutex_lock(&kp_locks[g_tw_tid]);
			tw_eventq_unshift(&ckp -> pevent_q, cev);
			pthread_mutex_unlock(&kp_locks[g_tw_tid]);
		#else
			tw_eventq_unshift(&ckp -> pevent_q, cev);
		#endif
	}
}

static void
tw_sched_init(tw_pe * me)
{ 
	(* me -> type.pre_lp_init) (me);
    tw_init_kps(me);
	tw_init_lps(me);
	(* me -> type.post_lp_init) (me);
    
	tw_net_barrier(me);

	/*
	 * Recv all of the startup events out of the network before
	 * starting simulation.. at this point, all LPs are done with init.
	 */
	if (tw_nnodes() > 1)
	{
	    //fprintf(stderr, "%llu tries to init\n", me -> id);		

		tw_net_read(me);
        
        //fprintf(stderr, "   %llu inits\n", me -> id);		
		tw_net_barrier(me);
        
        //fprintf(stderr, "       %llu done\n", me -> id);		
	}

	if (tw_nnodes() > 1) tw_clock_init(me);

	/* This lets the signal handler know that we have started
	 * the scheduler loop, and to print out the stats before
	 * finishing if someone should type CTRL-c
	 */
	if ((tw_nnodes() > 1 || g_tw_npe > 1) &&
		tw_node_eq(&g_tw_mynode, &g_tw_masternode) && 
		me->local_master)
	{
	    if ( g_tw_synchronization_protocol == CONSERVATIVE ) printf("*** START PARALLEL CONSERVATIVE SIMULATION ***\n\n");
	    if ( g_tw_synchronization_protocol == OPTIMISTIC ) printf("*** START PARALLEL OPTIMISTIC SIMULATION ***\n\n");
	}
	else if(tw_nnodes() == 1 && g_tw_npe == 1)
	{
	    // force the setting of SEQUENTIAL protocol
	    if( g_tw_synchronization_protocol!=SEQUENTIAL ) g_tw_synchronization_protocol=SEQUENTIAL;

	    printf("*** START SEQUENTIAL SIMULATION ***\n\n");
	}

	if (me->local_master) g_tw_sim_started = 1;
}

/*************************************************************************/
/* Primary Schedulers -- In order: Sequential, Conservative, Optimistic  */
/*************************************************************************/

void tw_scheduler_sequential(tw_pe * me, int n) {
	
	if(tw_nnodes() > 1) 
		tw_error(TW_LOC, "Sequential Scheduler used for world size greater than 1.");
	
	tw_event *cev;
  	
	tw_sched_init(me);
  	tw_wall_now(&me->start_time);
  	
	while ((cev = tw_pq_dequeue(me->pq))) 
    {
      tw_lp *clp = cev->dest_lp;
      tw_kp *ckp = clp->kp;
      
      me->cur_event = cev;
      ckp->last_time = cev->recv_ts;
      
      (*clp->type.event)(
			 clp->cur_state,
			 &cev->cv,
			 tw_event_data(cev),
			 clp);
      
      if (me->cev_abort)
		tw_error(TW_LOC, "insufficient event memory");
      
      ckp->s_nevent_processed++;
      tw_event_free(me, cev);
    }
  tw_wall_now(&me->end_time);
  
  printf("*** END SIMULATION ***\n\n");
  
  tw_stats(me,n);
  
  (*me->type.final)(me);
}

void
tw_scheduler_conservative(tw_pe * me, int n)
{
  tw_clock start;
  unsigned int msg_i;
  unsigned int round = 0;
  
  tw_sched_init(me);
  tw_wall_now(&me->start_time);
  me->stats.s_total = tw_clock_read();
  
  for (;;)
    {
      if (tw_nnodes() > 1)
	{
	  start = tw_clock_read();
	  tw_net_read(me);
	  me->stats.s_net_read += tw_clock_read() - start;
	}
      
      tw_gvt_step1(me);
      tw_sched_event_q(me);
      tw_gvt_step2(me);
	//printf("Done GVT compuatation\n");
      
      if (me->GVT > g_tw_ts_end)
	break;
      
      // put "batch" loop directly here
      /* Process g_tw_mblock events, or until the PQ is empty
       * (whichever comes first). 
       */
      for (msg_i = g_tw_mblock; msg_i; msg_i--) 
	{
	  tw_event *cev;
	  tw_lp *clp;
	  tw_kp *ckp;
	  
	  /* OUT OF FREE EVENT BUFFERS.  BAD.
	   * Go do fossil collect immediately.
	   */
	  if (me->free_q.size <= g_tw_gvt_threshold) {
            tw_gvt_force_update(me);
	    break;
	  }
	  
	  if(tw_pq_minimum(me->pq) >= me->GVT + g_tw_lookahead)
	    break;
	  
	  start = tw_clock_read();
	  if (!(cev = tw_pq_dequeue(me->pq)))
	    break;
	  me->stats.s_pq += tw_clock_read() - start;
	  
	  clp = cev->dest_lp;
	  ckp = clp->kp;
	  me->cur_event = cev;
	  ckp->last_time = cev->recv_ts;
	  
	  start = tw_clock_read();
	  (*clp->type.event)(
			     clp->cur_state,
			     &cev->cv,
			     tw_event_data(cev),
			     clp);
		
	  ckp -> s_nevent_processed++;
	  me->stats.s_event_process += tw_clock_read() - start;
	  
	  if (me->cev_abort)
	    tw_error(TW_LOC, "insufficient event memory");
	  
	  tw_event_free(me, cev);
	}

        if(me->type.periodic && (++round % g_tw_periodicity))
        {
            (*me->type.periodic)(me);
        }
    }

  
  tw_wall_now(&me->end_time);
  me->stats.s_total = tw_clock_read() - me->stats.s_total;
  
  if((tw_nnodes() > 1 || g_tw_npe > 1) &&
     tw_node_eq(&g_tw_mynode, &g_tw_masternode) && 
     me->local_master)
    printf("*** END SIMULATION ***\n\n");
  
  tw_net_barrier(me);
  
  // call the model PE finalize function
  (*me->type.final)(me);
  
  tw_stats(me,n);
}

void
tw_scheduler_optimistic(tw_pe * me, int n, int * epg, double * reg, double * rem)
{
	//fprintf(stderr, "%llu enters looping %.0f\n", me -> id, me -> GVT);		
	
	if (g_tw_tid == 0)	
	{
		if (pthread_mutex_init(&papi_lock, NULL) != 0) tw_error(TW_LOC, "Mutex init failed\n");
		if (pthread_mutex_init(&stat_lock, NULL) != 0) tw_error(TW_LOC, "Mutex init failed\n");
	}

    tw_clock start;
	tw_sched_init(me);
  
	//fprintf(stderr, "%llu enters %.0f\n", me -> id, me -> GVT);		
	
    tw_wall_now(&me -> start_time);
	me -> stats.s_total = tw_clock_read(); // Total cpu time

	
	#ifdef switch_sim_mode 
	    int orig_epg = *epg;
	    double orig_reg = *reg;
	    double orig_rem = *rem;
	
	    // Sim should start with computation dominated case
        // These for alternating
	    int comm_not_switched = 1;		
	    int comp_not_switched = 0;		
	
	    double comp_interval = 0.1;
	    double comm_interval = 0.2;

	    double orig_comp_interval = comp_interval;
	    double orig_comm_interval = comm_interval;

        // These just switch once from comp to comm
	    //int not_switched = 1;
	    //double switch_rate = 0.9;
    #endif

	int events = PAPI_NULL;
    long long int values [2];

	pthread_mutex_lock(&papi_lock);
	if (PAPI_thread_init(& pthread_self) != PAPI_OK) {printf("Thread Support Broken\n"); exit(1);}
	pthread_mutex_unlock(&papi_lock);

    if (PAPI_create_eventset(&events) != PAPI_OK) {printf("event create error !!!\n"); exit(1);}
    if (PAPI_add_event(events, PAPI_TOT_INS) != PAPI_OK) {printf("event add error !!!\n"); exit(1);} // Total instructions
    if (PAPI_add_event(events, PAPI_TOT_CYC) != PAPI_OK) {printf("event add error !!!\n"); exit(1);}

	if (PAPI_start(events) != PAPI_OK) {printf("event start error !!!\n"); exit(1);}

	for (;;)
	{	
	    //fprintf(stderr, "%llu in net read %.0f\n", me -> id, me -> GVT);		
      	
		if (tw_nnodes() > 1)
      	{
	  		start = tw_clock_read(); // Event read
	  		tw_net_read(me);
	  		me -> stats.s_net_read += tw_clock_read() - start; // Event Read
      	}
		
		//if (!g_tw_pid && !g_tw_tid) fprintf(stderr, "%d loops\n", g_tw_tid);		
		//else if (g_tw_pid && !g_tw_tid) fprintf(stderr, "	%d loops %d\n", g_tw_tid, interval_counter);	
      		
		#ifdef ROSS_GVT_mpi_allreduce
	  		start = tw_clock_read();
        		tw_gvt_step1(me);
	  		me -> stats.s_gvt_func += tw_clock_read() - start; // GVT Mattern
		#endif
			
		//if (!g_tw_pid) fprintf(stderr, "%llu loops %.0f\n", me -> id, me -> GVT);		
		//else fprintf(stderr, "	%llu loops %.0f\n", me -> id, me -> GVT);	

	  	start = tw_clock_read();
		sched_event(me);
	  	me -> stats.s_sched_event += tw_clock_read() - start; // Sched Event

		#ifdef ROSS_GVT_mpi_allreduce
	  		start = tw_clock_read();
        		tw_gvt_step2(me);
	  		me -> stats.s_gvt_func += tw_clock_read() - start; // GVT Mattern
		#endif

		#ifdef ROSS_GVT_mattern_orig
	  		start = tw_clock_read();
			if (!(me -> interval_counter)) tw_gvt_step1(me);
            else (me -> interval_counter)--;
	  		me -> stats.s_gvt_func += tw_clock_read() - start; // GVT Mattern
             		
			//if (!interval_counter) tw_gvt_step1(me);
            //else if (!g_tw_tid) interval_counter--;
      	#endif

      	if (me -> GVT >= g_tw_ts_end - 1) break;
		
		#ifdef SGVT
	  		start = tw_clock_read();
			if (!spin_array[g_tw_tid].should_spin) batch_event(me);	
	  		me -> stats.s_batch_event += tw_clock_read() - start; // Batch Event
		#else
	  		start = tw_clock_read();
			batch_event(me);
	  		me -> stats.s_batch_event += tw_clock_read() - start; // Batch Event
		#endif		

		#ifdef switch_sim_mode 
        /*
		if (!g_tw_tid && me -> GVT >= (g_tw_ts_end * switch_rate) && not_switched)
		{
				not_switched = 0;

				*epg = 5000;
				*reg = 0.9;
				*rem = 0.1;	
		}
        */

		if (g_tw_tid == 0)
		{
			// Switch to communication
			if (me -> GVT >= (g_tw_ts_end * comp_interval) && comm_not_switched)
			{	
				if (g_tw_pid == 0) fprintf(stderr, "    Switched to COMM\n");
				
				comm_not_switched = 0;
				comp_not_switched = 1;

				*epg = 5000;
				*reg = 0.9;
				*rem = 0.1;	
	
				comm_interval = comp_interval + orig_comm_interval;
			}
			// Switch to computation
			else if (me -> GVT >= (g_tw_ts_end * comm_interval) && comp_not_switched) 
			{
				if (g_tw_pid == 0) fprintf(stderr, "    Switched to COMP\n");
				
				comm_not_switched = 1;
				comp_not_switched = 0;

				*epg = orig_epg;
				*reg = orig_reg;
				*rem = orig_rem;	
			
				comp_interval = comm_interval + orig_comp_interval;
			}
		}
      	#endif	
	}
	
	if (PAPI_read(events, values) != PAPI_OK) {printf("event start error !!!\n"); exit(1);}

	//if (!g_tw_pid) fprintf(stderr, "%u leaves\n", g_tw_tid);
	//else fprintf(stderr, "	%u leaves\n", g_tw_tid);
    
    #ifdef ROSS_GVT_mattern_orig
    	g_tw_ts_end = 0;
    #endif
  
 	tw_wall_now(&me -> end_time); // Wall clock run time
  	me -> stats.s_total = tw_clock_read() - me -> stats.s_total; // cpu time

	tw_net_barrier(me);
	(*me -> type.final)(me);
  	tw_stats(me, n);
  	
	pthread_mutex_lock(&papi_lock);
	if (values[1] > total_cycles) total_cycles = values[1];
    total_inst += values[0];
	pthread_mutex_unlock(&papi_lock);

	tw_net_barrier(me);
	if (g_tw_tid == 0)
	{
		printf("\nPAPI Statistics:\n"); 
		printf("    Instructions completed in total: %llu\n", total_inst);
    	printf("    CPI: %.5f\n", total_cycles / (double) total_inst);

    	FILE * f = fopen("output/instructions.txt", "a");
    	fprintf(f, "%llu\n", total_inst);

    	f = fopen("output/cpi.txt", "a");
    	fprintf(f, "%f\n", total_cycles / (double) total_inst);
 	}


	#ifdef disparity_check_2
	if (!me -> id)
	{
		double std_dev_lvt[g_tw_gvt_done];
		double avg, total, dist_to_mean, std_dev_total = 0, std_dev_avg = 0;	
		int i, j;
	
		//printf("GVT done: %d\n", g_tw_gvt_done);
		printf("\n");

		for (i = 0; i < g_tw_gvt_done; i++)
		{
			avg = 0;
			total = 0;
			dist_to_mean = 0;
						

			for (j = 0; j < no_threads; j++)
			{
				total += lvt_array[i].lvt[j];
				printf("%.2f ", lvt_array[i].lvt[j]);
			}
			printf("\n");
			
			avg = total / no_threads;		
		
			for (j = 0; j < no_threads; j++) dist_to_mean += (lvt_array[i].lvt[j] - avg) * (lvt_array[i].lvt[j] - avg);

			std_dev_lvt[i] = sqrt(dist_to_mean / no_threads);
			
			//printf("Std dev: %.2f\n", std_dev_lvt[i]);
		} 

		for (i = 0; i < g_tw_gvt_done; i++) std_dev_total += std_dev_lvt[i];
		std_dev_avg = std_dev_total / g_tw_gvt_done;
	
		printf("\nAverage Standard Deviation: %.2f\n", std_dev_avg);
	}
	#endif

	#ifdef SGVT
	if (!me -> id)
	{
		printf("\nThrottle Analysis:\n");
		for (int i = 0; i < no_threads; i++) printf("Thread %d spinned %d times.\n", i, spin_analysis[i].should_spin);
	}	
	#endif
}

void 
sched_event(tw_pe * me)
{
	#ifdef SEPARATE_1
		if (g_tw_tid > 1)
		{
      			tw_sched_event_q(me);
      			tw_sched_cancel_q(me);
		}    
	#elif defined(SEPARATE_0)
		if (g_tw_tid)
		{
      			tw_sched_event_q(me);
      			tw_sched_cancel_q(me);
		}
	#else
      		tw_sched_event_q(me);
      		tw_sched_cancel_q(me);
	#endif
}

void batch_event(tw_pe * me)
{
	#ifdef SEPARATE_1
		if (g_tw_tid > 1) tw_sched_batch(me);
	#elif defined(SEPARATE_0)
		if (g_tw_tid)
		{
			//if (!g_tw_pid) fprintf(stderr, "Event send\n");		
			//else fprintf(stderr, "	Event send\n");	
			tw_sched_batch(me);
		}
	#else
		tw_sched_batch(me);
	#endif
}
