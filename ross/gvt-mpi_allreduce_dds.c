#include <ross.h>

static __thread unsigned gvt_force = 0;

#define WAIT 1000
#define EARLY_FINISH 2

static double GVT = 0;

static unsigned barrier_counter = 0;
volatile static bool barrier_passed = false;

static bool reset = true;
static int id = -1;

static long long total_white = 0;
static unsigned early_finish_counter = 0;

pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;

static unsigned new_active_threads = 0;
static unsigned active_threads = 0;

/*
void 
signal_handler (int signum)
{
    if (signum == SIGUSR1) {}
	else perror("sig handler error\n");
}
*/

void
gvt_init(tw_pe * me)
{
	//signal(SIGUSR1, signal_handler);
	//sigemptyset(&set);                                                             
	//if (sigaddset(&set, SIGUSR1) == -1) perror("Sigaddset error"); 

	if (!g_tw_tid) 
	{
		active_threads = g_tw_n_threads;
		new_active_threads = g_tw_n_threads;
	}

	me -> s_nwhite_sent = 0;
	me -> s_nwhite_recv = 0;

	me -> interval_counter = g_tw_gvt_interval;
	me -> blocked = false;
    me -> zero_counter = 0;
    me -> blocked_counter = 0;	

	//me -> blocked_gvt = -1;
	//me -> awaken_gvt = -1;
}


void
tw_gvt_step1(tw_pe * me)
{	
	tw_clock s, start = tw_clock_read();
	long long local_white = 0;	
	int cc = 0;
	
	if (id == g_tw_tid) GVT = DBL_MAX;

	while (1)
	{
		if (++cc > EARLY_FINISH) 
		{
			if (id == g_tw_tid)
			{
				if (active_threads < g_tw_n_threads)
				{
					pre_reactivate();		
					active_threads = new_active_threads;		
					real_reactivate();		
				}
				early_finish_counter++;
			}

			cc = 1;
		}

		barrier_wait_atomic();
	
		s = tw_clock_read();	
        tw_net_read(me);
        me -> stats.s_net_read += tw_clock_read() - s;	
		local_white = me -> s_nwhite_sent - me -> s_nwhite_recv;

		pthread_mutex_lock(&lock);
		if (reset) 
		{
			id = g_tw_tid; 
			total_white = 0; 
			reset = false;
		}
		total_white += local_white;
		pthread_mutex_unlock(&lock);

		barrier_wait_atomic();

		if (id == g_tw_tid) reset = true;
		if (!total_white) break;
	}	

	bool block = false;
	if (me -> blocked && queues_empty(me)) 
	{
		blocked_threads[g_tw_tid].blocked = true;
		//blocked_threads[g_tw_tid] = true;

		__sync_fetch_and_sub(&new_active_threads, 1);
		block = true;	
	}

	barrier_wait_atomic();
	if (id == g_tw_tid && new_active_threads < g_tw_n_threads) reactivate();		

	tw_stime lvt = min_pq_outq(me);

	pthread_mutex_lock(&lock);
	if (lvt < GVT) GVT = lvt;
	pthread_mutex_unlock(&lock);
	
	barrier_wait_atomic();

	//if (id == g_tw_tid && new_active_threads < g_tw_n_threads) active_threads = new_active_threads;		
	if (id == g_tw_tid) active_threads = new_active_threads;		

	if (GVT < me -> GVT) 
	{
		if (lvt == DBL_MAX) lvt = -1;
		fprintf(stderr, "%d GVT Error: new: %.0f, old: %.0f, LVT: %.0f at %d (%d-%d).%.0f, %.0f, %.0f, %.0f\n", 
			g_tw_tid, GVT, me -> GVT, lvt, gvt_counter, me -> blocked_gvt, me -> awaken_gvt, 
			min_pq(me), min_transit(me), min_eventq(me), min_cancelq(me));
		exit(1);
	}

	me -> interval_counter = g_tw_gvt_interval;
	me -> trans_msg_ts = DBL_MAX;
	me -> s_nwhite_sent = 0;
    me -> s_nwhite_recv = 0;
	me -> stats.s_gvt += tw_clock_read() - start;

	if (GVT != DBL_MAX) 
	{
		me -> GVT = GVT;
		s = tw_clock_read();
		tw_pe_fossil_collect(me);
		me -> stats.s_fossil_collect += tw_clock_read() - s;
	}

	if (id == g_tw_tid)
	{
		gvt_counter++;
		gvt_print(me);
	}

	if (block) self_block(me);
}

inline void
self_block (tw_pe * me)
{
    // Stats
    //me -> gvt_round_when_blocked[me -> blocked_counter] = gvt_counter;
	//me -> blocked_gvt = gvt_counter;

	if (sem_wait(&locks[g_tw_tid].sem)) tw_error(TW_LOC, "Sem Wait Failed\n\n");
	//if (sem_wait(&locks[g_tw_tid])) tw_error(TW_LOC, "Sem Wait Failed\n\n");

	blocked_threads[g_tw_tid].blocked = false;
	//blocked_threads[g_tw_tid] = false;

	me -> interval_counter = 0;
	me -> zero_counter = 0;
    me -> blocked = false;

	// Stats
	//me -> awaken_gvt = gvt_counter;
    me -> blocked_counter++;
}

inline void
//increment_active_thread_count() {__sync_fetch_and_add(&active_threads, 1);}
increment_active_thread_count() {new_active_threads++;}

inline void
//increase_active_thread_count(int n) {__sync_fetch_and_add(&active_threads, n);}
increase_active_thread_count(int n) {new_active_threads += n;}	

inline void 
barrier_wait_atomic ()
{
	bool barrier_passed_old = barrier_passed;

	if (__sync_add_and_fetch(&barrier_counter, 1) == active_threads)
	{
		barrier_counter = 0;
		barrier_passed = !barrier_passed;

		//for (int i = 0; i < g_tw_n_threads; i++) 
		{
			//if (barrier_blocked[i])
			{
				//fprintf(stderr, "	%d wakeing %d\n", g_tw_tid, i);
				//if (pthread_kill(tid_table[i], SIGUSR1)) perror("pthread_kill failled"); 
				//fprintf(stderr, "	%d done\n", g_tw_tid);
			}
		}
	}
	else 
	{
		//int counter = 0;
		//unsigned int u = 256;

		while (barrier_passed_old == barrier_passed) 
		//if (barrier_passed_old == barrier_passed) 
		{
			//int sig;
			//barrier_blocked[g_tw_tid] = true;
			//fprintf(stderr, "%d sleep\n", g_tw_tid);
			//if (sigwait(&set, &sig)) perror("Sigwait error");
			//fprintf(stderr, "%d done\n", g_tw_tid);
			//barrier_blocked[g_tw_tid] = false;

			usleep(WAIT);
			
			//usleep(u);
			//u *= 2;

			/*
			counter++;
			if (counter > 10000)
			{
				if (master_done) break;
				else counter = 0;
			}
			*/
		}
	}
}

inline tw_stime
min_pq (tw_pe * me)
{
	tw_stime ts = tw_pq_minimum(me -> pq);
	if (ts == DBL_MAX) return -1;
	return ts;
}

inline tw_stime
min_transit (tw_pe * me)
{
	tw_stime ts = me -> trans_msg_ts;
	if (ts == DBL_MAX) return -1;
	return ts;
}

inline tw_stime
min_eventq (tw_pe * me)
{
	tw_stime ts = DBL_MAX;
	tw_event * e = me -> event_q.head;
    if (e)
    {   
        for (int i = 0; i < me -> event_q.size; i++)
        {   
            if (ts > e -> recv_ts) ts = e -> recv_ts;
            e = e -> next;
        }
    }

	if (ts == DBL_MAX) return -1;
	return ts;
}

inline tw_stime
min_cancelq (tw_pe * me)
{
	tw_stime ts = DBL_MAX;
	tw_event * e = me -> cancel_q;
    while (e)
    {
        if (ts > e -> recv_ts) ts = e -> recv_ts;
        e = e -> cancel_next;
    }

	if (ts == DBL_MAX) return -1;
	return ts;
}

inline tw_stime
min_pq_outq(tw_pe * me)
{
	tw_stime lvt = tw_pq_minimum(me -> pq);

	if (lvt > me -> trans_msg_ts) lvt = me -> trans_msg_ts;

	return lvt;
	/*
	tw_event * e = me -> event_q.head;  
	if (e)
	{
		for (int i = 0; i < me -> event_q.size; i++)
		{
			if (lvt > e -> recv_ts) lvt = e -> recv_ts;
			e = e -> next;	
		}
	}

	// Fixes cancel q on free events
	e = me -> cancel_q;
	while (e)
	{
		if (lvt > e -> recv_ts) lvt = e -> recv_ts;
		e = e -> cancel_next;
	}

	if (lvt > me -> msg_to_blocked) lvt = me -> msg_to_blocked;
	*/
	
	// Not sure if we need this
	//if (lvt > me -> cur_event -> recv_ts) lvt = me -> cur_event -> recv_ts;
	
	//tw_stime net_min = tw_net_minimum(me);
	//if (lvt > net_min) lvt = net_min;
}

int 
percent_comp_ret () {return percent_complete;}

unsigned int 
get_gvt_count () {return gvt_counter;}

void
tw_gvt_force_update(tw_pe *me)
{
	gvt_force++;
	me -> interval_counter = 0;
}

void
tw_gvt_stats(FILE * f)
{
	fprintf(f, "\nTW GVT Statistics: Barrier DDS\n");
	fprintf(f, "\t%-50s %11d\n", "GVT Interval", g_tw_gvt_interval);
	fprintf(f, "\t%-50s %11d\n", "Batch Size", g_tw_mblock);
	fprintf(f, "\t%-50s %11d\n", "Forced GVT", gvt_force);
	fprintf(f, "\t%-50s %11d\n", "Premature Completed GVT", early_finish_counter);
	fprintf(f, "\t%-50s %11d\n", "Total GVT Computations", gvt_counter);
}

static const tw_optdef gvt_opts [] =
{
	TWOPT_GROUP("ROSS MPI GVT"),
	TWOPT_UINT("gvt-interval", g_tw_gvt_interval, "GVT Interval"),
	TWOPT_END()
};

const tw_optdef *
tw_gvt_setup(void) {return gvt_opts;}

void
tw_gvt_step2(tw_pe *me) {}

void
tw_gvt_start(void) {}
