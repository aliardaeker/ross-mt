#include <ross.h>

// Statistics
static __thread unsigned int gvt_force = 0;
static __thread tw_stime measure_fc = 0;
static __thread tw_stime measure_gvt = 0;

// Phase Counters
static int counter_a = 0;
static int counter_send = 0;
static int counter_b = 0;
static int counter_aware = 0;

struct min_padded 
{
	tw_stime min; // 8 bytes
	bool padding[56];
} __attribute__((aligned(64)));

//static struct min_padded * local_mins; 			// PE minimums
static tw_stime * local_mins; 			// PE minimums

static tw_stime node_gvt = DBL_MAX;     // local gvt
 
static bool GVT_run = true;        		// starts the first phase
static bool compute_GVT = true;         // compute local minima just once (tas)
static bool node_GVT_set = false;       // local gvt is read, local minima is computed

static unsigned new_active_threads = 0;
static unsigned active_threads = 0;

void 
gvt_init(tw_pe * me) // From tw_run() in tw_setup.c
{
	if (!g_tw_tid)
	{
		active_threads = g_tw_n_threads;
		new_active_threads = g_tw_n_threads;

		local_mins = (tw_stime *) calloc(sizeof(tw_stime), active_threads);
		//local_mins = (struct min_padded *) calloc(sizeof(struct min_padded), active_threads);
	}

	me -> phase = phase_a;

	me -> interval_counter = g_tw_gvt_interval;
    me -> blocked = false;
    me -> zero_counter = 0;
    me -> blocked_counter = 0;
}

void
tw_gvt_step1(tw_pe *me)
{
	// Phase A: compute min A 
	if (me -> phase == phase_a && GVT_run)
	{
		//fprintf(stderr, "	%d phase a - %d %d\n", g_tw_tid, active_threads, new_active_threads);
		measure_gvt = tw_clock_read();

		tw_net_read(me); 			 
		tw_stime pq_min = tw_pq_minimum(me -> pq); 
        tw_stime lvt = me -> trans_msg_ts;

        if (lvt > pq_min) lvt = pq_min;
		me -> min_a = lvt;

		me -> phase = send;
		__sync_fetch_and_add(&counter_a, 1);	
	}
	// Phase Send: process and send one more message
	else if (me -> phase == send && counter_a == active_threads)
	{				
		//fprintf(stderr, "	%d phase s\n", g_tw_tid);
		tw_net_read(me); 			   
		tw_sched_batch(me);			  
		
		me -> phase = phase_b;
		if (!__sync_fetch_and_add(&counter_send, 1)) GVT_run = false;	
	}
	// Phase B: compute min B
	else if (me -> phase == phase_b && counter_send == active_threads)
	{
		//fprintf(stderr, "	%d phase b\n", g_tw_tid);
		tw_net_read(me); 		
		tw_stime pq_min = tw_pq_minimum(me -> pq);
        tw_stime lvt = me -> trans_msg_ts;
 
        if (lvt > pq_min) lvt = pq_min;
        me -> min_b = lvt;

		local_mins[g_tw_tid] = min(me -> min_a, me -> min_b);
		//local_mins[g_tw_tid].min = min(me -> min_a, me -> min_b);

		me -> phase = aware;
		__sync_fetch_and_add(&counter_b, 1);	
	}
	// Phase aware: compute GVT
	else if (me -> phase == aware && counter_b == active_threads)
	{
		//fprintf(stderr, "	%d phase aware\n", g_tw_tid);
		if (__sync_bool_compare_and_swap(&compute_GVT, true, false))
		{
			if (active_threads < g_tw_n_threads) reactivate();

			node_gvt = DBL_MAX;
			for (int i = 0; i < g_tw_n_threads; i++) 
			{
				//if (!blocked_threads[i].blocked && local_mins[i].min < node_gvt) node_gvt = local_mins[i].min;
				if (!blocked_threads[i].blocked && local_mins[i] < node_gvt) node_gvt = local_mins[i];
			}

			//if (node_gvt < me -> GVT) 
			{
			//	printf("%d: Error in GVT - old: %.0f new: %.0f\n", g_tw_tid, me -> GVT, node_gvt);
			//	exit(1);
			}

			node_GVT_set = true;	
		}
	
		if (node_GVT_set)
		{
			g_tw_gvt_done++;
			me -> interval_counter = g_tw_gvt_interval;
			me -> trans_msg_ts = DBL_MAX;
			me -> phase = phase_a;
			me -> stats.s_gvt += tw_clock_read() - measure_gvt;

			if (node_gvt != DBL_MAX) 
			{
				me -> GVT = node_gvt;
            	measure_fc = tw_clock_read();
           		tw_pe_fossil_collect(me);
            	me -> stats.s_fossil_collect += tw_clock_read() - measure_fc;
			}

			bool blocked = false;
			if (me -> blocked && queues_empty(me)) 
			{
				int cpu = affinity_table_inv[g_tw_tid];
				if (cpu >= 0) affinity_table[cpu] = -1;

				blocked_threads[g_tw_tid].blocked = true;
				__sync_fetch_and_sub(&new_active_threads, 1);
				blocked = true;
			}
		
			if (__sync_add_and_fetch(&counter_aware, 1) == active_threads)
			{
			 	set_affinity();
				
				gvt_counter++;
				gvt_print(me);

				counter_a = 0;
				counter_send = 0;
				counter_b = 0;	
				counter_aware = 0;
			
				compute_GVT = true;
				node_GVT_set = false;
	
    			active_threads = new_active_threads;
    			GVT_run = true;
			}

			if (blocked) self_block (me);
		}
	}
}

inline void
increment_active_thread_count() {new_active_threads++;}

inline void
increase_active_thread_count(int n) {new_active_threads += n;}	

inline void
self_block (tw_pe * me)
{
    // Stats
    //me -> gvt_round_when_blocked[me -> blocked_counter] = gvt_counter;
	//me -> blocked_gvt = gvt_counter;

	//fprintf(stderr, "%d b on %d\n", g_tw_tid, gvt_counter);
	if (sem_wait(&locks[g_tw_tid].sem)) tw_error(TW_LOC, "Sem Wait Failed\n\n");
	//fprintf(stderr, "%d s on %d\n", g_tw_tid, gvt_counter);

	blocked_threads[g_tw_tid].blocked = false;

	// Stats
	//me -> awaken_gvt = gvt_counter;
    me -> blocked_counter++;

	me -> interval_counter = 0;
	me -> zero_counter = 0;
    me -> blocked = false;
}

int 
percent_comp_ret () {return percent_complete;}

void
tw_gvt_start() {} // From tw_init() in tw_setup.c

void
tw_gvt_force_update(tw_pe *me) 
{
	gvt_force++;
	me -> interval_counter = 0;
    //fprintf(stderr, "GVT forced\n");
}

unsigned int
get_gvt_count() {return gvt_counter;}

static const tw_optdef gvt_opts [] =
{
	TWOPT_GROUP("ROSS MPI GVT"),
	TWOPT_UINT("gvt-interval", g_tw_gvt_interval, "GVT Interval"),
	TWOPT_END()
};

void
tw_gvt_stats(FILE * f)
{
	fprintf(f, "\nTW GVT Statistics: Wait Free DDS\n");
	fprintf(f, "\t%-50s %11d\n", "GVT Interval", g_tw_gvt_interval);
	fprintf(f, "\t%-50s %11d\n", "Batch Size", g_tw_mblock);
	fprintf(f, "\t%-50s %11d\n", "Forced GVT", gvt_force);
	fprintf(f, "\t%-50s %11d\n", "Total GVT Computations", gvt_counter);
}

const tw_optdef *
tw_gvt_setup(void) {return gvt_opts;}
