#ifndef INC_epi_h
#define INC_epi_h

#include <ross.h>

#include "epi-types.h"
#include "epi-extern.h"

typedef struct
{   
    int argc;
    char **argv;
    char **env;
    int thread_id;
} param;

int no_threads;

#endif
