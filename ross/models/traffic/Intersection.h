#ifndef INC_inter_h
#define INC_inter_h

#include <ross.h>
#include <pthread.h>
#include <papi.h>
#include <assert.h>

#define INTERSECTION_LPS 0
#define MAX_CARS_ON_ROAD 5

#define NUM_CELLS_X 256 //128 //64 //32     //1024
#define NUM_CELLS_Y 256 //128 //64 //32     //1024	

#define NUM_VP_X 32 
#define NUM_VP_Y 32 

typedef struct
{
    int argc;
    char **argv;
    char **env;
    int thread_id;
} param;

enum events { ARIVAL, DEPARTURE, DIRECTION_SELECT };

enum abs_directions {WEST_LEFT = 0, WEST_STRAIGHT, WEST_RIGHT, EAST_LEFT, EAST_STRAIGHT, EAST_RIGHT, 
					NORTH_LEFT, NORTH_STRAIGHT, NORTH_RIGHT, SOUTH_LEFT, SOUTH_STRAIGHT, SOUTH_RIGHT};

enum ariv_dept {IN, OUT};

typedef struct 
{
	int x_to_go;
	int y_to_go;
	int sent_back;
	enum abs_directions arrived_from;
	enum abs_directions current_lane;
	enum ariv_dept in_out;
} a_car;

typedef struct 
{
	enum events event_type;
	a_car car;
} Msg_Data;

typedef struct 
{
	int total_cars_arrived;
	int total_cars_finished;
	int num_in_west_left;
	int num_in_west_straight;
	int num_in_west_right;
	int num_in_north_left;
	int num_in_north_straight;
	int num_in_north_right;
	int num_in_south_left;
	int num_in_south_straight;
	int num_in_south_right;
	int num_in_east_left;
	int num_in_east_straight;
	int num_in_east_right;
	int num_out_west_left;
	int num_out_west_straight;
	int num_out_west_right;
	int num_out_north_left;
	int num_out_north_straight;
	int num_out_north_right;
	int num_out_south_left;
	int num_out_south_straight;
	int num_out_south_right;
	int num_out_east_left;
	int num_out_east_straight;
	int num_out_east_right;
} Intersection_State;

void Intersection_StartUp(Intersection_State *, tw_lp *);
void Intersection_EventHandler(Intersection_State *, tw_bf *, Msg_Data *, tw_lp *);
void Intersection_RC_EventHandler(Intersection_State *, tw_bf *, Msg_Data *, tw_lp *);
void Intersection_Statistics_CollectStats(Intersection_State *, tw_lp *);

tw_lpid g_vp_per_proc = 0; // set in main
tw_lpid g_cells_per_vp_x = NUM_CELLS_X / NUM_VP_X;
tw_lpid g_cells_per_vp_y = NUM_CELLS_Y / NUM_VP_Y;
tw_lpid g_cells_per_vp = (NUM_CELLS_X / NUM_VP_X) * (NUM_CELLS_Y / NUM_VP_Y);

tw_stime MEAN_SERVICE = 1.0;
static unsigned int stagger = 0;
static tw_stime percent_remote = 0.25;

static unsigned int nlp_per_pe = 8;
static int g_traffic_start_events = 15;

//static int optimistic_memory = 65536;
//static int optimistic_memory = 200000;
static int optimistic_memory = 1000000;

//static tw_stime ts_end = 300;
static tw_stime ts_end = 30;

// rate for timestamp exponential distribution
static tw_stime mult = 1.6;
static tw_stime mean = 0.0;
tw_stime lookahead = 1.0;

//static char run_id[1024] = "undefined";
static unsigned long long totalCars = 0;
static unsigned long long carsFinished = 0;

tw_lpid         num_cells_per_kp = 0;
tw_lpid         vp_per_proc = 0;

// Added for MT
int no_threads = -1;
pthread_mutex_t print_lock;
pthread_barrier_t print_barrier;

#ifdef SCHED
static unsigned char controller_init_done = 0;
#endif
#endif
