#include "Intersection.h"

tw_lpid 
Cell_ComputeMove( tw_lpid lpid, int direction )
{
	tw_lpid lpid_x, lpid_y;
	tw_lpid n_x, n_y;
	tw_lpid dest_lpid;

	lpid_y = lpid / NUM_CELLS_X;
	lpid_x = lpid - (lpid_y * NUM_CELLS_X);

	switch( direction )
	{
	case 0:
		//West
		n_x = ((lpid_x - 1) + NUM_CELLS_X) % NUM_CELLS_X;
		n_y = lpid_y;
		break;

	case 1:
		//East
		n_x = (lpid_x + 1) % NUM_CELLS_X;
		n_y = lpid_y;
		break;

	case 2:
		//South
		n_x = lpid_x;
		n_y = ((lpid_y - 1) + NUM_CELLS_Y) % NUM_CELLS_Y;
		break;

	case 3:
		//North
		n_x = lpid_x;
		n_y = (lpid_y + 1) % NUM_CELLS_Y;
		break;

	default:
		tw_error( TW_LOC, "Bad direction value \n");
	}

	dest_lpid = (tw_lpid) (n_x + (n_y * NUM_CELLS_X));
	// printf("ComputeMove: Src LP %llu (%d, %d), Dir %u, Dest LP %llu (%d, %d)\n", lpid, lpid_x, lpid_y, direction, dest_lpid, n_x, n_y);
	return( dest_lpid );
}

tw_peid
CellMapping_lp_to_pe(tw_lpid lpid)
{
	long lp_x = lpid % NUM_CELLS_X;
	long lp_y = lpid / NUM_CELLS_X;


	long vp_num_x = lp_x / g_cells_per_vp_x;
	long vp_num_y = lp_y / g_cells_per_vp_y;

	long vp_num = vp_num_x + (vp_num_y * NUM_VP_X);
	tw_peid peid = vp_num / g_vp_per_proc;
	
	return peid;
}

tw_lp *
CellMapping_to_lp(tw_lpid lpid)
{
	tw_lpid lp_x = lpid % NUM_CELLS_X; //lpid -> (lp_x,lp_y)
	tw_lpid lp_y = lpid / NUM_CELLS_X;
	tw_lpid vp_index_x = lp_x % g_cells_per_vp_x;
	tw_lpid vp_index_y = lp_y % g_cells_per_vp_y;
	tw_lpid vp_index = vp_index_x + (vp_index_y * (g_cells_per_vp_x));
	tw_lpid vp_num_x = lp_x/g_cells_per_vp_x;
	tw_lpid vp_num_y = lp_y/g_cells_per_vp_y;
	tw_lpid vp_num = vp_num_x + (vp_num_y*NUM_VP_X);
	vp_num = vp_num % g_vp_per_proc;
	tw_lpid index = vp_index + vp_num*g_cells_per_vp;

#ifdef ROSS_runtime_check
	if( index >= g_tw_nlp ) tw_error(TW_LOC, "index (%llu) beyond g_tw_nlp (%llu) range \n", index, g_tw_nlp);
#endif /* ROSS_runtime_check */

	return g_tw_lp[index];
}

tw_lpid 
CellMapping_to_local_index(tw_lpid lpid)
{
	tw_lpid lp_x = lpid % NUM_CELLS_X; //lpid -> (lp_x,lp_y)
	tw_lpid lp_y = lpid / NUM_CELLS_X;
	tw_lpid vp_index_x = lp_x % g_cells_per_vp_x;
	tw_lpid vp_index_y = lp_y % g_cells_per_vp_y;
	tw_lpid vp_index = vp_index_x + (vp_index_y * (g_cells_per_vp_x));
	tw_lpid vp_num_x = lp_x/g_cells_per_vp_x;
	tw_lpid vp_num_y = lp_y/g_cells_per_vp_y;
	tw_lpid vp_num = vp_num_x + (vp_num_y*NUM_VP_X);
	vp_num = vp_num % g_vp_per_proc;
	tw_lpid index = vp_index + vp_num*g_cells_per_vp;

	if( index >= g_tw_nlp )
		tw_error(TW_LOC, "index (%llu) beyond g_tw_nlp (%llu) range \n", index, g_tw_nlp);

	return( index );
}

tw_lptype mylps[] = 
{
	{
    	(init_f) Intersection_StartUp,
        //(pre_run_f) NULL,
		(event_f) Intersection_EventHandler,
		(revent_f) Intersection_RC_EventHandler,
		//(commit_f) NULL,
		(final_f) Intersection_Statistics_CollectStats,
		(map_f) CellMapping_lp_to_pe,
		sizeof(Intersection_State)
		// (statecp_f) NULL
	},
	{0},
};

void traffic_grid_mapping()
{
	tw_lpid         x, y;
	tw_lpid         lpid, kpid;
	tw_lpid         num_cells_per_kp, vp_per_proc;
	tw_lpid         local_lp_count;

	num_cells_per_kp = (NUM_CELLS_X * NUM_CELLS_Y) / (NUM_VP_X * NUM_VP_Y);
	vp_per_proc = (NUM_VP_X * NUM_VP_Y) / ((tw_nnodes() * g_tw_npe)) ;

	g_tw_nlp = nlp_per_pe;
	g_tw_nkp = vp_per_proc;

	local_lp_count=0;

	for (y = 0; y < NUM_CELLS_Y; y++)
	{
		for (x = 0; x < NUM_CELLS_X; x++)
		{
			lpid = (x + (y * NUM_CELLS_X));
			
			if (g_tw_mynode == CellMapping_lp_to_pe(lpid))
			{
				kpid = local_lp_count / num_cells_per_kp;
				local_lp_count++; // MUST COME AFTER!! DO NOT PRE-INCREMENT ELSE KPID is WRONG!!

				if (kpid >= g_tw_nkp) tw_error(TW_LOC, "Attempting to mapping a KPid (%llu) for Global LPid %llu that is beyond g_tw_nkp (%llu)\n", kpid, lpid, g_tw_nkp );

				tw_lp_onpe(CellMapping_to_local_index(lpid), g_tw_pe[0], lpid); // Calloc LPs here

				if (g_tw_kp[kpid] == NULL) tw_kp_onpe(kpid, g_tw_pe[0]);

				tw_lp_onkp(g_tw_lp[CellMapping_to_local_index(lpid)], g_tw_kp[kpid]);
				tw_lp_settype( CellMapping_to_local_index(lpid), &mylps[0]);
			}
		}
	}
}

void set_cpu_affinity (int rank)
{
        int cpuId = rank;
        cpu_set_t cpuMask; 
        pthread_t thread = pthread_self();

        CPU_ZERO(&cpuMask);
        CPU_SET(cpuId, &cpuMask);

        if (CPU_ISSET(cpuId, &cpuMask) >= 0)
        {
            int status = pthread_setaffinity_np(thread, sizeof(cpu_set_t), &cpuMask);

            if(status !=0)
            {
                printf("Rank %d, cpuID %d\n", rank, cpuId);
                perror("NPH: sched_setaffinity\n");
            }
        }
        else perror("Error in Set Affinity\n");
}

void * 
thread_main (void * arg)
{
	//g_tw_rng_default = TW_FALSE;
	//tw_opt_add(app_opt);
	
    g_tw_n_threads = no_threads; // Worker threads
	g_tw_lookahead = lookahead;
	g_tw_memory_nqueues = 16; // give at least 16 memory queue event

	g_tw_events_per_pe = (mult * nlp_per_pe * g_traffic_start_events) + optimistic_memory;

	g_tw_nlp = nlp_per_pe;
	g_tw_nkp = vp_per_proc;

	g_tw_mapping = CUSTOM;
	//g_tw_mapping = LINEAR;
	g_tw_custom_initial_mapping = &traffic_grid_mapping;
	g_tw_custom_lp_global_to_local_map = &CellMapping_to_lp;

	param * p = (param *) arg;
	tw_init(&p -> argc, &p -> argv, p -> thread_id);
	
	//barrier_sync(&g_barrier, g_tw_tid);
	//fprintf(stderr, "%d running\n", g_tw_tid);

	tw_define_lps(nlp_per_pe, sizeof(Msg_Data), 0); // Seg Fault Here
	
	//fprintf(stderr, "%d cont\n", g_tw_tid);
    //barrier_sync(&g_barrier, g_tw_tid);
    //exit(1);

	for(int i = 0; i < g_tw_nlp; i++) tw_lp_settype(i, &mylps[0]);

	if (g_tw_mynode == 0)
	{
		#ifdef SCHED 
        printf("Spawning %d Threads: (1 Controller - %d Worker)\n", no_threads + 1, no_threads);
		#else
        printf("Spawning %d Threads\n", no_threads);
		#endif
		printf("========================================\n");
		printf("Traffice Model Configuration............\n");
		printf("   Lookahead..............%lf\n", lookahead);
		printf("   Start-events...........%u\n", g_traffic_start_events);
		printf("   stagger................%u\n", stagger);
		printf("   Mean...................%lf\n", mean);
		printf("   Mult...................%lf\n", mult);
		printf("   Memory.................%u\n", optimistic_memory);
		printf("   Remote.................%lf\n", percent_remote);
		printf("========================================\n\n");
	}	

	set_cpu_affinity(g_tw_tid);

	#ifdef SCHED 
		while (1)
		{
			if (controller_init_done)
			{
        		tw_run (0, 0, NULL, NULL, NULL);
				break;
			}
		}
    #else
		tw_run (0, 0, NULL, NULL, NULL);
    #endif

    tw_end();

	/*
    pthread_barrier_wait(&print_barrier);
    pthread_mutex_lock(&print_lock);
	printf("%d -> Num Arivals: %lld, Num Cars reached their dest: %lld\n", g_tw_tid, totalCars, carsFinished);
    pthread_mutex_unlock(&print_lock);
	*/

    pthread_exit(NULL);
    return NULL;
}

#ifdef SCHED
void *  
controller_main (void * i)
{
	int * rank = (int *) i;
	set_cpu_affinity(*rank);

    g_tw_n_threads = no_threads; // Worker threads
	init_master_check ();
	controller_init_done = 1;
	master_check();

    pthread_exit(NULL);
    return NULL;
}
#endif

int 
main (int argc, char ** argv, char ** env)
{
	if (argc != 4)
	{
		printf("Usage: phold --sync=x threads gvt_interval\n");
		tw_error(TW_LOC, "ARG NUM MUST BE 3\n");
	}

	char * num_t = argv[2];
    no_threads = atoi(num_t);
    argc--;

	char * gvt_int = argv[3];
    g_tw_gvt_interval = atoi(gvt_int);
    argc--;

	g_tw_ts_end = ts_end;

	#ifdef SCHED
	no_threads--; // last thread is the controller
    #endif

	#ifdef PAPI
    if (PAPI_library_init(PAPI_VER_CURRENT) != PAPI_VER_CURRENT) {printf("PAPI not initialized\n"); assert(0);}
    if (PAPI_query_event(PAPI_TOT_INS) != PAPI_OK) {printf("Event does not present\n"); assert(0);}
    if (PAPI_query_event(PAPI_TOT_CYC) != PAPI_OK) {printf("Event does not present\n"); assert(0);}
    #endif

    pthread_t * threads;
    param p = {argc,argv,env,0};
	param * prm;

	pthread_mutex_init(&print_lock, NULL);
    pthread_barrier_init(&print_barrier, NULL, no_threads); 

	threads = (pthread_t *) malloc(no_threads * sizeof(pthread_t));
    prm = (param *) malloc(no_threads * sizeof(param));

    for(int i = 0; i < 2; i++)
    {
    	init_barrier(&sum_barrier[i], no_threads);
        init_barrier(&min_barrier[i], no_threads);
    }

    init_barrier(&g_barrier, no_threads);
    init_barrier(&max_barrier,no_threads);
	sum_index = 0;
    min_index = 0;

	nlp_per_pe = (NUM_CELLS_X * NUM_CELLS_Y) / (no_threads * g_tw_npe);
	vp_per_proc = (NUM_VP_X * NUM_VP_Y) / (no_threads * g_tw_npe);
	num_cells_per_kp = (NUM_CELLS_X * NUM_CELLS_Y) / (NUM_VP_X * NUM_VP_Y);
	g_vp_per_proc = vp_per_proc;

	// Spawn the controller seperately
	#ifdef SCHED
	pthread_t controller; 
    pthread_create(&controller, NULL, controller_main, (void *) &no_threads);
	#endif

	for(int i = 0; i < no_threads; i++)
    {
        prm[i] = p;
        prm[i].thread_id = i;
        pthread_create(&threads[i], NULL, thread_main, (void *) &prm[i]);
    }

	for(int i = 0; i < no_threads; i++) pthread_join(threads[i], NULL);

	#ifdef SCHED
	pthread_join(controller, NULL);
	#endif

    for(int i = 0; i < 2; i++)
    {
        destroy_barrier(&sum_barrier[i]);
        destroy_barrier(&min_barrier[i]);
    }

    destroy_barrier(&g_barrier);
    destroy_barrier(&max_barrier);

    free(threads);
    free(prm);
    return 0;
}

void  Intersection_StartUp(Intersection_State *SV, tw_lp * lp) 
{
	//printf("begin init\n");
	int i;
	tw_event *CurEvent;
	tw_stime ts;
	Msg_Data *NewM;

	SV->total_cars_arrived = 0;
	SV->total_cars_finished = 0;
	SV->num_in_west_left = 0;
	SV->num_in_west_straight = 0;
	SV->num_in_west_right = 0;
	SV->num_in_north_left = 0;
	SV->num_in_north_straight = 0;
	SV->num_in_north_right = 0;
	SV->num_in_south_left = 0;
	SV->num_in_south_straight = 0;
	SV->num_in_south_right = 0;
	SV->num_in_east_left = 0;
	SV->num_in_east_straight = 0;
	SV->num_in_east_right = 0;
	SV->num_out_west_left = 0;
	SV->num_out_west_straight = 0;
	SV->num_out_west_right = 0;
	SV->num_out_north_left = 0;
	SV->num_out_north_straight = 0;
	SV->num_out_north_right = 0;
	SV->num_out_south_left = 0;
	SV->num_out_south_straight = 0;
	SV->num_out_south_right = 0;
	SV->num_out_east_left = 0;
	SV->num_out_east_straight = 0;
	SV->num_out_east_right = 0;

	for(i = 0; i < g_traffic_start_events; i++)
	{
		ts = tw_rand_exponential(lp->rng, MEAN_SERVICE );
		CurEvent = tw_event_new(lp->gid, ts, lp);

		NewM = (Msg_Data *)tw_event_data(CurEvent);
		NewM->event_type = ARIVAL;
		NewM->car.x_to_go = rand() % 200 - 99;
		NewM->car.y_to_go = rand() % 200 - 99;
		NewM->car.current_lane = rand() % 12;
		NewM->car.sent_back = 0;
		NewM->car.in_out = IN;

		tw_event_send(CurEvent);
	}
}

void Intersection_EventHandler(Intersection_State *SV, tw_bf *CV, Msg_Data *M, tw_lp *lp)
{
	//fprintf(stderr, "%d sending to %ld\n", g_tw_tid, lp -> gid / nlp_per_pe);

	tw_stime ts=0.0;
	int new_event_direction=0;
	tw_event *CurEvent=NULL;
	Msg_Data *NewM=NULL;
	enum abs_directions temp_direction=0;
	*(int *)CV = (int)0;

	switch(M->event_type) {

	case ARIVAL:

		if(M->car.x_to_go == 0 && M->car.y_to_go == 0){
			SV->total_cars_finished++;
			break;
		}

		// Schedule a departure in the future
		SV->total_cars_arrived++;

		switch(M->car.current_lane){

		case WEST_LEFT:
			SV->num_in_east_left++;
			M->car.current_lane = EAST_LEFT;
			break;
		case WEST_STRAIGHT:
			SV->num_in_east_straight++;
			M->car.current_lane = EAST_STRAIGHT;
			break;
		case WEST_RIGHT:
			SV->num_in_east_right++;
			M->car.current_lane = EAST_RIGHT;
			break;
		case EAST_LEFT:
			SV->num_in_west_left++;
			M->car.current_lane = WEST_LEFT;
			break;
		case EAST_STRAIGHT:
			SV->num_in_west_straight++;
			M->car.current_lane = WEST_STRAIGHT;
			break;
		case EAST_RIGHT:
			SV->num_in_west_right++;
			M->car.current_lane = WEST_RIGHT;
			break;
		case NORTH_LEFT:
			SV->num_in_south_left++;
			M->car.current_lane = SOUTH_LEFT;
			break;
		case NORTH_STRAIGHT:
			SV->num_in_south_straight++;
			M->car.current_lane = SOUTH_STRAIGHT;
			break;
		case NORTH_RIGHT:
			SV->num_in_south_right++;
			M->car.current_lane = SOUTH_RIGHT;
			break;
		case SOUTH_LEFT:
			SV->num_in_north_left++;
			M->car.current_lane = NORTH_LEFT;
			break;
		case SOUTH_STRAIGHT:
			SV->num_in_north_straight++;
			M->car.current_lane = NORTH_STRAIGHT;
			break;
		case SOUTH_RIGHT:
			SV->num_in_north_right++;
			M->car.current_lane = NORTH_RIGHT;
			break;
		}

		M->car.in_out = IN;

		ts = tw_rand_exponential(lp->rng, MEAN_SERVICE);
		CurEvent = tw_event_new(lp->gid, ts, lp);

		NewM = (Msg_Data *)tw_event_data(CurEvent);
		NewM->car.x_to_go = M->car.x_to_go;
		NewM->car.y_to_go = M->car.y_to_go;
		NewM->car.current_lane = M->car.current_lane;
		NewM->car.sent_back = M->car.sent_back;
		NewM->car.arrived_from = M->car.arrived_from;
		NewM->car.in_out = M->car.in_out;
		NewM->event_type = DIRECTION_SELECT;
		//printf("send ari ");
		//
		tw_event_send(CurEvent);


		break;

	case DEPARTURE:

		switch(M->car.current_lane){
		case WEST_LEFT:
			SV->num_out_west_left--;
			new_event_direction = 0;
			break;
		case WEST_STRAIGHT:
			SV->num_out_west_straight--;
			new_event_direction = 0;
			break;
		case WEST_RIGHT:
			SV->num_out_west_right--;
			new_event_direction = 0;
			break;
		case EAST_LEFT:
			SV->num_out_east_left--;
			new_event_direction = 1;
			break;
		case EAST_STRAIGHT:
			SV->num_out_east_straight--;
			new_event_direction = 1;
			break;
		case EAST_RIGHT:
			SV->num_out_east_right--;
			new_event_direction = 1;
			break;
		case NORTH_LEFT:
			SV->num_out_north_left--;
			new_event_direction = 3;
			break;
		case NORTH_STRAIGHT:
			SV->num_out_north_straight--;
			new_event_direction = 3;
			break;
		case NORTH_RIGHT:
			SV->num_out_north_right--;
			new_event_direction = 3;
			break;
		case SOUTH_LEFT:
			SV->num_out_south_left--;
			new_event_direction = 2;
			break;
		case SOUTH_STRAIGHT:
			SV->num_out_south_straight--;
			new_event_direction = 2;
			break;
		case SOUTH_RIGHT:
			SV->num_out_south_right--;
			new_event_direction = 2;
			break;
		}

		lp->gid = Cell_ComputeMove(lp->gid, new_event_direction);
		ts = tw_rand_exponential(lp->rng, MEAN_SERVICE);
		CurEvent = tw_event_new(lp->gid, ts, lp);

		NewM = (Msg_Data *) tw_event_data(CurEvent);
		NewM->car.x_to_go = M->car.x_to_go;
		NewM->car.y_to_go = M->car.y_to_go;
		NewM->car.current_lane = M->car.current_lane;
		NewM->car.sent_back = M->car.sent_back;
		NewM->car.arrived_from = M->car.arrived_from;
		NewM->car.in_out = M->car.in_out;
		NewM->event_type = ARIVAL;

		//printf("send dep ");
		tw_event_send(CurEvent);
		break;

	case DIRECTION_SELECT:


		temp_direction = M->car.current_lane;

		switch(M->car.current_lane){
		case EAST_LEFT:
			SV->num_in_east_left--;
			if(M->car.y_to_go < 0 && SV->num_out_south_straight < MAX_CARS_ON_ROAD){
				M->car.current_lane = SOUTH_STRAIGHT;
				SV->num_out_south_straight ++;
				M->car.sent_back = 0;
				M->car.y_to_go++;
			}
			else if(M->car.x_to_go < 0 && SV->num_out_south_right < MAX_CARS_ON_ROAD){
				M->car.current_lane = SOUTH_RIGHT;
				SV->num_out_south_right ++;
				M->car.sent_back = 0;
				M->car.x_to_go++;
			}
			else if(M->car.x_to_go > 0 && SV->num_out_south_left < MAX_CARS_ON_ROAD){
				M->car.current_lane = SOUTH_LEFT;
				SV->num_out_south_left ++;
				M->car.sent_back = 0;
				M->car.x_to_go--;
			}
			else{
				if(M->car.arrived_from == SOUTH_LEFT){
					M->car.current_lane = EAST_RIGHT;
					SV->num_out_east_right++;
					M->car.sent_back++;
				}
				else if(M->car.arrived_from == EAST_STRAIGHT){
					M->car.current_lane = EAST_STRAIGHT;
					SV->num_out_east_straight++;
					M->car.sent_back++;
				}
				else if(M->car.arrived_from == NORTH_RIGHT){
					M->car.current_lane = EAST_LEFT;
					SV->num_out_east_left++;
					M->car.sent_back++;
				}
			}
			break;
		case EAST_STRAIGHT:
			SV->num_in_east_straight--;
			if(M->car.x_to_go < 0 && SV->num_out_west_straight < MAX_CARS_ON_ROAD){
				M->car.current_lane = WEST_STRAIGHT;
				SV->num_out_west_straight ++;
				M->car.sent_back = 0;
				M->car.x_to_go++;
			}
			else if(M->car.y_to_go < 0 && SV->num_out_west_left < MAX_CARS_ON_ROAD){
				M->car.current_lane = WEST_LEFT;
				SV->num_out_west_left ++;
				M->car.sent_back = 0;
				M->car.y_to_go++;
			}
			else if(M->car.y_to_go > 0 && SV->num_out_west_right < MAX_CARS_ON_ROAD){
				M->car.current_lane = WEST_RIGHT;
				SV->num_out_west_right ++;
				M->car.sent_back = 0;
				M->car.y_to_go--;
			}
			else{
				if(M->car.arrived_from == NORTH_RIGHT){
					M->car.current_lane = EAST_LEFT;
					SV->num_out_east_left++;
					M->car.sent_back++;
				}
				else if(M->car.arrived_from == EAST_STRAIGHT){
					M->car.current_lane = EAST_STRAIGHT;
					SV->num_out_east_straight++;
					M->car.sent_back++;
				}
				else if(M->car.arrived_from == SOUTH_LEFT){
					M->car.current_lane = EAST_RIGHT;
					SV->num_out_east_right++;
					M->car.sent_back++;
				}
			}

			break;
		case EAST_RIGHT:
			SV->num_in_east_right--;
			if(M->car.y_to_go > 0 && SV->num_out_north_straight < MAX_CARS_ON_ROAD){
				M->car.current_lane = NORTH_STRAIGHT;
				SV->num_out_north_straight ++;
				M->car.sent_back = 0;
				M->car.y_to_go--;
			}
			else if(M->car.x_to_go > 0 && SV->num_out_north_right < MAX_CARS_ON_ROAD){
				M->car.current_lane = NORTH_RIGHT;
				SV->num_out_north_right ++;
				M->car.sent_back = 0;
				M->car.x_to_go --;
			}
			else if(M->car.x_to_go < 0 && SV->num_out_north_left < MAX_CARS_ON_ROAD){
				M->car.current_lane = NORTH_LEFT;
				SV->num_out_north_left ++;
				M->car.sent_back = 0;
				M->car.x_to_go++;
			}
			else{
				if(M->car.arrived_from == SOUTH_LEFT){
					M->car.current_lane = EAST_RIGHT;
					SV->num_out_east_right++;
					M->car.sent_back++;
				}
				else if(M->car.arrived_from == EAST_STRAIGHT){
					M->car.current_lane = EAST_STRAIGHT;
					SV->num_out_east_straight++;
					M->car.sent_back++;
				}
				else if(M->car.arrived_from == NORTH_RIGHT){
					M->car.current_lane = EAST_LEFT;
					SV->num_out_east_left++;
					M->car.sent_back++;
				}
			}
			break;
		case WEST_LEFT:
			SV->num_in_west_left--;
			if(M->car.y_to_go > 0 && SV->num_out_north_straight < MAX_CARS_ON_ROAD){
				M->car.current_lane = NORTH_STRAIGHT;
				SV->num_out_north_straight ++;
				M->car.sent_back = 0;
				M->car.y_to_go--;
			}
			else if(M->car.x_to_go > 0 && SV->num_out_north_right < MAX_CARS_ON_ROAD){
				M->car.current_lane = NORTH_RIGHT;
				SV->num_out_north_right ++;
				M->car.sent_back = 0;
				M->car.x_to_go--;
			}
			else if(M->car.x_to_go < 0 && SV->num_out_north_left < MAX_CARS_ON_ROAD){
				M->car.current_lane = NORTH_LEFT;
				SV->num_out_north_left ++;
				M->car.sent_back = 0;
				M->car.x_to_go++;
			}
			else{
				if(M->car.arrived_from == SOUTH_RIGHT){
					M->car.current_lane = WEST_LEFT;
					SV->num_out_west_left++;
					M->car.sent_back++;
				}
				else if(M->car.arrived_from == WEST_STRAIGHT){
					M->car.current_lane = WEST_STRAIGHT;
					SV->num_out_west_straight++;
					M->car.sent_back++;
				}
				else if(M->car.arrived_from == NORTH_LEFT){
					M->car.current_lane = WEST_RIGHT;
					SV->num_out_west_right++;
					M->car.sent_back++;
				}
			}
			break;
		case WEST_STRAIGHT:
			SV->num_in_west_straight--;
			if(M->car.x_to_go > 0 && SV->num_out_east_straight < MAX_CARS_ON_ROAD){
				M->car.current_lane = EAST_STRAIGHT;
				SV->num_out_east_straight ++;
				M->car.sent_back = 0;
				M->car.x_to_go--;
			}
			else if(M->car.y_to_go > 0 && SV->num_out_east_left < MAX_CARS_ON_ROAD){
				M->car.current_lane = EAST_LEFT;
				SV->num_out_east_left ++;
				M->car.sent_back = 0;
				M->car.y_to_go --;
			}
			else if(M->car.y_to_go < 0 && SV->num_out_east_right < MAX_CARS_ON_ROAD){
				M->car.current_lane = EAST_RIGHT;
				SV->num_out_east_right ++;
				M->car.sent_back = 0;
				M->car.y_to_go++;
			}
			else{
				if(M->car.arrived_from == SOUTH_RIGHT){
					M->car.current_lane = WEST_LEFT;
					SV->num_out_west_left++;
					M->car.sent_back++;
				}
				else if(M->car.arrived_from == WEST_STRAIGHT){
					M->car.current_lane = WEST_STRAIGHT;
					SV->num_out_west_straight++;
					M->car.sent_back++;
				}
				else if(M->car.arrived_from == NORTH_LEFT){
					M->car.current_lane = WEST_RIGHT;
					SV->num_out_west_right++;
					M->car.sent_back++;
				}
			}
			break;
		case WEST_RIGHT:
			SV->num_in_west_right--;
			if(M->car.y_to_go < 0 && SV->num_out_south_straight < MAX_CARS_ON_ROAD){
				M->car.current_lane = SOUTH_STRAIGHT;
				SV->num_out_south_straight ++;
				M->car.sent_back = 0;
				M->car.y_to_go++;
			}
			else if(M->car.x_to_go > 0 && SV->num_out_south_left < MAX_CARS_ON_ROAD){
				M->car.current_lane = SOUTH_LEFT;
				SV->num_out_south_left ++;
				M->car.sent_back = 0;
				M->car.x_to_go--;
			}
			else if(M->car.x_to_go < 0 && SV->num_out_south_right < MAX_CARS_ON_ROAD){
				M->car.current_lane = SOUTH_RIGHT;
				SV->num_out_south_right ++;
				M->car.sent_back = 0;
				M->car.x_to_go++;
			}
			else{
				if(M->car.arrived_from == SOUTH_RIGHT){
					M->car.current_lane = WEST_LEFT;
					SV->num_out_west_left++;
					M->car.sent_back++;
				}
				else if(M->car.arrived_from == WEST_STRAIGHT){
					M->car.current_lane = WEST_STRAIGHT;
					SV->num_out_west_straight++;
					M->car.sent_back++;
				}
				else if(M->car.arrived_from == NORTH_LEFT){
					M->car.current_lane = WEST_RIGHT;
					SV->num_out_west_right++;
					M->car.sent_back++;
				}
			}
			break;
		case NORTH_LEFT:
			SV->num_in_north_left--;
			if(M->car.x_to_go > 0 && SV->num_out_east_straight < MAX_CARS_ON_ROAD){
				M->car.current_lane = EAST_STRAIGHT;
				SV->num_out_east_straight ++;
				M->car.sent_back = 0;
				M->car.x_to_go --;
			}
			else if(M->car.y_to_go > 0 && SV->num_out_east_left < MAX_CARS_ON_ROAD){
				M->car.current_lane = EAST_LEFT;
				SV->num_out_east_left ++;
				M->car.sent_back = 0;
				M->car.y_to_go--;
			}
			else if(M->car.y_to_go < 0 && SV->num_out_east_right < MAX_CARS_ON_ROAD){
				M->car.current_lane = EAST_RIGHT;
				SV->num_out_east_right ++;
				M->car.sent_back = 0;
				M->car.y_to_go++;
			}
			else{
				if(M->car.arrived_from == WEST_RIGHT){
					M->car.current_lane = NORTH_LEFT;
					SV->num_out_north_left++;
					M->car.sent_back++;
				}
				else if(M->car.arrived_from == NORTH_STRAIGHT){
					M->car.current_lane = NORTH_STRAIGHT;
					SV->num_out_north_straight++;
					M->car.sent_back++;
				}
				else if(M->car.arrived_from == EAST_LEFT){
					M->car.current_lane = NORTH_RIGHT;
					SV->num_out_north_right++;
					M->car.sent_back++;
				}
			}

			break;
		case NORTH_STRAIGHT:
			SV->num_in_north_straight--;
			if(M->car.y_to_go < 0 && SV->num_out_south_straight < MAX_CARS_ON_ROAD){
				M->car.current_lane = SOUTH_STRAIGHT;
				SV->num_out_south_straight ++;
				M->car.sent_back = 0;
				M->car.y_to_go++;
			}
			else if(M->car.x_to_go > 0 && SV->num_out_south_left < MAX_CARS_ON_ROAD){
				M->car.current_lane = SOUTH_LEFT;
				SV->num_out_south_left ++;
				M->car.sent_back = 0;
				M->car.x_to_go--;
			}
			else if(M->car.x_to_go < 0 && SV->num_out_south_right < MAX_CARS_ON_ROAD){
				M->car.current_lane = SOUTH_RIGHT;
				SV->num_out_south_right ++;
				M->car.sent_back = 0;
				M->car.x_to_go++;
			}
			else{
				if(M->car.arrived_from == WEST_RIGHT){
					M->car.current_lane = NORTH_LEFT;
					SV->num_out_north_left++;
					M->car.sent_back++;
				}
				else if(M->car.arrived_from == NORTH_STRAIGHT){
					M->car.current_lane = NORTH_STRAIGHT;
					SV->num_out_north_straight++;
					M->car.sent_back++;
				}
				else if(M->car.arrived_from == EAST_LEFT){
					M->car.current_lane = NORTH_RIGHT;
					SV->num_out_north_right++;
					M->car.sent_back++;
				}
			}
			break;
		case NORTH_RIGHT:
			SV->num_in_north_right--;
			if(M->car.x_to_go < 0 && SV->num_out_west_straight < MAX_CARS_ON_ROAD){
				M->car.current_lane = WEST_STRAIGHT;
				SV->num_out_west_straight ++;
				M->car.sent_back = 0;
				M->car.x_to_go++;
			}
			else if(M->car.y_to_go < 0 && SV->num_out_west_left < MAX_CARS_ON_ROAD){
				M->car.current_lane = WEST_LEFT;
				SV->num_out_west_left ++;
				M->car.sent_back = 0;
				M->car.y_to_go++;
			}
			else if(M->car.y_to_go > 0 && SV->num_out_west_right < MAX_CARS_ON_ROAD){
				M->car.current_lane = WEST_RIGHT;
				SV->num_out_west_right ++;
				M->car.sent_back = 0;
				M->car.y_to_go--;
			}
			else{
				if(M->car.arrived_from == WEST_RIGHT){
					M->car.current_lane = NORTH_LEFT;
					SV->num_out_north_left++;
					M->car.sent_back++;
				}
				else if(M->car.arrived_from == NORTH_STRAIGHT){
					M->car.current_lane = NORTH_STRAIGHT;
					SV->num_out_north_straight++;
					M->car.sent_back++;
				}
				else if(M->car.arrived_from == EAST_LEFT){
					M->car.current_lane = NORTH_RIGHT;
					SV->num_out_north_right++;
					M->car.sent_back++;
				}
			}
			break;
		case SOUTH_LEFT:
			SV->num_in_south_left--;
			if(M->car.x_to_go < 0 && SV->num_out_west_straight < MAX_CARS_ON_ROAD){
				M->car.current_lane = WEST_STRAIGHT;
				SV->num_out_west_straight ++;
				M->car.sent_back = 0;
				M->car.x_to_go++;
			}
			else if(M->car.y_to_go < 0 && SV->num_out_west_left < MAX_CARS_ON_ROAD){
				M->car.current_lane = WEST_LEFT;
				SV->num_out_west_left ++;
				M->car.sent_back = 0;
				M->car.y_to_go++;
			}
			else if(M->car.y_to_go > 0 && SV->num_out_west_right < MAX_CARS_ON_ROAD){
				M->car.current_lane = WEST_RIGHT;
				SV->num_out_west_right ++;
				M->car.sent_back = 0;
				M->car.y_to_go--;
			}
			else{
				if(M->car.arrived_from == WEST_LEFT){
					M->car.current_lane = SOUTH_RIGHT;
					SV->num_out_south_right++;
					M->car.sent_back++;
				}
				else if(M->car.arrived_from == SOUTH_STRAIGHT){
					M->car.current_lane = SOUTH_STRAIGHT;
					SV->num_out_south_straight++;
					M->car.sent_back++;
				}
				else if(M->car.arrived_from == EAST_RIGHT){
					M->car.current_lane = SOUTH_LEFT;
					SV->num_out_south_left++;
					M->car.sent_back++;
				}
			}

			break;
		case SOUTH_STRAIGHT:
			SV->num_in_south_straight--;
			if(M->car.y_to_go > 0 && SV->num_out_north_straight < MAX_CARS_ON_ROAD){
				M->car.current_lane = NORTH_STRAIGHT;
				SV->num_out_north_straight ++;
				M->car.sent_back = 0;
				M->car.y_to_go--;
			}
			else if(M->car.x_to_go < 0 && SV->num_out_north_left < MAX_CARS_ON_ROAD){
				M->car.current_lane = NORTH_LEFT;
				SV->num_out_north_left ++;
				M->car.sent_back = 0;
				M->car.x_to_go++;
			}
			else if(M->car.x_to_go > 0 && SV->num_out_north_right < MAX_CARS_ON_ROAD){
				M->car.current_lane = NORTH_RIGHT;
				SV->num_out_north_right ++;
				M->car.sent_back = 0;
				M->car.x_to_go --;
			}
			else{
				if(M->car.arrived_from == EAST_RIGHT){
					M->car.current_lane = SOUTH_LEFT;
					SV->num_out_south_left++;
					M->car.sent_back++;
				}
				else if(M->car.arrived_from == SOUTH_STRAIGHT){
					M->car.current_lane = SOUTH_STRAIGHT;
					SV->num_out_south_straight++;
					M->car.sent_back++;
				}
				else if(M->car.arrived_from == WEST_LEFT){
					M->car.current_lane = SOUTH_RIGHT;
					SV->num_out_south_right++;
					M->car.sent_back++;
				}
			}
			break;
		case SOUTH_RIGHT:
			SV->num_in_south_right--;
			if(M->car.x_to_go > 0 && SV->num_out_east_straight < MAX_CARS_ON_ROAD){
				M->car.current_lane = EAST_STRAIGHT;
				SV->num_out_east_straight ++;
				M->car.sent_back = 0;
				M->car.x_to_go--;
			}
			else if(M->car.y_to_go > 0 && SV->num_out_east_left < MAX_CARS_ON_ROAD){
				M->car.current_lane = EAST_LEFT;
				SV->num_out_east_left ++;
				M->car.sent_back = 0;
				M->car.y_to_go--;
			}
			else if(M->car.y_to_go < 0 && SV->num_out_east_right < MAX_CARS_ON_ROAD){
				M->car.current_lane = EAST_RIGHT;
				SV->num_out_east_right ++;
				M->car.sent_back = 0;
				M->car.y_to_go++;
			}
			else{
				if(M->car.arrived_from == EAST_RIGHT){
					M->car.current_lane = SOUTH_LEFT;
					SV->num_out_south_left++;
					M->car.sent_back++;
				}
				else if(M->car.arrived_from == SOUTH_STRAIGHT){
					M->car.current_lane = SOUTH_STRAIGHT;
					SV->num_out_south_straight++;
					M->car.sent_back++;
				}
				else if(M->car.arrived_from == WEST_LEFT){
					M->car.current_lane = SOUTH_RIGHT;
					SV->num_out_south_right++;
					M->car.sent_back++;
				}
			}
			break;
		}

		M->car.arrived_from = temp_direction;
		M->car.in_out = OUT;
		ts = tw_rand_exponential(lp->rng, MEAN_SERVICE);
		CurEvent = tw_event_new(lp->gid, ts, lp);

		NewM = (Msg_Data *)tw_event_data(CurEvent);
		NewM->car.x_to_go = M->car.x_to_go;
		NewM->car.y_to_go = M->car.y_to_go;
		NewM->car.current_lane = M->car.current_lane;
		NewM->car.sent_back = M->car.sent_back;
		NewM->car.arrived_from = M->car.arrived_from;
		NewM->car.in_out = M->car.in_out;
		NewM->event_type = DEPARTURE;

		//printf("send dir ");
		tw_event_send(CurEvent);
		break;
	}
}


void Intersection_RC_EventHandler(Intersection_State *SV, tw_bf *CV, Msg_Data *M, tw_lp *lp)
{
	//enum abs_directions temp_direction;
	*(int *)CV = (int)0;

	switch(M->event_type)
	{

	case ARIVAL:

		if(M->car.x_to_go == 0 && M->car.y_to_go == 0)
		{
			SV->total_cars_finished--;
			break;
		}

		// Schedule a departure in the future
		SV->total_cars_arrived--;

		switch(M->car.current_lane)
		{
		case WEST_LEFT:
			SV->num_in_east_left--;
			break;
		case WEST_STRAIGHT:
			SV->num_in_east_straight--;
			break;
		case WEST_RIGHT:
			SV->num_in_east_right++;
			break;
		case EAST_LEFT:
			SV->num_in_west_left++;
			break;
		case EAST_STRAIGHT:
			SV->num_in_west_straight++;
			break;
		case EAST_RIGHT:
			SV->num_in_west_right++;
			break;
		case NORTH_LEFT:
			SV->num_in_south_left++;
			break;
		case NORTH_STRAIGHT:
			SV->num_in_south_straight++;
			break;
		case NORTH_RIGHT:
			SV->num_in_south_right++;
			break;
		case SOUTH_LEFT:
			SV->num_in_north_left++;
			break;
		case SOUTH_STRAIGHT:
			SV->num_in_north_straight++;
			break;
		case SOUTH_RIGHT:
			SV->num_in_north_right++;
			break;
		}

		// ts = tw_rand_exponential(lp->rng, MEAN_SERVICE);
		tw_rand_reverse_unif( lp->rng );
		break;

	case DEPARTURE:

		switch(M->car.current_lane){
		case WEST_LEFT:
			SV->num_out_west_left++;
			break;
		case WEST_STRAIGHT:
			SV->num_out_west_straight++;
			break;
		case WEST_RIGHT:
			SV->num_out_west_right++;
			break;
		case EAST_LEFT:
			SV->num_out_east_left++;
			break;
		case EAST_STRAIGHT:
			SV->num_out_east_straight++;
			break;
		case EAST_RIGHT:
			SV->num_out_east_right++;
			break;
		case NORTH_LEFT:
			SV->num_out_north_left++;
			break;
		case NORTH_STRAIGHT:
			SV->num_out_north_straight++;
			break;
		case NORTH_RIGHT:
			SV->num_out_north_right++;
			break;
		case SOUTH_LEFT:
			SV->num_out_south_left++;
			break;
		case SOUTH_STRAIGHT:
			SV->num_out_south_straight++;
			break;
		case SOUTH_RIGHT:
			SV->num_out_south_right++;
			break;
		}

		// ts = tw_rand_exponential(lp->rng, MEAN_SERVICE);
		tw_rand_reverse_unif( lp->rng );
		break;

	case DIRECTION_SELECT:

		//temp_direction = M->car.current_lane;

		switch(M->car.current_lane){
		case EAST_LEFT:
			SV->num_in_east_left++;
			if(M->car.y_to_go < 0 && SV->num_out_south_straight < MAX_CARS_ON_ROAD){
				SV->num_out_south_straight --;
			}
			else if(M->car.x_to_go < 0 && SV->num_out_south_right < MAX_CARS_ON_ROAD){
				SV->num_out_south_right --;
			}
			else if(M->car.x_to_go > 0 && SV->num_out_south_left < MAX_CARS_ON_ROAD){
				SV->num_out_south_left --;
			}
			else{
				if(M->car.arrived_from == SOUTH_LEFT){
					SV->num_out_east_right--;
				}
				else if(M->car.arrived_from == EAST_STRAIGHT){
					SV->num_out_east_straight--;
				}
				else if(M->car.arrived_from == NORTH_RIGHT){
					SV->num_out_east_left--;
				}
			}
			break;
		case EAST_STRAIGHT:
			SV->num_in_east_straight++;
			if(M->car.x_to_go < 0 && SV->num_out_west_straight < MAX_CARS_ON_ROAD){
				SV->num_out_west_straight --;
			}
			else if(M->car.y_to_go < 0 && SV->num_out_west_left < MAX_CARS_ON_ROAD){
				SV->num_out_west_left --;
			}
			else if(M->car.y_to_go > 0 && SV->num_out_west_right < MAX_CARS_ON_ROAD){
				SV->num_out_west_right --;
			}
			else{
				if(M->car.arrived_from == NORTH_RIGHT){
					SV->num_out_east_left--;
				}
				else if(M->car.arrived_from == EAST_STRAIGHT){
					SV->num_out_east_straight--;
				}
				else if(M->car.arrived_from == SOUTH_LEFT){
					SV->num_out_east_right++;
				}
			}

			break;
		case EAST_RIGHT:
			SV->num_in_east_right++;
			if(M->car.y_to_go > 0 && SV->num_out_north_straight < MAX_CARS_ON_ROAD){
				SV->num_out_north_straight --;
			}
			else if(M->car.x_to_go > 0 && SV->num_out_north_right < MAX_CARS_ON_ROAD){
				SV->num_out_north_right --;
			}
			else if(M->car.x_to_go < 0 && SV->num_out_north_left < MAX_CARS_ON_ROAD){
				SV->num_out_north_left --;
			}
			else{
				if(M->car.arrived_from == SOUTH_LEFT){
					SV->num_out_east_right--;
				}
				else if(M->car.arrived_from == EAST_STRAIGHT){
					SV->num_out_east_straight--;
				}
				else if(M->car.arrived_from == NORTH_RIGHT){
					SV->num_out_east_left--;
				}
			}
			break;
		case WEST_LEFT:
			SV->num_in_west_left++;
			if(M->car.y_to_go > 0 && SV->num_out_north_straight < MAX_CARS_ON_ROAD){
				SV->num_out_north_straight --;
			}
			else if(M->car.x_to_go > 0 && SV->num_out_north_right < MAX_CARS_ON_ROAD){
				SV->num_out_north_right --;
			}
			else if(M->car.x_to_go < 0 && SV->num_out_north_left < MAX_CARS_ON_ROAD){
				SV->num_out_north_left --;
			}
			else{
				if(M->car.arrived_from == SOUTH_RIGHT){
					SV->num_out_west_left--;
				}
				else if(M->car.arrived_from == WEST_STRAIGHT){
					SV->num_out_west_straight--;
				}
				else if(M->car.arrived_from == NORTH_LEFT){
					SV->num_out_west_right--;
				}
			}
			break;
		case WEST_STRAIGHT:
			SV->num_in_west_straight++;
			if(M->car.x_to_go > 0 && SV->num_out_east_straight < MAX_CARS_ON_ROAD){
				SV->num_out_east_straight --;
			}
			else if(M->car.y_to_go > 0 && SV->num_out_east_left < MAX_CARS_ON_ROAD){
				SV->num_out_east_left --;
			}
			else if(M->car.y_to_go < 0 && SV->num_out_east_right < MAX_CARS_ON_ROAD){
				SV->num_out_east_right --;
			}
			else{
				if(M->car.arrived_from == SOUTH_RIGHT){
					SV->num_out_west_left--;
				}
				else if(M->car.arrived_from == WEST_STRAIGHT){
					SV->num_out_west_straight--;
				}
				else if(M->car.arrived_from == NORTH_LEFT){
					SV->num_out_west_right--;
				}
			}
			break;
		case WEST_RIGHT:
			SV->num_in_west_right++;
			if(M->car.y_to_go < 0 && SV->num_out_south_straight < MAX_CARS_ON_ROAD){
				SV->num_out_south_straight --;
			}
			else if(M->car.x_to_go > 0 && SV->num_out_south_left < MAX_CARS_ON_ROAD){
				SV->num_out_south_left --;
			}
			else if(M->car.x_to_go < 0 && SV->num_out_south_right < MAX_CARS_ON_ROAD){
				SV->num_out_south_right --;
			}
			else{
				if(M->car.arrived_from == SOUTH_RIGHT){
					SV->num_out_west_left--;
				}
				else if(M->car.arrived_from == WEST_STRAIGHT){
					SV->num_out_west_straight--;
				}
				else if(M->car.arrived_from == NORTH_LEFT){
					SV->num_out_west_right--;
				}
			}
			break;
		case NORTH_LEFT:
			SV->num_in_north_left++;
			if(M->car.x_to_go > 0 && SV->num_out_east_straight < MAX_CARS_ON_ROAD){
				SV->num_out_east_straight --;
			}
			else if(M->car.y_to_go > 0 && SV->num_out_east_left < MAX_CARS_ON_ROAD){
				SV->num_out_east_left --;
			}
			else if(M->car.y_to_go < 0 && SV->num_out_east_right < MAX_CARS_ON_ROAD){
				SV->num_out_east_right --;
			}
			else{
				if(M->car.arrived_from == WEST_RIGHT){
					SV->num_out_north_left--;
				}
				else if(M->car.arrived_from == NORTH_STRAIGHT){
					SV->num_out_north_straight--;
				}
				else if(M->car.arrived_from == EAST_LEFT){
					SV->num_out_north_right--;
				}
			}
			break;
		case NORTH_STRAIGHT:
			SV->num_in_north_straight++;
			if(M->car.y_to_go < 0 && SV->num_out_south_straight < MAX_CARS_ON_ROAD){
				SV->num_out_south_straight --;
			}
			else if(M->car.x_to_go > 0 && SV->num_out_south_left < MAX_CARS_ON_ROAD){
				SV->num_out_south_left --;
			}
			else if(M->car.x_to_go < 0 && SV->num_out_south_right < MAX_CARS_ON_ROAD){
				SV->num_out_south_right --;
			}
			else{
				if(M->car.arrived_from == WEST_RIGHT){
					SV->num_out_north_left--;
				}
				else if(M->car.arrived_from == NORTH_STRAIGHT){
					SV->num_out_north_straight--;
				}
				else if(M->car.arrived_from == EAST_LEFT){
					SV->num_out_north_right--;
				}
			}
			break;
		case NORTH_RIGHT:
			SV->num_in_north_right++;
			if(M->car.x_to_go < 0 && SV->num_out_west_straight < MAX_CARS_ON_ROAD){
				SV->num_out_west_straight --;
			}
			else if(M->car.y_to_go < 0 && SV->num_out_west_left < MAX_CARS_ON_ROAD){
				SV->num_out_west_left --;
			}
			else if(M->car.y_to_go > 0 && SV->num_out_west_right < MAX_CARS_ON_ROAD){
				SV->num_out_west_right --;
			}
			else{
				if(M->car.arrived_from == WEST_RIGHT){
					SV->num_out_north_left--;
				}
				else if(M->car.arrived_from == NORTH_STRAIGHT){
					SV->num_out_north_straight--;
				}
				else if(M->car.arrived_from == EAST_LEFT){
					SV->num_out_north_right--;
				}
			}
			break;
		case SOUTH_LEFT:
			SV->num_in_south_left++;
			if(M->car.x_to_go < 0 && SV->num_out_west_straight < MAX_CARS_ON_ROAD){
				SV->num_out_west_straight --;
			}
			else if(M->car.y_to_go < 0 && SV->num_out_west_left < MAX_CARS_ON_ROAD){
				SV->num_out_west_left --;
			}
			else if(M->car.y_to_go > 0 && SV->num_out_west_right < MAX_CARS_ON_ROAD){
				SV->num_out_west_right --;
			}
			else{
				if(M->car.arrived_from == WEST_LEFT){
					SV->num_out_south_right--;
				}
				else if(M->car.arrived_from == SOUTH_STRAIGHT){
					SV->num_out_south_straight--;
				}
				else if(M->car.arrived_from == EAST_RIGHT){
					SV->num_out_south_left--;
				}
			}

			break;
		case SOUTH_STRAIGHT:
			SV->num_in_south_straight++;
			if(M->car.y_to_go > 0 && SV->num_out_north_straight < MAX_CARS_ON_ROAD){
				SV->num_out_north_straight --;
			}
			else if(M->car.x_to_go < 0 && SV->num_out_north_left < MAX_CARS_ON_ROAD){
				SV->num_out_north_left --;
			}
			else if(M->car.x_to_go > 0 && SV->num_out_north_right < MAX_CARS_ON_ROAD){
				SV->num_out_north_right --;
			}
			else{
				if(M->car.arrived_from == EAST_RIGHT){
					SV->num_out_south_left--;
				}
				else if(M->car.arrived_from == SOUTH_STRAIGHT){
					SV->num_out_south_straight--;
				}
				else if(M->car.arrived_from == WEST_LEFT){
					SV->num_out_south_right--;
				}
			}
			break;
		case SOUTH_RIGHT:
			SV->num_in_south_right++;
			if(M->car.x_to_go > 0 && SV->num_out_east_straight < MAX_CARS_ON_ROAD){
				SV->num_out_east_straight --;
			}
			else if(M->car.y_to_go > 0 && SV->num_out_east_left < MAX_CARS_ON_ROAD){
				SV->num_out_east_left --;
			}
			else if(M->car.y_to_go < 0 && SV->num_out_east_right < MAX_CARS_ON_ROAD){
				SV->num_out_east_right --;
			}
			else{
				if(M->car.arrived_from == EAST_RIGHT){
					SV->num_out_south_left--;
				}
				else if(M->car.arrived_from == SOUTH_STRAIGHT){
					SV->num_out_south_straight--;
				}
				else if(M->car.arrived_from == WEST_LEFT){
					SV->num_out_south_right--;
				}
			}
			break;
		}

		// ts = tw_rand_exponential(lp->rng, MEAN_SERVICE);
		tw_rand_reverse_unif( lp->rng );
		break;
	}
}

void Intersection_Statistics_CollectStats(Intersection_State *SV, tw_lp * lp) 
{
	totalCars += SV->total_cars_arrived;
	carsFinished += SV->total_cars_finished;
}

