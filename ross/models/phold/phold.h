#ifndef INC_phold_h
#define INC_phold_h

#include <ross.h>

	/*
	 * PHOLD Types
	 */

typedef struct
{
	int argc;
	char **argv;
	char **env;
	int thread_id;
} param;

typedef struct phold_state phold_state;
typedef struct phold_message phold_message;

struct phold_state
{
	long int	 dummy_state;
};

struct phold_message
{
	long int	 dummy_data;
};

	/*
	 * PHOLD Globals
	 */

// rate for timestamp exponential distribution
static tw_stime mult = 1.4;
static tw_stime mean = 0;
tw_stime lookahead = 1.0;

#ifdef SIM_SLOW
static unsigned int nlp_per_pe = 1;
#else
static unsigned int nlp_per_pe = 128;
//static unsigned int nlp_per_pe = 32;
#endif

static int g_phold_start_events = 1;
static unsigned int stagger = 0;

//static int optimistic_memory = 100;
//static int optimistic_memory = 500;
//static int optimistic_memory = 1000;
//static int optimistic_memory = 5000;
//static int optimistic_memory = 10000;

static int optimistic_memory = 100000;

//static int optimistic_memory = 1000000; // Even this is occasionally not enough for Mattern with 64 threads
//static int optimistic_memory = 2000000;
//static int optimistic_memory = 10000000;

//static int optimistic_memory = 65536; // Default

static tw_stime percent_regional = 0.00;
static int epg = 0;
static int no_threads = 1;
static int imbalance_ratio = -1;

//static int possible_targets[12] = {0, 64, 128, 192, 1, 65, 129, 193, 2, 66, 130, 194};
//static int possible_targets[24] = {0, 64, 128, 192, 1, 65, 129, 193, 2, 66, 130, 194, 3, 67, 131, 195, 4, 68, 132, 196, 5, 69, 133, 197};

//static int possible_targets[32] = 
//{0, 64, 128, 192, 1, 65, 129, 193, 2, 66, 130, 194, 3, 67, 131, 195, 4, 68, 132, 196, 5, 69, 133, 197, 6, 7, 70, 71, 134, 135, 198, 199};

/*
static int possible_targets[63] =
{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15,
64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79,
128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 140, 141, 142, 143,
192, 193, 194, 195, 196, 197, 198, 199, 200, 201, 202, 203, 204, 205, 206};
*/

//static int possible_targets[126];
//static int possible_targets_2[126];

//static int possible_targets[252];

#ifdef DDS 
volatile static unsigned char controller_init_done = 0;
#endif
#endif
