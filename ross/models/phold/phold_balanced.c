#include "phold.h"
#include <pthread.h>
#include <mpi.h>
#include <papi.h>
#include <assert.h>

tw_peid
phold_map(tw_lpid gid)
{
	return (tw_peid) gid / g_tw_nlp;
}

void
phold_init(phold_state * s, tw_lp * lp)
{
	int i;
	if (stagger) // ?
	{
	    for (i = 0; i < g_phold_start_events; i++)
	    {
        	tw_event *e = tw_event_new(lp->gid,
            	tw_rand_exponential(lp->rng, mean) + lookahead +
        		(tw_stime)(lp->gid % (unsigned int)g_tw_ts_end), lp);

			tw_event_send(e);
	    }
	}
	else
	{
		// Send itself some messages before Sim starts
	    for (i = 0; i < g_phold_start_events; i++)
	    {
           	tw_event *e = tw_event_new(lp->gid, tw_rand_exponential(lp->rng, mean) + lookahead, lp);	
			tw_event_send(e);
	   	}
	}
}

void
phold_event_handler(phold_state * s, tw_bf * bf, phold_message * m, tw_lp * lp)
{ 
	// Do not optimize, do not put in the registers, go to memory at each access
	volatile double result = 0;

    for (int i = 0; i < epg; i++)
    {
    	result = result + (i * 315789) / 12345;
        result += 30000;
    }
	
	tw_lpid	 dest = 0;
	tw_event * e = NULL;
	
	tw_stime percent = tw_rand_unif(lp->rng);
	
	if (percent <= percent_regional)
	{
		// Normal model
		dest = tw_rand_integer(lp -> rng, 0, nlp_per_pe * no_threads - 1);
		
		// For 2-2 Comm: 2 threads communucate and exclusively while other 2 are not choosen as destination
		//if (g_tw_tid < 2) dest = tw_rand_integer(lp -> rng, 0, nlp_per_pe * 2 - 1);
		//if (g_tw_tid < 3) dest = tw_rand_integer(lp -> rng, 0, nlp_per_pe * 3 - 1);
		
	    if (dest == lp -> gid) bf -> c1 = 0;
   	    else bf -> c1 = 1;	
	}
    else
    {
        bf->c1 = 0;
        dest = lp -> gid;
    }

	if (dest < 0 || dest >= (g_tw_nlp * tw_nnodes()))
    {
    	printf("\ng_tw_pid = %d, g_tw_tid = %d, dest = %lu\n", g_tw_pid, g_tw_tid, dest);  
		tw_error(TW_LOC, "Bad Dest\n");
    }
	
	// dest lp id, offset, source lp (sender)
    //fprintf(stderr, "src: %u, dest: %lu\n", g_tw_tid, dest/128);

	// Last 2 threads do not send messages. Do not receive also.
	//if (g_tw_tid < 2)
	{
		e = tw_event_new(dest, tw_rand_exponential(lp->rng, mean) + lookahead, lp); // mean: 0, lookahead: 1
		tw_event_send(e);
	}
}

void
phold_event_handler_rc(phold_state * s, tw_bf * bf, phold_message * m, tw_lp * lp)
{
	tw_rand_reverse_unif(lp->rng);
	tw_rand_reverse_unif(lp->rng);

	if(bf->c1 == 1) tw_rand_reverse_unif(lp->rng);
}

void
phold_finish(phold_state * s, tw_lp * lp)
{
}

tw_lptype mylps[] = 
{
	{(init_f) phold_init,
	 (event_f) phold_event_handler,
	 (revent_f) phold_event_handler_rc,
	 (final_f) phold_finish,
	 (map_f) phold_map,
	sizeof(phold_state)},
	{0},
};

const tw_optdef app_opt[] =
{
	TWOPT_GROUP("PHOLD Model"),
	TWOPT_STIME("remote", percent_regional, "desired remote event rate"),
	TWOPT_UINT("nlp", nlp_per_pe, "number of LPs per processor"),
	TWOPT_STIME("mean", mean, "exponential distribution mean for timestamps"),
	TWOPT_STIME("mult", mult, "multiplier for event memory allocation"),
	TWOPT_STIME("lookahead", lookahead, "lookahead for events"),
	TWOPT_UINT("start-events", g_phold_start_events, "number of initial messages per LP"),
	TWOPT_UINT("stagger", stagger, "Set to 1 to stagger event uniformly across 0 to end time."),
	TWOPT_UINT("memory", optimistic_memory, "additional memory buffers"),
	TWOPT_END()
};

// For single core, 4-way SMT affinity
void set_single_core_affinity ()
{
        pthread_t thread = pthread_self();
        
		cpu_set_t cpuMask;
        CPU_ZERO(&cpuMask);
		
		int cpu_id = -1;

		//if (g_tw_tid < 4) cpu_id = g_tw_tid * 64;
		//else cpu_id = 1;

		// Always pin the controller on a seperate core
		if (g_tw_tid < 2) cpu_id = g_tw_tid * 64;
		else cpu_id = 1;

		CPU_SET(cpu_id, &cpuMask);

        int s = pthread_setaffinity_np(thread, sizeof(cpu_set_t), &cpuMask);
        if (s != 0) {perror("\nCPU Affinity Broken\n\n"); assert(0);}
}

void set_cpu_affinity (int rank)
{
	int new_rank = rank;

	if (rank >= 63) new_rank++;
	if (new_rank >= 127) new_rank++;
	if (new_rank >= 191) new_rank++;	


	int cpuId = new_rank;
	//int cpuId = rank;
    
	cpu_set_t cpuMask;
    pthread_t thread = pthread_self();

    CPU_ZERO(&cpuMask);
    CPU_SET(cpuId, &cpuMask);

    if (CPU_ISSET(cpuId, &cpuMask) >= 0)
   	{
    	int status = pthread_setaffinity_np(thread, sizeof(cpu_set_t), &cpuMask);

        if(status !=0)
        {
        	printf("Rank %d, cpuID %d\n", rank, cpuId);
            perror("NPH: sched_setaffinity\n");
        }
	}
    else perror("Error in Set Affinity\n");
}

void *
thread_main(void *arg)
{
	//tw_opt_add(app_opt);
	//g_tw_rng_default = TW_FALSE;

	g_tw_n_threads = no_threads;
	g_tw_lookahead = lookahead; 
   	g_tw_memory_nqueues = 16; 

	// events_per_pe = 10,179. mult (1.4) x nlp (128) = 179
	g_tw_events_per_pe = (mult * nlp_per_pe * g_phold_start_events) + optimistic_memory;

	param *p = (param *) arg;
	tw_init(&p->argc, &p->argv, p->thread_id); // in tw_setup, calls gvt_start()

	tw_define_lps(nlp_per_pe, sizeof(phold_message), 0);
	for(int i = 0; i < g_tw_nlp; i++) tw_lp_settype(i, &mylps[0]);
 
    if (g_tw_mynode == 0)
	{
		#ifdef DDS 
        printf("Spawning %d Threads: (1 Controller - %d Worker)\n", no_threads + 1, no_threads);
        #else
        printf("Spawning %d Threads\n", no_threads);
        #endif
  
	    printf("========================================\n");
	    printf("PHOLD Model Configuration..............\n");
	    printf("   Lookahead..................%.2lf\n", g_tw_lookahead);
	    printf("   Start-events...............%u\n", g_phold_start_events);
	    printf("   stagger....................%u\n", stagger);
	    printf("   Mean.......................%.2lf\n", mean);
	   	printf("   Mult.......................%.2lf\n", mult);
	    printf("   Memory.....................%u\n", optimistic_memory);
	    printf("   Remote Percentage..........%.0lf\n", percent_regional * 100);
        printf("   EPC........................%d\n", epg);
		printf("========================================\n\n");
	}
	
	//set_single_core_affinity(); 
    set_cpu_affinity(g_tw_tid);

	#ifdef DDS 
        while (1)
        {
            if (controller_init_done)
            {
                tw_run (0, 0, NULL, NULL, NULL);
                break;
            }
        }
    #else
        tw_run (0, 0, NULL, NULL, NULL);
    #endif

	tw_end();

	pthread_exit(NULL);
	return NULL;
}

#ifdef DDS 
void *
controller_main (void * i)
{
    //int * rank = (int *) i;
	//set_cpu_affinity(*rank);

	int rank = 63;
	int cpuId = rank;
	cpu_set_t cpuMask;
    pthread_t thread = pthread_self();

    CPU_ZERO(&cpuMask);
    CPU_SET(cpuId, &cpuMask);

    if (CPU_ISSET(cpuId, &cpuMask) >= 0)
   	{
    	int status = pthread_setaffinity_np(thread, sizeof(cpu_set_t), &cpuMask);

        if(status !=0)
        {
        	printf("Rank %d, cpuID %d\n", rank, cpuId);
            perror("NPH: sched_setaffinity\n");
        }
	}
    else perror("Error in Set Affinity\n");


    g_tw_n_threads = no_threads;
	init_master_check ();
    controller_init_done = 1;
    master_check();

    pthread_exit(NULL);
    return NULL;
}
#endif


int main(int argc, char **argv, char **env)
{	
	#ifdef DDS 
	if(argc != 7)
	#else
	if(argc != 6)
	#endif
    {
    	printf("Usage: phold --sync=x threads percent_regional epg gvt_interval zero_counter_threshold\n");
        exit(0);
    }
        
    no_threads = atoi(argv[2]); // Last one is the master
    argc--;

    percent_regional = atof(argv[3]);
    argc--;
      
    epg = atof(argv[4]);
    argc--;
      
    g_tw_gvt_interval = atof(argv[5]);
    argc--;

	#ifdef DDS 
	zero_counter_threshold = atof(argv[6]);
	argc--;
    no_threads--; // last thread is the controller
    #endif

	#ifdef PAPI
	if (PAPI_library_init(PAPI_VER_CURRENT) != PAPI_VER_CURRENT) {printf("PAPI not initialized\n"); exit(1);}
    if (PAPI_query_event(PAPI_TOT_INS) != PAPI_OK) printf("Event does not present\n");
    if (PAPI_query_event(PAPI_TOT_CYC) != PAPI_OK) printf("Event does not present\n");
	#endif

	pthread_t * threads;
	param p = {argc,argv,env,0};
 	param * prm;

	threads = (pthread_t *) malloc(no_threads * sizeof(*threads));
	prm = (param *) malloc(sizeof(param) * no_threads);

	for(int i = 0; i < 2; i++)
    {
        init_barrier(&sum_barrier[i], no_threads);
        init_barrier(&min_barrier[i], no_threads);
    }

    init_barrier(&g_barrier, no_threads);
    init_barrier(&max_barrier,no_threads);
    sum_index = 0;
    min_index = 0;
	
	// Spawn the controller seperately
	#ifdef DDS 
	pthread_t controller;
	pthread_create(&controller, NULL, controller_main, (void *) &no_threads);
	#endif

	for(int i = 0; i < no_threads; i++)
	{
		prm[i] = p;
		prm[i].thread_id = i;
		pthread_create(&threads[i], NULL, thread_main, (void *) &prm[i]);
	}
	
	for(int i = 0; i < no_threads; i++) pthread_join(threads[i], NULL);

	#ifdef DDS 
    pthread_join(controller, NULL);
    #endif

	for(int i = 0; i < 2; i++)
	{
        destroy_barrier(&sum_barrier[i]);
        destroy_barrier(&min_barrier[i]);
	}
    
	destroy_barrier(&g_barrier);
	destroy_barrier(&max_barrier);
	
	free(threads);
	free(prm);
	return 0;	
}

