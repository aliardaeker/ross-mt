#ifndef INC_phold_h
#define INC_phold_h

#include <ross.h>

	/*
	 * PHOLD Types
	 */

typedef struct phold_state phold_state;
typedef struct phold_message phold_message;

struct phold_state
{
	long int	 dummy_state;
};

struct phold_message
{
	long int	 dummy_data;
};

	/*
	 * PHOLD Globals
	 */

// rate for timestamp exponential distribution
static tw_stime g_mean = 0;
static __thread tw_stime mean = 0;
static tw_stime g_lookahead = 1.0;
static __thread tw_stime lookahead = 1.0;

static tw_stime mult = 1.4;
static unsigned int stagger = 0;

static unsigned int nlp_per_pe = 128;
static int g_phold_start_events = 1;

static tw_stime percent_regional=0.00;
static int epg = 0;
int no_threads = 1;
int gvt_interval = 1;

//static int optimistic_memory = 100;
//static int optimistic_memory = 500;
//static int optimistic_memory = 1000;
//static int optimistic_memory = 5000;
//static int optimistic_memory = 10000;
//static int optimistic_memory = 100000;
//static int optimistic_memory = 1000000;
static int optimistic_memory = 10000000;

static char run_id[1024] = "undefined";

#ifdef SCHED
pthread_barrier_t barrier;
#endif
#endif
