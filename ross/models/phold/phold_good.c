#include "phold.h"
#include <pthread.h>
#include <mpi.h>
#include <assert.h>
#include <stdlib.h>	
#include <string.h>

tw_peid
phold_map(tw_lpid gid)
{
	return (tw_peid) gid / g_tw_nlp;
}

void
phold_init(phold_state * s, tw_lp * lp)
{
	int i;
	if (stagger) // ?
	{
	    for (i = 0; i < g_phold_start_events; i++)
	    {
        	tw_event *e = tw_event_new(lp->gid,
            	tw_rand_exponential(lp->rng, mean) + lookahead +
        		(tw_stime)(lp->gid % (unsigned int)g_tw_ts_end), lp);

			tw_event_send(e);
	    }
	}
	else
	{
		// Favorable Imbalance
		//if (g_tw_tid >= 12) return; // First 12 threads have messages	

		// Unfavorable Imbalance
		//if (g_tw_tid % 64 > 2) return;
		//if (g_tw_tid % 64 > 5) return;
		
		//if (g_tw_tid % 64 > 7) return;
		//if (g_tw_tid % 64 > 15 || g_tw_tid == 207) return;
		//if (g_tw_tid % 64 > 31|| g_tw_tid == 159 || g_tw_tid == 223) return;
		//if (g_tw_tid % 64 > 62) return;

		// Send itself some messages before Sim starts
	    for (i = 0; i < g_phold_start_events; i++)
	    {
           	tw_event *e = tw_event_new(lp->gid, tw_rand_exponential(lp->rng, mean) + lookahead, lp);	
			tw_event_send(e);
	   	}
	}
}

void
phold_event_handler(phold_state * s, tw_bf * bf, phold_message * m, tw_lp * lp)
{ 
	// Do not optimize, do not put in the registers, go to memory at each access
	volatile double result = 0;

    for (int i = 0; i < epg; i++)
    {
    	result = result + (i * 315789) / 12345;
        result += 30000;
    }
	
	tw_lpid	 dest = 0;
	tw_event * e = NULL;
	tw_stime percent = tw_rand_unif(lp -> rng);
	
	if (percent <= percent_regional) // X% Remote messages
	{
		// Favorable Imbalanced Model
		int p = percent_comp_ret ();

		int first_dest, last_dest;
		int region = 100 / imbalance_ratio;

		for (int i = 1; i <= imbalance_ratio; i++)
		{
			if (p <= (i * region))
			{
				first_dest = nlp_per_pe * (no_threads * (i - 1) / imbalance_ratio);
				last_dest = nlp_per_pe * (no_threads * i / imbalance_ratio) - 1;

				dest = tw_rand_integer(lp -> rng, first_dest, last_dest);

				break;
			}
				
			if (i == imbalance_ratio)
			{
				first_dest = nlp_per_pe * (no_threads * (i - 1) / imbalance_ratio);
				last_dest = nlp_per_pe * (no_threads * i / imbalance_ratio) - 1;
				dest = tw_rand_integer(lp -> rng, first_dest, last_dest);
			}
		}

	    if (dest == lp -> gid) bf -> c1 = 0;
   	    else bf -> c1 = 1;	
	}
	else
    {
        bf -> c1 = 0;
        dest = lp -> gid;
    }

	if (dest < 0 || dest >= (g_tw_nlp * no_threads))
    {
    	printf("BAD DEST: g_tw_tid = %d, dest = %lu\n", g_tw_tid, dest / 128);  
		exit(1);
    }

	// Balanced Model
	//dest = tw_rand_integer(lp -> rng, 0, nlp_per_pe * no_threads - 1);
	
	// mean: 0, lookahead: 1
	// e -> recv_ts == (lp -> kp -> last_time) + (lp -> rng + 1)
	e = tw_event_new(dest, tw_rand_exponential(lp->rng, mean) + lookahead, lp);
	tw_event_send(e);
}

void
phold_event_handler_rc(phold_state * s, tw_bf * bf, phold_message * m, tw_lp * lp)
{
	tw_rand_reverse_unif(lp->rng);
	tw_rand_reverse_unif(lp->rng);

	if(bf->c1 == 1) tw_rand_reverse_unif(lp->rng);
}

void
phold_finish(phold_state * s, tw_lp * lp)
{
}

tw_lptype mylps[] = 
{
	{(init_f) phold_init,
	 (event_f) phold_event_handler,
	 (revent_f) phold_event_handler_rc,
	 (final_f) phold_finish,
	 (map_f) phold_map,
	sizeof(phold_state)},
	{0},
};

const tw_optdef app_opt[] =
{
	TWOPT_GROUP("PHOLD Model"),
	TWOPT_STIME("remote", percent_regional, "desired remote event rate"),
	TWOPT_UINT("nlp", nlp_per_pe, "number of LPs per processor"),
	TWOPT_STIME("mean", mean, "exponential distribution mean for timestamps"),
	TWOPT_STIME("mult", mult, "multiplier for event memory allocation"),
	TWOPT_STIME("lookahead", lookahead, "lookahead for events"),
	TWOPT_UINT("start-events", g_phold_start_events, "number of initial messages per LP"),
	TWOPT_UINT("stagger", stagger, "Set to 1 to stagger event uniformly across 0 to end time."),
	TWOPT_UINT("memory", optimistic_memory, "additional memory buffers"),
	TWOPT_END()
};

void set_cpu_affinity (int rank)
{
	int cpuId = rank % 256;
	
	cpu_set_t cpuMask;
    pthread_t thread = pthread_self();

    CPU_ZERO(&cpuMask);
    CPU_SET(cpuId, &cpuMask);

   	if (CPU_ISSET(cpuId, &cpuMask) >= 0)
    {
    	int status = pthread_setaffinity_np(thread, sizeof(cpu_set_t), &cpuMask);

        if(status !=0)
        {
        	printf("Rank %d, cpuID %d\n", rank, cpuId);
            perror("NPH: sched_setaffinity\n");
        }
	}
    else perror("Error in Set Affinity\n");
}

void *
thread_main(void *arg)
{
	//tw_opt_add(app_opt);
	//g_tw_rng_default = TW_FALSE;

	g_tw_n_threads = no_threads;
	g_tw_lookahead = lookahead; 
   	g_tw_memory_nqueues = 16; 

	// events_per_pe = 10,179. mult (1.4) x nlp (128) = 179
	g_tw_events_per_pe = (mult * nlp_per_pe * g_phold_start_events) + optimistic_memory;

	param *p = (param *) arg;
	tw_init(&p->argc, &p->argv, p->thread_id); // in tw_setup, calls gvt_start()

	tw_define_lps(nlp_per_pe, sizeof(phold_message), 0);
	for(int i = 0; i < g_tw_nlp; i++) tw_lp_settype(i, &mylps[0]);
 
    if (g_tw_mynode == 0)
	{
        printf("Spawning %d Threads\n", no_threads);
        //#endif
  
	    printf("========================================\n");
	    printf("PHOLD 1/%d Imbalanced Model:\n", imbalance_ratio);
	    printf("   Lookahead..................%.2lf\n", g_tw_lookahead);
	    printf("   Start-events...............%u\n", g_phold_start_events);
	    printf("   stagger....................%u\n", stagger);
	    printf("   Mean.......................%.2lf\n", mean);
	   	printf("   Mult.......................%.2lf\n", mult);
	    printf("   Memory.....................%u\n", optimistic_memory);
	    printf("   Remote Percentage..........%.0lf\n", percent_regional * 100);
        printf("   EPC........................%d\n", epg);
		printf("========================================\n\n");
	}

    set_cpu_affinity(g_tw_tid);

	// Dynamic Affinity
	//affinity_table[g_tw_tid] = -1;
	//sys_tid_table[g_tw_tid] = syscall(SYS_gettid); 

    tw_run (0, 0, NULL, NULL, NULL);
	tw_end();

	//fprintf(stderr, "%d exit\n", g_tw_tid);
	pthread_exit(NULL);
}

int main(int argc, char **argv, char **env)
{	
	#ifdef DDS 
	if(argc != 7)
	#else
	if(argc != 6)
	#endif
    {
    	printf("Usage: phold --sync=x threads percent_regional epg gvt_interval zero_counter_threshold\n");
        exit(1);
    }
        
    no_threads = atoi(argv[2]); // Last one is the master
    argc--;

    percent_regional = atof(argv[3]);
    argc--;
      
    epg = atof(argv[4]);
    argc--;
      
    g_tw_gvt_interval = atof(argv[5]);
    argc--;

	#ifdef DDS 
	/*
	for (int i = 0; i < 32; i++) possible_targets[i] = i;
	for (int i = 32; i < 64; i++) possible_targets[i] = i + 32;
	for (int i = 64; i < 95; i++) possible_targets[i] = i + 64;
	for (int i = 95; i < 126; i++) possible_targets[i] = i + 97;

	for (int i = 0; i < 31; i++) possible_targets_2[i] = i + 32;
	for (int i = 31; i < 62; i++) possible_targets_2[i] = i + 65;
	for (int i = 62; i < 94; i++) possible_targets_2[i] = i + 97;
	for (int i = 94; i < 126; i++) possible_targets_2[i] = i + 129;
	*/

	//for (int i = 0; i < 63; i++) possible_targets[i] = i;
	//for (int i = 63; i < 126; i++) possible_targets[i] = i + 1;
	//for (int i = 126; i < 189; i++) possible_targets[i] = i + 2;
	//for (int i = 189; i < 252; i++) possible_targets[i] = i + 3;

	zero_counter_threshold = atof(argv[6]);
	argc--;
    #endif

	#ifdef PAPI
	if (PAPI_library_init(PAPI_VER_CURRENT) != PAPI_VER_CURRENT) {printf("PAPI not initialized\n"); exit(1);}
    if (PAPI_query_event(PAPI_TOT_INS) != PAPI_OK) printf("Event does not present\n");
    if (PAPI_query_event(PAPI_TOT_CYC) != PAPI_OK) printf("Event does not present\n");
	#endif

	pthread_t * threads;
	param p = {argc,argv,env,0};
 	param * prm;

	threads = (pthread_t *) malloc(no_threads * sizeof(*threads));
	prm = (param *) malloc(sizeof(param) * no_threads);
	init_barrier(&g_barrier, no_threads);

	#ifdef ROSS_GVT_mpi_allreduce
	for(int i = 0; i < 2; i++)
    {
        init_barrier(&sum_barrier[i], no_threads);
        init_barrier(&min_barrier[i], no_threads);
    }
    sum_index = 0;
    min_index = 0;
	#endif
	
	#ifdef DDS 
	init_master_check (no_threads);
	#endif

	for(int i = 0; i < no_threads; i++)
	{
		prm[i] = p;
		prm[i].thread_id = i;

		int ret;
		if ((ret = pthread_create(&threads[i], NULL, thread_main, (void *) &prm[i]))) fprintf(stderr, "Error: %d\n", ret);
		
		//#ifdef DDS 
		//	tid_table[i] = threads[i];
		//#endif
	}
	
	for(int i = 0; i < no_threads; i++) pthread_join(threads[i], NULL);

	#ifdef ROSS_GVT_mpi_allreduce
	for(int i = 0; i < 2; i++)
	{
        destroy_barrier(&sum_barrier[i]);
        destroy_barrier(&min_barrier[i]);
	}
	#endif
	
	destroy_barrier(&g_barrier);
	free(threads);
	free(prm);
	return 0;	
}

/*
#ifdef DDS 
void *
controller_main (void * i)
{
	// For reserving a core for the controller when oversubscribed	
	#ifdef OVER
	char s_tid[10];
	pid_t tid = syscall(SYS_gettid);
	sprintf(s_tid, "%d", tid);

	char command[100] = "sudo /home/aeker801/libraries/cpuset-bin/bin/cset shield --shield --pid ";
	strcat(command, s_tid);
	system(command);
	#else
	// For reserving a core for the controller
	int rank = 63;
	int cpuId = rank;
	cpu_set_t cpuMask;
    pthread_t thread = pthread_self();

    CPU_ZERO(&cpuMask);
    CPU_SET(cpuId, &cpuMask);

    if (CPU_ISSET(cpuId, &cpuMask) != 0)
   	{
    	int status = pthread_setaffinity_np(thread, sizeof(cpu_set_t), &cpuMask);

        if(status !=0)
        {
        	printf("Rank %d, cpuID %d\n", rank, cpuId);
            perror("NPH: sched_setaffinity\n");
        }
	}
    else perror("Error in Set Affinity\n");
	#endif

    g_tw_n_threads = no_threads;
	init_master_check ();
    controller_init_done = 1;
    master_check();

	//fprintf(stderr, "controller exit\n");
    pthread_exit(NULL);
}
#endif
*/

		// Shifting Unfavorable Imbalance
		/*
		int p = percent_comp_ret (), x;
		int region = 100 / imbalance_ratio;
		int select = tw_rand_integer(lp -> rng, 0, 125);

		for (int i = 1; i <= imbalance_ratio; i++)
		{
			if (p <= (i * region)) 
			{	
				x = possible_targets[select];
				break;
			}
			else 
			{	
				x = possible_targets_2[select];
				break;
			}
		}
			
		int first_dest = nlp_per_pe * x;
		int last_dest = nlp_per_pe * (x + 1) - 1;
		dest = tw_rand_integer(lp -> rng, first_dest, last_dest);
		*/

		// Unfavorable Imbalance
		//int i = tw_rand_integer(lp -> rng, 0, 11);
		//int i = tw_rand_integer(lp -> rng, 0, 23);

		//int i = tw_rand_integer(lp -> rng, 0, 31);
		//int i = tw_rand_integer(lp -> rng, 0, 62);
		//int i = tw_rand_integer(lp -> rng, 0, 125);
		//int i = tw_rand_integer(lp -> rng, 0, 251);

		/*
		int x = possible_targets[i];
		
		int first_dest = nlp_per_pe * x;
		int last_dest = nlp_per_pe * (x + 1) - 1;
		dest = tw_rand_integer(lp -> rng, first_dest, last_dest);
		*/


