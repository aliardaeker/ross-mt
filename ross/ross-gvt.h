#ifndef INC_ross_gvt_h
#define INC_ross_gvt_h

/*
 * Initialize the GVT library and parse options.
 */

/* setup the GVT library (config cmd line args, etc) */
extern const tw_optdef *tw_gvt_setup(void);

/* start the GVT library (init vars, etc) */
extern void tw_gvt_start(void);

/* 
 * GVT computation is broken into two stages:
 * stage 1: determine if GVT computation should be started
 * stage 2: compute GVT
 */

extern void print_stat(tw_pe * me, bool fail);
extern void decrement_active_thread_count();
tw_stime tw_inq_minimum(tw_pe * me);
extern void tw_gvt_step1(tw_pe *);

#if !defined(ROSS_GVT_mattern) && !defined(ROSS_GVT_wait_free)
extern void tw_gvt_step2(tw_pe *);
#endif

/*
 * Provide a mechanism to force a GVT computation outside of the 
 * GVT interval (optional)
 */
extern void tw_gvt_force_update(tw_pe *);

/* Set the PE GVT value */
extern int tw_gvt_set(tw_pe * pe, tw_stime LVT);

/* Returns true if GVT in progress, false otherwise */
static inline bool tw_gvt_inprogress(tw_pe * pe);

/* Statistics collection and printing function */
extern void tw_gvt_stats(FILE * F);

extern void gvt_init(tw_pe * me);

extern tw_stime min_pq_outq(tw_pe * me);
extern void finish_gvt(tw_pe * me);
extern void gvt_done();

extern tw_stime min_pq();
extern tw_stime min_transit();
extern tw_stime min_eventq();
extern tw_stime min_cancelq();

#ifdef ROSS_GVT_mattern
extern void mattern_receive_track (tw_pe * me, tw_event * e);
extern void mattern_send_track (tw_pe * me, tw_event * cp_e, tw_event * e, int dest_node_id);

extern void accumulate_msg_counters(tw_pe * me);

extern void update_control();
extern void reset_control_message();
#endif

#ifdef DDS 
extern void barrier_wait_atomic();

extern unsigned int get_gvt_count();

extern int num_active_threads ();
extern int num_real_active_threads ();
extern void gvt_stat_print(int i);

extern bool controller_busy();
extern void controller_done();

extern void self_block (tw_pe * me);
extern void increment_active_thread_count();
extern void increase_active_thread_count(int n);
extern bool gvt_not_inprogress ();
extern void maximize_active_thread_count();
#endif

#endif
