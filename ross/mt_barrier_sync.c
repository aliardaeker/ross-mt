#include "mt_barrier_sync.h"
#include <ross.h>

void update_barrier(barrier_t * barrier, int u)
{
	barrier -> count = u;
    barrier -> n_threads = u;
}

void init_barrier(barrier_t *barrier,int n)
{
	pthread_mutex_init(&(barrier->mutex),NULL);
	pthread_barrier_init(&(barrier -> barrier), NULL, n);

	barrier -> count = n;
    barrier -> n_threads = n;
}

void barrier_sync(barrier_t *barrier,long tid)
{
	pthread_barrier_wait(&(barrier -> barrier));
}

void destroy_barrier(barrier_t *barrier)
{
	pthread_mutex_destroy(&(barrier -> mutex));        
	pthread_barrier_destroy(&(barrier -> barrier));
}


int mt_sum(void *in_no,void *out_no,int in_cnt,int type,barrier_t *barrier){
    int status,i;
	double db_no[32],*db_ptr_no=(double *)in_no;
	long long  ll_no[32],*ll_ptr_no=(long long *)in_no;

       // printf("\nLock taken %ld barrier->count=%d barrier->n_threads=%d\n",tid,barrier->count,barrier->n_threads);

	switch(type){
		case MT_DOUBLE:
			for(i=0;i<in_cnt;i++){
				db_no[i] = db_ptr_no[i];
			}
			break;
		case MT_LONG_LONG:
			for(i=0;i<in_cnt;i++){
				ll_no[i] = ll_ptr_no[i];
			}
			break;
	}

        status=pthread_mutex_lock(&(barrier->mutex));
        if (status != 0){
                printf("\nLock failed :%d Error:%d\n",status,EINVAL);
                return -1;
        }
	// Intialize result to zero
	if(barrier->count == barrier->n_threads){
		switch(type){
			case MT_DOUBLE:
				for (i = 0; i < in_cnt; i++)
				{
					barrier -> db_result[i] = 0;
				}
				break;
			case MT_LONG_LONG:
				for(i=0;i<in_cnt;i++){
					barrier->ll_result[i] = 0;
				}
				break;
		}
	}
        barrier->count=barrier->count-1;
	//actual sum operation
	switch(type){
		case MT_DOUBLE:
			for(i=0;i<in_cnt;i++){
				barrier->db_result[i] += db_no[i];
			#ifdef MT_DBG
			if(barrier_dbg==1) printf("Sum barrier->db_result[%d]=%lf db_no[%d]=%lf thread_no=%d\n",i,barrier->db_result[i],i,db_no[i],g_tw_mynode);
			#endif
			}
			break;
		case MT_LONG_LONG:
			for(i=0;i<in_cnt;i++){
				barrier->ll_result[i] += ll_no[i];
			#ifdef MT_DBG
			if(barrier_dbg==1) printf("Sum barrier->ll_result[%d]=%lld ll_no[%d]=%lld thread_no=%d\n",i,barrier->ll_result[i],i,ll_no[i],g_tw_mynode);
			#endif
			}
			//printf("Sum barrier->ll_result=%lld ll_no=%lld thread_no=%d\n",barrier->ll_result,ll_no,g_tw_mynode);
			break;
	}

	pthread_mutex_unlock(&(barrier -> mutex));

	if (pthread_barrier_wait(&(barrier -> barrier)) == PTHREAD_BARRIER_SERIAL_THREAD) barrier -> count = barrier -> n_threads;

        //put result back before returning
	db_ptr_no=(double *)out_no;
	ll_ptr_no=(long long *)out_no;
        switch(type){
                case MT_DOUBLE:
			for(i=0;i<in_cnt;i++){
                        db_ptr_no[i] = barrier->db_result[i];
			#ifdef MT_DBG
			if(barrier_dbg==1) printf("Sum Done => barrier->db_result[%d]=%lf db_ptr_no[%d]=%lf thread_no=%d\n",i,barrier->db_result[i],i,db_ptr_no[i],g_tw_mynode);
			#endif
			}
                        break;
                case MT_LONG_LONG:
			for(i=0;i<in_cnt;i++){
                        ll_ptr_no[i] = barrier->ll_result[i];
			#ifdef MT_DBG
			if(barrier_dbg==1) printf("Sum Done => barrier->ll_result[%d]=%lld db_ptr_no[%d]=%lld thread_no=%d\n",i,barrier->ll_result[i],i,ll_ptr_no[i],g_tw_mynode);
			#endif
			}
                        break;
        }
//printf("Sum Done all threads are in sync: barrier->ll_result=%lld ll_no=%lld thread_no=%d\n",barrier->ll_result,ll_no,g_tw_mynode);
        
	return 0;
}
int mt_min(void *in_no,void *out_no,int in_cnt,int type,barrier_t *barrier){
        int status,i;
	double db_no[32],*db_ptr_no=(double *)in_no;
	long long  ll_no[32],*ll_ptr_no=(long long *)in_no;

       // printf("\nLock taken %ld barrier->count=%d barrier->n_threads=%d\n",tid,barrier->count,barrier->n_threads);

        switch(type){
                case MT_DOUBLE:
			for(i=0;i<in_cnt;i++){
                        db_no[i] = db_ptr_no[i];
			}
                        break;
                case MT_LONG_LONG:
			for(i=0;i<in_cnt;i++){
                        ll_no[i] = ll_ptr_no[i];
			}
                        break;
        }

        status=pthread_mutex_lock(&(barrier->mutex));
        if (status != 0){
                printf("\nLock failed :%d Error:%d\n",status,EINVAL);
                return -1;
        }
        // Intialize result to zero
        if(barrier->count == barrier->n_threads){
                switch(type){
                        case MT_DOUBLE:
				for(i=0;i<in_cnt;i++){
                                	barrier->db_result[i] = db_no[i];
				}
                                break;
                        case MT_LONG_LONG:
				for(i=0;i<in_cnt;i++){
                                	barrier->ll_result[i] = ll_no[i];
				}
                                break;
                }
        }
        barrier->count=barrier->count-1;
        //actual sum operation
        switch(type){
                case MT_DOUBLE:
			for(i=0;i<in_cnt;i++){
                        	if(db_no[i] < barrier->db_result[i])
					barrier->db_result[i] = db_no[i];
			}
			//printf("Min barrier->db_result=%lf db_no=%lf thread_no=%d\n",barrier->db_result,db_no,g_tw_mynode);
                        break;
                case MT_LONG_LONG:
			for(i=0;i<in_cnt;i++){
                        	if(ll_no[i] < barrier->ll_result[i])
					barrier->ll_result[i] = ll_no[i];
			}
                        break;
        }
	
		pthread_mutex_unlock(&(barrier -> mutex));

		if (pthread_barrier_wait(&(barrier -> barrier)) == PTHREAD_BARRIER_SERIAL_THREAD)
		{
			barrier -> count = barrier -> n_threads;
		}

        //put result back before returning
	db_ptr_no=(double *)out_no;
	ll_ptr_no=(long long *)out_no;
        switch(type){
                case MT_DOUBLE:
			for(i=0;i<in_cnt;i++){
                        	db_ptr_no[i] = barrier->db_result[i];
			}
                        break;
                case MT_LONG_LONG:
			for(i=0;i<in_cnt;i++){
                        	ll_ptr_no[i] = barrier->ll_result[i];
			}
                        break;
        }
//printf("Min Done all threads are in sync: barrier->db_result=%lf db_no=%lf thread_no=%d\n",barrier->db_result,db_no,g_tw_mynode);
        //Don't unlock until all threads are done
        
	return 0;
}

int mt_max(void *in_no,void *out_no,int in_cnt,int type,barrier_t *barrier){
        int status,i;
	double db_no[32],*db_ptr_no=(double *)in_no;
	long long  ll_no[32],*ll_ptr_no=(long long *)in_no;

       // printf("\nLock taken %ld barrier->count=%d barrier->n_threads=%d\n",tid,barrier->count,barrier->n_threads);

        switch(type){
                case MT_DOUBLE:
			for(i=0;i<in_cnt;i++)
                        	db_no[i] = db_ptr_no[i];
                        break;
                case MT_LONG_LONG:
			for(i=0;i<in_cnt;i++)
                        	ll_no[i] = ll_ptr_no[i];
                        break;
        }

        status=pthread_mutex_lock(&(barrier->mutex));
        if (status != 0){
                printf("\nLock failed :%d Error:%d\n",status,EINVAL);
                return -1;
        }
        // Intialize result to zero
        if(barrier->count == barrier->n_threads){
                switch(type){
                        case MT_DOUBLE:
				for(i=0;i<in_cnt;i++)
                                	barrier->db_result[i] = db_no[i];
                                break;
                        case MT_LONG_LONG:
				for(i=0;i<in_cnt;i++)
                                	barrier->ll_result[i] = ll_no[i];
                                break;
                }
        }
        barrier->count=barrier->count-1;
        //actual sum operation
        switch(type){
                case MT_DOUBLE:
			for(i=0;i<in_cnt;i++){
                        	if(db_no[i] > barrier->db_result[i])
					barrier->db_result[i] = db_no[i];
			#ifdef MT_DBG
			if(barrier_dbg==1)printf("Max barrier->db_result[%d]=%lf db_no[%d]=%lf thread_no=%d\n",i,barrier->db_result[i],i,db_no[i],g_tw_mynode);
			#endif
			}	
                        break;
                case MT_LONG_LONG:
			for(i=0;i<in_cnt;i++){
                        	if(ll_no[i] > barrier->ll_result[i])
					barrier->ll_result[i] = ll_no[i];
			#ifdef MT_DBG
			if(barrier_dbg==1)printf("Max barrier->ll_result[%d]=%lld ll_no[%d]=%lld thread_no=%d\n",i,barrier->ll_result[i],i,ll_no[i],g_tw_mynode);
			#endif
			}
                        break;
        }

		pthread_mutex_unlock(&(barrier -> mutex));
	
		if (pthread_barrier_wait(&(barrier -> barrier)) == PTHREAD_BARRIER_SERIAL_THREAD)
		{
		barrier -> count = barrier -> n_threads;
		}

        //put result back before returning
	db_ptr_no=(double *)out_no;
	ll_ptr_no=(long long *)out_no;
        switch(type){
                case MT_DOUBLE:
			for(i=0;i<in_cnt;i++){
                        db_ptr_no[i] = barrier->db_result[i];
			#ifdef MT_DBG
			if(barrier_dbg==1)printf("Max Done => barrier->db_result[%d]=%lf db_ptr_no[%d]=%lf thread_no=%d\n",i,barrier->db_result[i],i,db_ptr_no[i],g_tw_mynode);
			#endif
			}
                        break;
                case MT_LONG_LONG:
			for(i=0;i<in_cnt;i++){
			ll_ptr_no[i] = barrier->ll_result[i];
			#ifdef MT_DBG
			if(barrier_dbg==1) printf("Max Done => barrier->ll_result[%d]=%lld db_ptr_no[%d]=%lld thread_no=%d\n",i,barrier->ll_result[i],i,ll_ptr_no[i],g_tw_mynode);
			#endif
			}
                        break;
        }
        //Don't unlock until all threads are done
        
	return 0;
}
int mt_allreduce(void *in_no,void *out_no,int in_cnt,int type,int op,barrier_t *br){
	switch(op){
		case MT_SUM:
			return mt_sum(in_no,out_no,in_cnt,type,br);
			break;
		case MT_MIN:
			return mt_min(in_no,out_no,in_cnt,type,br);
			break;
		case MT_MAX:
			return mt_max(in_no,out_no,in_cnt,type,br);
			break;
	}
	return 0;
}

int mt_allreduce_long_sum(unsigned long long * in_no, unsigned long long * out_no,barrier_t *barrier){

	unsigned long long ll_no = *in_no;

	int status;

	status=pthread_mutex_lock(&(barrier->mutex));

    if (status != 0)
	{
    	printf("\nLock failed :%d Error:%d\n",status,EINVAL);
        return -1;
    }

	if (barrier -> count == barrier -> n_threads) barrier -> ll_result[0] = ll_no;
	else barrier->ll_result[0] += ll_no;

	barrier -> count--;

    status = pthread_mutex_unlock(&(barrier->mutex));

    if (status != 0)
	{
    	printf("\nLock failed :%d Error:%d\n",status,EINVAL);
        return -1;
    }

	if (pthread_barrier_wait(&(barrier->barrier)) == PTHREAD_BARRIER_SERIAL_THREAD)
	{
		barrier -> count = barrier -> n_threads;
	}

	*out_no = barrier -> ll_result[0];

	return 0;

}

int mt_allreduce_long_sum_2(int node, unsigned long long * in_no, unsigned long long * out_no,barrier_t *barrier){

        unsigned long long ll_no = *in_no;

        int status;

        if(node == 0)
        {
           status=pthread_mutex_lock(&(barrier->mutex));

           if (status != 0){

                   printf("\nLock failed :%d Error:%d\n",status,EINVAL);

                   return -1;

           }

           barrier->ll_result[0] = ll_no;

           status=pthread_mutex_unlock(&(barrier->mutex));

           if (status != 0){

                printf("\nLock failed :%d Error:%d\n",status,EINVAL);

                return -1;

           }
        }
        if (pthread_barrier_wait(&(barrier->barrier)) == PTHREAD_BARRIER_SERIAL_THREAD){

                 barrier->count=barrier->n_threads;
        }

        *out_no=barrier->ll_result[0];

        return 0;
}




int mt_allreduce_double_min(double * in_no, double * out_no, barrier_t *barrier){

        double db_no = *in_no;

	int status;

        status=pthread_mutex_lock(&(barrier->mutex));

        if (status != 0){

                printf("\nLock failed :%d Error:%d\n",status,EINVAL);

                return -1;

        }

        if(barrier->count == barrier->n_threads)
                barrier->db_result[0] = db_no;
        else{

		if(db_no < barrier->db_result[0])
                 	barrier->db_result[0] = db_no;

	}

        barrier->count--;

        status=pthread_mutex_unlock(&(barrier->mutex));

        if (status != 0){

                printf("\nLock failed :%d Error:%d\n",status,EINVAL);

                return -1;

        }

        if (pthread_barrier_wait(&(barrier->barrier)) == PTHREAD_BARRIER_SERIAL_THREAD){

                 barrier->count=barrier->n_threads;

        }

        *out_no=barrier->db_result[0];
//	printf("\nmt_allreduce_double_min in_no=%lf out_no=%lf Node=%d\n",db_no,barrier->db_result[0],g_tw_mynode);

		return 0;

}

int mt_allreduce_double_min_2(int node, double * in_no, double * out_no, barrier_t *barrier){

        double db_no = *in_no;

        int status;

        if(node == 0)
        {
           status=pthread_mutex_lock(&(barrier->mutex));

           if (status != 0){

                   printf("\nLock failed :%d Error:%d\n",status,EINVAL);

                   return -1;

           }

           barrier->db_result[0] = db_no;

           status=pthread_mutex_unlock(&(barrier->mutex));

           if (status != 0){

                printf("\nLock failed :%d Error:%d\n",status,EINVAL);

                return -1;

           }

        }
        if (pthread_barrier_wait(&(barrier->barrier)) == PTHREAD_BARRIER_SERIAL_THREAD){

                 barrier->count=barrier->n_threads;

        }

        *out_no=barrier->db_result[0];
//      printf("\nmt_allreduce_double_min in_no=%lf out_no=%lf Node=%d\n",db_no,barrier->db_result[0],g_tw_mynode);

        return 0;

}
