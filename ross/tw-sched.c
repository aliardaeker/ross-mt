#include <ross.h>

/* 
 * Get all events out of my event queue and spin them out into
 * the priority queue so they can be processed in time stamp
 * order.  
 */
//static 
void
tw_sched_event_q(tw_pe * me)
{
  	tw_clock	 start;
  	tw_kp		* dest_kp;
  	tw_event	* cev;
  	tw_event	* nev;
  
  	while (me->event_q.size) 
    {
    	cev = tw_eventq_pop_list(&me -> event_q);
      
      	for (; cev; cev = nev) 
		{
	  		nev = cev->next;
	  
	  		if (!cev->state.owner || cev->state.owner == TW_pe_free_q) tw_error(TW_LOC, "no owner!");
	  
	  		if (cev -> state.cancel_q)
	    	{
	      		cev -> state.owner = TW_pe_anti_msg;
	      		cev -> next = cev -> prev = NULL;
	      		continue;
	    	}
	  
	  		switch (cev -> state.owner) 
	    	{
	    		case TW_pe_event_q:
	      			dest_kp = cev -> dest_lp -> kp;
	      
	      			if (dest_kp -> last_time > cev -> recv_ts) 
					{
		  				/* cev is a straggler message which has arrived
		   				* after we processed events occuring after it.
		   				* We need to jump back to before cev's timestamp.
		   				*/
						
						start = tw_clock_read(); // Rollback
						tw_kp_rollback_to(dest_kp, cev -> recv_ts);

						me -> stats.s_rollback += tw_clock_read() - start;
					}
	      
					start = tw_clock_read();
	      			tw_pq_enqueue(me -> pq, cev);
	      			me -> stats.s_pq += tw_clock_read() - start; // Priority Queue push
	      			break;
	      
	    		default:
	      			tw_error(TW_LOC,
		       		"Event in event_q, but owner %d not recognized",
		      		cev->state.owner);
	    	}
		}
	}
}

/*
 * OPT: need to link events into canq in reverse order so 
 *      that when we rollback the 1st event, we should not
 *	need to do any further rollbacks.
 */
static void
tw_sched_cancel_q(tw_pe * me)
{
    tw_clock	 start;
	tw_event 	* cev;
	tw_event	* nev;

	start = tw_clock_read();
	
	while (me -> cancel_q) 
	{
		cev = me -> cancel_q;
		me -> cancel_q = NULL;

		for (; cev; cev = nev) 
		{
			nev = cev -> cancel_next;

			if (!cev -> state.cancel_q || cev -> state.owner == TW_pe_free_q || cev -> state.owner == TW_net_acancel )
			{
				fprintf(stderr, "%d: Error in sched_cancel: %d - %d\n", g_tw_tid, cev -> state.cancel_q, cev -> state.owner);
				return;
				// tw_error(TW_LOC, "No cancel_q bit on event in cancel_q");
			}

			if (!cev -> state.owner) 
			{
				FILE * fp = fopen("./output/rates.txt", "a");
    			fprintf(fp, "Sched Cancel Q Error: %d\n", cev -> state.owner);
				fclose(fp);
				
				unsigned long src = cev -> src_lp -> pe -> id;
				unsigned long dest = cev -> dest_lp -> pe -> id;
				fprintf(stderr, "%d: cancelled event, no owner! %d, src: %lu dest: %lu\n", g_tw_tid, cev -> state.owner, src, dest);
				exit(1);
				//tw_error(TW_LOC, "%d: cancelled event, no owner! %d, src: %lu dest: %lu\n", g_tw_tid, cev -> state.owner, src, dest);
			}

			switch (cev -> state.owner) 
			{
				case TW_pe_event_q:
					/* This event hasn't been added to our pq yet and we
					 * have not officially received it yet either.  We'll
					 * do the actual free of this event when we receive it
					 * as we spin out the event_q chain.
					 */
					tw_eventq_delete_any(&me -> event_q, cev);
					tw_event_free(me, cev);
					break;

				case TW_pe_anti_msg:
					tw_event_free(me, cev);
					break;

				case TW_pe_pq:
					/* Event was not cancelled directly from the event_q
					 * because the cancel message came after we popped it
					 * out of that queue but before we could process it.
					 */
					tw_pq_delete_any(me -> pq, cev);
					tw_event_free(me, cev);
					break;

				case TW_kp_pevent_q:
					/* The event was already processed. 
					 * SECONDARY ROLLBACK
					 */

					tw_kp_rollback_event(cev);

					tw_event_free(me, cev);
					break;

				default:
					tw_error(TW_LOC,
						"Event in cancel_q, but owner %d not recognized",
						cev->state.owner);
			}
		}
	}

	me->stats.s_cancel_q += tw_clock_read() - start; // Cancel Queue
}

void
flush_all_messages(tw_pe * me)
{	
	//tw_clock start = tw_clock_read(); // Event read
	//tw_net_read(me);
	//me -> stats.s_net_read += tw_clock_read() - start; // Event Read

	tw_clock start = tw_clock_read(); 
    tw_sched_event_q(me);
	me -> stats.s_sched_event += tw_clock_read() - start; // Sched Event
	int counter = 0;
	
	start = tw_clock_read();
	while (1)
	{
		tw_event *cev;
		tw_lp *clp;
		tw_kp *ckp;

		if (me -> free_q.size <= g_tw_gvt_threshold) 
		{
			//fprintf(stderr, "%d: Error in flush - freeq: %ld\n", g_tw_tid, me -> free_q.size);
			//exit(1);
			
			tw_gvt_force_update(me);
			break;
		}
		//else fprintf(stderr, "%d in flush, %d\n", g_tw_tid, g_tw_gvt_threshold);

		start = tw_clock_read();
		if (!(cev = tw_pq_dequeue(me -> pq))) break;
		me -> stats.s_pq += tw_clock_read() - start; // Priority Queue pop 

		clp = cev -> dest_lp;	
		ckp = clp -> kp;
		me -> cur_event = cev;
		ckp -> last_time = cev -> recv_ts;

		/* Save state if no reverse computation is available */
		if (!clp -> type.revent) tw_error(TW_LOC, "Reverse Computation must be implemented!");

		counter++;
		// Event is sent here
        start = tw_clock_read();
		(*clp -> type.event) (clp -> cur_state, &cev -> cv, tw_event_data(cev), clp);
       	me -> stats.s_event_process += tw_clock_read() - start; // Event Processing

		ckp -> s_nevent_processed++;

       	/* We ran out of events while processing this event.  We
		 * cannot continue without doing GVT and fossil collect.
		 */

		if (me -> cev_abort) 
		{
		    start = tw_clock_read();
			me -> stats.s_nevent_abort++;
			me -> cev_abort = 0;

			tw_event_rollback(cev);
			tw_pq_enqueue(me -> pq, cev);
			cev = tw_eventq_peek(&ckp -> pevent_q);

			ckp -> last_time = cev ? cev -> recv_ts : me -> GVT;
		    me->stats.s_event_abort += tw_clock_read() - start; // Event Abort
			break;
		}

		/* Thread's current event into processed queue of kp */
		cev -> state.owner = TW_kp_pevent_q;
		tw_eventq_unshift(&ckp -> pevent_q, cev);
	}
	me -> stats.s_batch_event += tw_clock_read() - start; // Batch Event

	//fprintf(stderr, "%d FLUSHED: %d - sizes: %d, %ld, %d - %.0f\n", 
	//	g_tw_tid, counter, tw_pq_get_size(me -> pq), me -> event_q.size, get_inq_size(), me -> GVT);
}

//static 
void
tw_sched_batch(tw_pe * me)
{
    tw_clock	 start;
	unsigned int	 msg_i;

	/* Process g_tw_mblock events, or until the PQ is empty
	 * (whichever comes first). 
	 */
	for (msg_i = g_tw_mblock; msg_i; msg_i--) 
	{
		tw_event * cev;
		tw_lp * clp;
		tw_kp * ckp;

		/* OUT OF FREE EVENT BUFFERS.  BAD.
		 * Go do fossil collect immediately.
		 */
		if (me -> free_q.size <= g_tw_gvt_threshold) 
		{
			tw_gvt_force_update(me);
			break;
		}

		start = tw_clock_read();
		if (!(cev = tw_pq_dequeue(me -> pq))) break;
		me -> stats.s_pq += tw_clock_read() - start; // Priority Queue pop 

		clp = cev -> dest_lp; // clp is the processing lp which cev is destined to.  
		ckp = clp -> kp;
		ckp -> last_time = cev -> recv_ts; // LVT of LP

		me -> cur_event = cev;

		/* Save state if no reverse computation is available */
		if (!clp -> type.revent) tw_error(TW_LOC, "Reverse Computation must be implemented!");


        start = tw_clock_read();
		// Event is sent here
		(*clp -> type.event)(
			clp -> cur_state,
			&cev -> cv,
			tw_event_data(cev),
			clp);
		
		ckp -> s_nevent_processed++;
       	me -> stats.s_event_process += tw_clock_read() - start; // Event Processing

       	/* We ran out of events while processing this event.  We
		 * cannot continue without doing GVT and fossil collect.
		 */
		if (me -> cev_abort) 
		{
		    start = tw_clock_read();
			me -> stats.s_nevent_abort++;
			me -> cev_abort = 0;

			tw_event_rollback(cev);

			tw_pq_enqueue(me -> pq, cev);

			cev = tw_eventq_peek(&ckp -> pevent_q);

			ckp -> last_time = cev ? cev -> recv_ts : me -> GVT;
		    me->stats.s_event_abort += tw_clock_read() - start; // Event Abort

			break;
		}

		/* Thread's current event into processed queue of kp */
		cev -> state.owner = TW_kp_pevent_q;
		
		tw_eventq_unshift(&ckp -> pevent_q, cev);
	}
}

static void
tw_sched_init(tw_pe * me)
{ 
	(* me -> type.pre_lp_init) (me);
    tw_init_kps(me);
	tw_init_lps(me);

	//net_barrier();
	//fprintf(stderr, "%d after init lps\n", g_tw_tid);
	//net_barrier();

	(* me -> type.post_lp_init) (me);

	/*
	 * Recv all of the startup events out of the network before
	 * starting simulation.. at this point, all LPs are done with init.
	 */
	if (tw_nnodes() > 1)
	{
		tw_net_read(me);
		tw_net_barrier(me);
	}

	if (tw_nnodes() > 1) tw_clock_init(me);

	/* This lets the signal handler know that we have started
	 * the scheduler loop, and to print out the stats before
	 * finishing if someone should type CTRL-c
	 */
	if ((tw_nnodes() > 1 || g_tw_npe > 1) &&
		tw_node_eq(&g_tw_mynode, &g_tw_masternode) && 
		me->local_master)
	{
	    if (g_tw_synchronization_protocol == CONSERVATIVE) printf("*** START PARALLEL CONSERVATIVE SIMULATION ***\n\n");
	   	if (g_tw_synchronization_protocol == OPTIMISTIC) printf("*** START PARALLEL OPTIMISTIC SIMULATION ***\n\n");
	}
	else if (tw_nnodes() == 1 && g_tw_npe == 1)
	{
	    // force the setting of SEQUENTIAL protocol
	    if (g_tw_synchronization_protocol!=SEQUENTIAL) g_tw_synchronization_protocol=SEQUENTIAL;

	    printf("*** START SEQUENTIAL SIMULATION ***\n\n");
	}

	if (me->local_master) g_tw_sim_started = 1;
}

/*************************************************************************/
/* Primary Schedulers -- In order: Optimistic, Sequential, Conservative */
/*************************************************************************/

#ifdef DDS 
//static unsigned int blocked_counters[1024];
//static unsigned int gvt_participated[1024];
//static unsigned int gvt_rounds_accum_when_blocked[1024][10000];
//static unsigned int gvt_rounds_accum_when_activated[1024][10000];
#endif

#ifdef PAPI
	pthread_mutex_t papi_lock = PTHREAD_MUTEX_INITIALIZER;
	long long total_inst = 0;
	long long total_cycles = 0;
	
	long long total_l1_cache_misses = 0;
	long long total_l2_cache_misses = 0;

	long long total_l1_data_cache_access = 0;
	long long total_l1_inst_cache_access = 0;
	long long total_l2_cache_access = 0;
#endif

void
tw_scheduler_optimistic(tw_pe * me, int n, int * epg, double * reg, double * rem)
{
	tw_clock start;
	tw_sched_init(me); // Barrier here

    tw_wall_now(&me -> start_time);		    // Wall clock time
	me -> stats.s_total = tw_clock_read(); // Total cpu time

	#ifdef PAPI
	int events = PAPI_NULL;
    long long int values [1];

	pthread_mutex_lock(&papi_lock);
	if (PAPI_thread_init(& pthread_self) != PAPI_OK) {printf("Thread Support Broken\n"); exit(1);}
	pthread_mutex_unlock(&papi_lock);

    if (PAPI_create_eventset(&events) != PAPI_OK) {printf("event create error !!!\n"); exit(1);}

   	//if (PAPI_add_event(events, PAPI_TOT_INS) != PAPI_OK) {printf("1 event add error !!!\n"); exit(1);} // Total instructions
    //if (PAPI_add_event(events, PAPI_TOT_CYC) != PAPI_OK) {printf("2 event add error !!!\n"); exit(1);}

    //if (PAPI_add_event(events, PAPI_L2_TCM) != PAPI_OK) {printf("4 event add error !!!\n"); exit(1);}
    //if (PAPI_add_event(events, PAPI_L2_TCA) != PAPI_OK) {printf("7 event add error !!!\n"); exit(1);}

   	//if (PAPI_add_event(events, PAPI_L1_TCM) != PAPI_OK) {printf("3 event add error !!!\n"); exit(1);}

    //if (PAPI_add_event(events, PAPI_L1_DCM) != PAPI_OK) {printf("7 event add error !!!\n"); exit(1);}
    //if (PAPI_add_event(events, PAPI_L1_DCA) != PAPI_OK) {printf("5 event add error !!!\n"); exit(1);}

    //if (PAPI_add_event(events, PAPI_L1_ICM) != PAPI_OK) {printf("8 event add error !!!\n"); exit(1);}
    //if (PAPI_add_event(events, PAPI_L1_ICA) != PAPI_OK) {printf("6 event add error !!!\n"); exit(1);}
    if (PAPI_add_event(events, PAPI_L1_ICH) != PAPI_OK) {printf("6 event add error !!!\n"); exit(1);}

	if (PAPI_start(events) != PAPI_OK) {printf("event start error !!!\n"); exit(1);}
	#endif
	
	for (;;)
	{		
	  	start = tw_clock_read(); // Event read
	  	tw_net_read(me);
	  	me -> stats.s_net_read += tw_clock_read() - start; // Event Read

		#ifdef DDS 
	  		start = tw_clock_read(); // Event counts read
	  		check_msg_counts(me);
	  		me -> stats.read_msg += tw_clock_read() - start; // Event counts read
		#endif

	  	start = tw_clock_read();
		if (!(me -> interval_counter)) tw_gvt_step1(me);
        else (me -> interval_counter)--;
		me -> stats.s_gvt_func += tw_clock_read() - start; 

      	if (me -> GVT >= g_tw_ts_end) break;

		start = tw_clock_read();
		tw_sched_batch(me);
	  	me -> stats.s_batch_event += tw_clock_read() - start; // Schedule Batch of Event

	  	start = tw_clock_read();
      	tw_sched_event_q(me);
	  	me -> stats.s_sched_event += tw_clock_read() - start; // Schedule from eventq to pq

	  	start = tw_clock_read();
      	tw_sched_cancel_q(me);
	  	me -> stats.s_cancel_event += tw_clock_read() - start; // Schedule Cancel Events
	}
	
	#ifdef PAPI
		if (PAPI_read(events, values) != PAPI_OK) {printf("event start error !!!\n"); exit(1);}
    #endif

 	tw_wall_now(&me -> end_time); // Wall clock run time
  	me -> stats.s_total = tw_clock_read() - me -> stats.s_total; // cpu time

	//fprintf(stderr, "%d Exiting %.0f\n", g_tw_tid, me -> GVT);
	g_tw_ts_end = 0; // Fix this

	#ifdef DDS 
		if (__sync_add_and_fetch(&master_done, 1) == 1) reactivate_all();

		//blocked_counters[g_tw_tid] = me -> blocked_counter;
		//gvt_participated[g_tw_tid] = g_tw_gvt_done;

		//for (int i = 0; i < me -> blocked_counter; i++) 
		{
			//gvt_rounds_accum_when_blocked[g_tw_tid][i] = me -> gvt_round_when_blocked[i];
			//gvt_rounds_accum_when_activated[g_tw_tid][i] = me -> gvt_round_when_activated[i];
		}
    #endif

	tw_net_barrier(me);	

	#ifdef DDS
	if (!g_tw_tid) 
	{
		/*
		FILE * fp = fopen("./output/dds_stats.txt", "w");	

		fprintf(fp, "\nThread ID | # Blocked | # GVT Participated\n");	
		for (int i = 0; i < g_tw_n_threads; i++) fprintf(fp, "  [%3d]: %8u %18u\n", 
			i, blocked_counters[i], gvt_participated[i]);

		fp = fopen("./output/dds_stats_more.txt", "w");
		fprintf(fp, "Absolute GVT Computations: %u\n\n", get_gvt_count());	
		fprintf(fp, "Thread ID | GVT Round When Blocked - Activated");	

		for (int i = 0; i < g_tw_n_threads; i++) 
		{
			fprintf(fp, "\n  [%3d]:      ", i);
			for (int j = 0; j < blocked_counters[i]; j++)  
			{
				fprintf(fp, "[%d %d], ", gvt_rounds_accum_when_blocked[i][j], gvt_rounds_accum_when_activated[i][j]);
			}
		}		

		fclose(fp);
		*/
	}
	tw_net_barrier(me);
	#endif

	(*me -> type.final)(me);
  	tw_stats(me, n);
 
	#ifdef PAPI
	pthread_mutex_lock(&papi_lock);

    total_inst += values[0];
	if (values[0] > total_cycles) total_cycles = values[0];

	total_l2_cache_misses += values[0];
	total_l2_cache_access += values[0];

	total_l1_cache_misses += values[0];
	total_l1_data_cache_access += values[0];
	total_l1_inst_cache_access += values[0];

	pthread_mutex_unlock(&papi_lock);

	tw_net_barrier(me);

	if (g_tw_tid == 0)
	{
		printf("\nPAPI Statistics:\n"); 
		printf("    Instructions completed in total: %llu\n", total_inst);
		printf("    Maximum Cycles: %llu\n", total_cycles);
    	printf("    CPI: %.5f\n\n", total_cycles / (double) total_inst);
    	
		printf("    L1 Cache Misses: %llu, L1 D Cache Access: %llu, L1 I Cache Access: %llu\n\n", 
			total_l1_cache_misses, total_l1_data_cache_access, total_l1_inst_cache_access);

    	printf("    L2 Cache Miss Rate: %.5f\n", total_l2_cache_misses / (double) total_l2_cache_access);

    	FILE * f = fopen("output/instructions.txt", "a");
    	fprintf(f, "%llu\n", total_inst);

    	f = fopen("output/cpi.txt", "a");
    	fprintf(f, "%f\n", total_cycles / (double) total_inst);
 	}
    #endif
	tw_net_barrier(me);
}

void tw_scheduler_sequential(tw_pe * me, int n) {
	
	if(tw_nnodes() > 1) 
		tw_error(TW_LOC, "Sequential Scheduler used for world size greater than 1.");
	
	tw_event *cev;
  	
	tw_sched_init(me);
  	tw_wall_now(&me->start_time);
  	
	while ((cev = tw_pq_dequeue(me->pq))) 
    {
      tw_lp *clp = cev->dest_lp;
      tw_kp *ckp = clp->kp;
      
      me->cur_event = cev;
      ckp->last_time = cev->recv_ts;
      
      (*clp->type.event)(
			 clp->cur_state,
			 &cev->cv,
			 tw_event_data(cev),
			 clp);
      
      if (me->cev_abort)
		tw_error(TW_LOC, "insufficient event memory");
      
      ckp->s_nevent_processed++;
      tw_event_free(me, cev);
    }
  tw_wall_now(&me->end_time);
  
  printf("*** END SIMULATION ***\n\n");
  
  tw_stats(me,n);
  
  (*me->type.final)(me);
}

void
tw_scheduler_conservative(tw_pe * me, int n)
{
  tw_clock start = 0;
  unsigned int msg_i;
  unsigned int round = 0;
  
  tw_sched_init(me);
  tw_wall_now(&me->start_time);
  me->stats.s_total = tw_clock_read();
  
  for (;;)
    {
      if (tw_nnodes() > 1)
	{
	  start = tw_clock_read();
	  tw_net_read(me);
	  me->stats.s_net_read += tw_clock_read() - start;
	}
      
      tw_gvt_step1(me);
      tw_sched_event_q(me);

	  #if !defined(ROSS_GVT_mattern) && !defined(ROSS_GVT_wait_free)
      tw_gvt_step2(me);
	  #endif
	//printf("Done GVT compuatation\n");
      
      if (me->GVT > g_tw_ts_end)
	break;
      
      // put "batch" loop directly here
      /* Process g_tw_mblock events, or until the PQ is empty
       * (whichever comes first). 
       */
      for (msg_i = g_tw_mblock; msg_i; msg_i--) 
	{
	  tw_event *cev;
	  tw_lp *clp;
	  tw_kp *ckp;
	  
	  /* OUT OF FREE EVENT BUFFERS.  BAD.
	   * Go do fossil collect immediately.
	   */
	  if (me->free_q.size <= g_tw_gvt_threshold) {
            tw_gvt_force_update(me);
	    break;
	  }
	  
	  if(tw_pq_minimum(me->pq) >= me->GVT + g_tw_lookahead)
	    //break;
	  
	  start = tw_clock_read();
	  if (!(cev = tw_pq_dequeue(me->pq)))
	    break;
	  me->stats.s_pq += tw_clock_read() - start;
	  
	  clp = cev->dest_lp;
	  ckp = clp->kp;
	  me->cur_event = cev;
	  ckp->last_time = cev->recv_ts;
	  
	  start = tw_clock_read();
	  (*clp->type.event)(
			     clp->cur_state,
			     &cev->cv,
			     tw_event_data(cev),
			     clp);
		
	  ckp -> s_nevent_processed++;
	  me->stats.s_event_process += tw_clock_read() - start;
	  
	  if (me->cev_abort)
	    tw_error(TW_LOC, "insufficient event memory");
	  
	  tw_event_free(me, cev);
	}

        if(me->type.periodic && (++round % g_tw_periodicity))
        {
            (*me->type.periodic)(me);
        }
    }

  
  tw_wall_now(&me->end_time);
  me->stats.s_total = tw_clock_read() - me->stats.s_total;
  
  if((tw_nnodes() > 1 || g_tw_npe > 1) &&
     tw_node_eq(&g_tw_mynode, &g_tw_masternode) && 
     me->local_master)
     printf("*** END SIMULATION ***\n\n");
  
  tw_net_barrier(me);
  
  // call the model PE finalize function
  (*me->type.final)(me);
  
  tw_stats(me,n);
}
